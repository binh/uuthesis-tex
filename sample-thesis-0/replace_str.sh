#!/bin/bash

function replace(){
				sed -i.bak "s/$1/$2/g" $3 || {
					echo "Can't replace string $1 by string $2 in file $3!"
					exit 1
				}

				echo "Replaced str $1 by $2 in file $3"
}

#Replace a string by another string of all files in a folder
if [ $# -lt 3 ]; then
	echo "Usage: <String to be replaced> <string to be in> <folder name>"
	exit 1
fi

echo $1
echo $2
echo $3
#exit 1

cd $3 
for i in *.tex
do
	if [ -f $i ]; then
		replace "$1" "$2" $i
	fi
done



