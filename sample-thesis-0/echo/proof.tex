\newtheorem{theorem}{Theorem}
\newtheorem{property}{Property}
\newtheorem{assumption}{Assumption}
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}[theorem]{Lemma}

Here we give a sketch of why ECHO is safe even though components are redundant
and non-blocking under failure.

First, we must be more specific about what is required of the individual
transition handlers of each component instance.
\begin{assumption}[Operation Atomicity]
\label{atomicity}
Each transition on a {\em component instance} must be
1) handled in transaction ID order (client FIFO order),
2) idempotent, and
3) atomic (when applied it leaves the component instance in a ``before'' state or an ``after'' state).
\end{assumption}

Given these assumptions on the individual transitions of each component
instance, the system can be shown to be linearizable.

\begin{lemma}[Linearizability]
\label{linearizability}
ECHO is linearizable; that is, it appears to process operations operation atomically in some total order consistent with the request order of clients.
\end{lemma}

\noindent
\textbf{Proof.}
An operation takes effect some time between its first invocation and its acknowledgement.
Component instances only process messages transaction ID order, which is consistent with client invocation order.
Individually, each message processed for the transaction $T_{n+1}$ with ID $n+1$  results in one of four outcomes: 1) the component instance is not in state $n$ or $n+1$, so it is ignored; 2) the operation completes successfully (moving from state $n$ to $n+1$), 3) the operation fails completely (leaving the component instance in state $n$), or 4) the operation partially.
By Assumption~\ref{atomicity}, case 4 is equivalent to case 2 or 3.
In case 2, two cases can be encountered: if the component instance is in state $n$ then it transitions to $n+1$; if the component instance is already in state $n+1$, then the effect is the same by the idempotence assumption.
Since all operations are retried until completion, case 3 will eventually result in case 2 or 4.
%
\qed

In fact, in all components the final update to shared storage serves a linearization point for the processing of each message by a whole component.
It is a single, atomic, FIFO ordered operation that all processed messages must reach before they are acknowledged to the client.
The final update to the top-level component's shared state serves as the linearization point for each transaction.

Lemma~\ref{linearizability} can be immediately strengthened, since the total
order above is precisely FIFO order.

\begin{lemma}[FIFO Processing]
ECHO appears to processes operations atomically, in client FIFO order, one-at-a-time.
\end{lemma}

Finally, since the ordinary, unreplicated protocol precisely processes messages atomically, in client FIFO order, one-at-a-time, this gives the essential safety property:

\begin{property}[ECHO Safety Property]
The set of states observed by ECHO clients is equivalent to the unreplicated protocol.
\end{property}
