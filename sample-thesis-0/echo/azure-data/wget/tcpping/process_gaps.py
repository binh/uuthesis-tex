import argparse

mappings = [["bdeploy1my", "52.178.207.8"], 
						["deploy1weumy", "13.80.14.121"],
						["deploy1wusmy", "40.118.165.178"],
						["deploy2my", "13.79.168.159"],
						["deploy2weumy", "13.95.147.203"],
						["deploy2wusmy", "104.42.234.146"]
]


def convert_to_ip(arr):
	out = []
	out2 = []
	unique_ip = set()
	
	for n in arr:
		a = n.split(":")
		
		# Skip BBC for now
		if "bbc" in a[0]:
			continue

		# Ping to 127.0.0.1 without port is seen in one set - skip
		if len(a) < 2:
			continue
		
		# Map DNS name to IP
		for ip in mappings:
			if ip[0] in a[0]:
				a[0] = ip[1]
				break

		# Add unique IPs
		unique_ip.add(a[0])
			
		# Map ports to VMs (0 = LB)
		if "80" in a[1]:
			a[1] = 0
		elif "2000" in a[1]:
			a[1] = 1
		elif "2001" in a[1]:
			a[1] = 2
		elif "2002" in a[1]:
			a[1] = 3
			
		out.append(a)

	for i in unique_ip:
		m = set()
		for o in out:
			if o[0] == i:
				m.add(o[1])
		out2.append([i, sorted(m)])
		
	return out2


def get_locality(ip_addr, resource):
	ind = 0
	for i in mappings:
		if i[1] == ip_addr:
			break
		ind = ind + 1

	if ind > len(mappings):
		locality = -1
	else:
		if ind == resource:
			locality = 0
		elif ind == resource+3 or ind == resource-3:
			locality = 1
		else:
		  locality = 2
			
	return locality





parser = argparse.ArgumentParser(description='Parse tcpping gaps')
parser.add_argument('-r', action="store", required=True,
										help="ID of the resource ID group (as indexed in mappings var)", dest="resource", type=int)
parser.add_argument('-v', action="store", required=True,
										help="VM ID within resource group", dest="vm", type=int)
parser.add_argument('-f', action="store", required=True,
										help="tcpping gap file to be processed (e.g. bdeploy1myVM1.inter_dc.tcpping_gap)", dest="file")

args = parser.parse_args()

filename = args.file
resource = args.resource
vm = args.vm


f = open(filename, 'r');
for line in f:
	a = line.split("|")
	time = a[0]
	gap = a[1]
	pnodes = a[2].split(",")

	#if int(gap) < 500:
	#	continue
	
	nodes = convert_to_ip(pnodes)
	#print time, gap, nodes
	if len(nodes) > 0:
		#print time, gap, nodes
		for n in nodes:
			if 0 in n[1]:
				print time, resource, vm, gap, get_locality(n[0], resource), 0
			if 1 in n[1] or 2 in n[1] or 3 in n[1]:
				print time, resource, vm, gap, get_locality(n[0], resource), 1

f.close()
