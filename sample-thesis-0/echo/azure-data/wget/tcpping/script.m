data = load('mout.txt');

%time, resource, vm, gap, get_locality(n[0], resource), 0

iTime = 1;
iResource = 2;
iVM = 3;
iGap = 4;
iLocality = 5;   % 0 - same LB, 1 - same DC, 2 - other DC
iType = 6;       % 0 - LB, 1 - VMs


% Summary: from cat *tcpping.summary
% Order:
% bdeploy1myVM1.tcpping.summary
% bdeploy1myVM2.tcpping.summary
% bdeploy1myVM3.tcpping.summary
% deploy1weumyVM1.tcpping.summary
% deploy1weumyVM2.tcpping.summary
% deploy1weumyVM3.tcpping.summary
% deploy1wusmyVM1.tcpping.summary
% deploy1wusmyVM2.tcpping.summary
% deploy1wusmyVM3.tcpping.summary
% deploy2myVM1.tcpping.summary
% deploy2myVM2.tcpping.summary
% deploy2myVM3.tcpping.summary
% deploy2weumyVM1.tcpping.summary
% deploy2weumyVM2.tcpping.summary
% deploy2weumyVM3.tcpping.summary
% deploy2wusmyVM1.tcpping.summary
% deploy2wusmyVM2.tcpping.summary
% deploy2wusmyVM3.tcpping.summary

summary = [
40206311, 27103, .00067409815339686349; ...
23772046, 10186, .00042848646683587941; ...
23559775, 12101, .00051362969298306117; ...
23100551, 15478, .00067002730800663585; ...
14984479, 6490, .00043311482501326872; ...
6479004, 1153, .00017795945179228165; ...
23554331, 7838, .00033276258196422560; ...
23482083, 8765, .00037326330888107328; ...
23555003, 7976, .00033861171658521970; ...
23023040, 15122, .00065682029827511918; ...
23519806, 11004, .00046786100191472667; ...
23546155, 10661, .00045277031430397022; ...
23777763, 10681, .00044920121375589453; ...
23583702, 11655, .00049419722145403635; ...
23398920, 13418, .00057344527012357835; ...
23396951, 9332, .00039885538931974512; ...
23514525, 8418, .00035799149674509691; ...
15864317, 5950, .00037505554131325035
];



LOC = unique(data(:,iLocality));
TYP = unique(data(:,iType));

ind0 = find(data(:,iLocality) == 0);
ind1 = find(data(:,iLocality) == 1);
ind2 = find(data(:,iLocality) == 2);

fprintf('Total gaps with different locality - 0: %d, 1: %d, 2: %d\n', ...
	length(ind0), length(ind1), length(ind2)); 

figure(1);
clf;

subplot(1,3,1)

ind0 = find(data(:,iLocality) == 0);
ind1 = find(data(:,iLocality) == 1);
ind2 = find(data(:,iLocality) == 2);

X0 = sort(data(ind0, iGap));
X1 = sort(data(ind1, iGap));
X2 = sort(data(ind2, iGap));

Y0 = (0:length(ind0)-1)/(length(ind0)-1);
Y1 = (0:length(ind1)-1)/(length(ind1)-1);
Y2 = (0:length(ind2)-1)/(length(ind2)-1);

semilogx(X0, Y0, X1, Y1, X2, Y2);
ylabel('CDF');
xlabel('Gap duration [s]');
legend('Same LB', 'Same DC', 'Other DC', 'Location', 'SouthEast');
title('Outage duration distribution across all VMs and LBs');



%figure(2);
%clf;
subplot(1,3,2);

ind0 = find(data(:,iLocality) == 0 & data(:,iType) > 0);
ind1 = find(data(:,iLocality) == 1 & data(:,iType) > 0);
ind2 = find(data(:,iLocality) == 2 & data(:,iType) > 0);

X0 = sort(data(ind0, iGap));
X1 = sort(data(ind1, iGap));
X2 = sort(data(ind2, iGap));

Y0 = (0:length(ind0)-1)/(length(ind0)-1);
Y1 = (0:length(ind1)-1)/(length(ind1)-1);
Y2 = (0:length(ind2)-1)/(length(ind2)-1);

semilogx(X0, Y0, X1, Y1, X2, Y2);
ylabel('CDF');
xlabel('Gap duration [s]');
legend('Same LB', 'Same DC', 'Other DC', 'Location', 'SouthEast');
title('Outage duration distribution across VMs');



%figure(3);
%clf;
subplot(1,3,3);

ind0 = find(data(:,iLocality) == 0 & data(:,iType) == 0);
ind1 = find(data(:,iLocality) == 1 & data(:,iType) == 0);
ind2 = find(data(:,iLocality) == 2 & data(:,iType) == 0);

X0 = sort(data(ind0, iGap));
X1 = sort(data(ind1, iGap));
X2 = sort(data(ind2, iGap));

Y0 = (0:length(ind0)-1)/(length(ind0)-1);
Y1 = (0:length(ind1)-1)/(length(ind1)-1);
Y2 = (0:length(ind2)-1)/(length(ind2)-1);

semilogx(X0, Y0, X1, Y1, X2, Y2);
ylabel('CDF');
xlabel('Gap duration [s]');
legend('Same LB', 'Same DC', 'Other DC', 'Location', 'SouthEast');
title('Outage duration distribution across LBs');




