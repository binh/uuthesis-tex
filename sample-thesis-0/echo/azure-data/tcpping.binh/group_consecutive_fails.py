import sys
import collections


#input="tmp.vm.gap/all.inter_dc.tcpping_fail.enumerated"
if len(sys.argv) < 2:
 	print "Usage: <input file name>"
	sys.exit(1)

input_file=open(sys.argv[1],'r').readlines()
#input_file=str(sys.argv[1])


ts_bin = []
dst_bin = []
for l in input_file:
	try:
		ts = int(l.split("|")[0])
		dst = str(l.split("|")[1]).strip()
		ts_bin.append(ts)
		dst_bin.append(dst)
	except:
		continue

#print len(ts_bin)
ts_bin.sort()
gap_bin = collections.defaultdict(list)
gap_bin_dst = collections.defaultdict(list)
index = 0
current_index = 0
ts = ts_bin[0]
last_ts = ts_bin[0]

while current_index < len(ts_bin)-1:
	current_ts = ts_bin[current_index]	
	current_dst = dst_bin[current_index]
	gap_bin[current_ts].append(current_ts)
	if current_dst not in gap_bin_dst[current_ts]:
		gap_bin_dst[current_ts].append(current_dst)

	last_ts = current_ts
	for i in range (current_index+1, len(ts_bin)):
		#print "i=%s"%i
		forward_ts = ts_bin[i]
		forward_dst = dst_bin[i]
		if forward_ts - last_ts <= 10: #if the 2 fail events are 10s away, group them
			if forward_dst not in gap_bin_dst[current_ts]:
				gap_bin_dst[current_ts].append(forward_dst)
			if forward_ts - last_ts != 0 and forward_ts > last_ts: #If consecutive timestamp
				gap_bin[current_ts].append(forward_ts)
			last_ts = forward_ts
		else:	#if not, move the last_index pointer forward to current index
			current_index = i
			break
	current_index = i
	#print current_index

gap_len = []
for ts in gap_bin:
	dst_str = ",".join(gap_bin_dst[ts])
	min_ts = float("inf")
	max_ts = -float("inf")
	for t in gap_bin[ts]:
		if int(t) <= min_ts:
			min_ts = int(t)
		if int(t) >= max_ts:
			max_ts = int(t)
	print "%s|%s|%s"%(ts, str(max_ts-min_ts+1), dst_str)

