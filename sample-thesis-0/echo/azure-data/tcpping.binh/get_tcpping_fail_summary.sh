#!/bin/bash
#source ../../constants.sh

source  constants.sh	#deploy

mkdir RESULTS
echo "$(hostname)"
echo "-----------"

r="/tmp/tcpping.fails"
total="/tmp/tcpping.total"
summary="/tmp/tcpping.summary"

rm $r
rm $total
rm $summary


sum=0
for i in ./RESULTS/*.tcpping
do
	if [[ $i == *"bbc.co.uk-200"* ]]; then
		continue
	fi

	grep -v open $i >> $r
	wc -l $i >> $total
	t=$(wc -l $i | awk '{print $1}')
	let sum+=$t
done

f=$(wc -l $r | awk '{print $1}')
f_percent=$(echo $f/$sum | bc -l) 
echo "total tcppings|fail tcppings| percent" >> $summary
echo "$sum|$f|$f_percent" >> $summary

echo "total tcppings|fail tcppings| percent"
echo "$sum|$f|$f_percent"
