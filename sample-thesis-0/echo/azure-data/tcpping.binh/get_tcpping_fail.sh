#!/bin/bash

#install probes on all VMs on multiple deployments
#source ../../constants.sh
source constants.sh

for ip in $HTTP_IP
do
	if [ "$ip" == "127.0.0.1" -o "$ip" == "bbc.co.uk" ]; then
		continue
	fi
	echo "$ip,1000"
	echo "----------------"
	ssh -p 1000  -t -t epc@$ip "grep -v azure /home/epc/http/RESULTS/http.result | wc -l";

	echo "$ip,1001"
	echo "----------------"
	ssh -p 1001  -t -t epc@$ip "grep -v azure /home/epc/http/RESULTS/http.result | wc -l";

	echo "$ip,1002"
	echo "----------------"
	ssh -p 1002  -t -t epc@$ip "grep -v azure /home/epc/http/RESULTS/http.result | wc -l";
done

#scp -P 1000  ports.conf epc@$ip:/home/epc/ports.conf;
