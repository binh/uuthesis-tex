\begin{abstract}

%% The flexibility and cost-effectiveness of cloud based 
%% solutions have resulted in many services, including
%% core networking functions, being moved to cloud platforms.
%% Much work has been done to realize this transition,
%% including dynamically orchestrating virtualized
%% network functions and dealing with the datapath
%% (in)efficiencies of virtual forwarding elements.
%% The fact that cloud environments provide
%% a platform with inherently different robustness
%% properties, compared to earlier (often redundant) hardware
%% platforms, has not been addressed. Core network
%% elements are typically virtualized by simply running 
%% the same software stack in a cloud environment.
%% However, cloud platforms typically offer 
%% availability guarantees that might be an order
%% of magnitude worse than redundant hardware platforms.
%% In this paper we address these concerns by applying
%% distributed systems principles to the design of
%% a reliable and distributed core mobile network
%% control plane. We present the design and implementation
%% of our approach and evaluate its functionality on
%% a public cloud platform.


Economies of scale associated with public cloud platforms offer
flexibility and cost-effectiveness,
resulting in various services and businesses moving
to the cloud. 
%A part of their success is in reducing hardware costs by letting applications deal with the
%inherent platform unreliability, as well as offering the same underlying IaaS
%(infrastructure-as-a-service) abstractions to all customer.
%Much work has been done to move various services and entire businesses to public cloud platforms.
One area with little progress is cellular core network.
A cellular core network manages states of cellular clients; it is a large distributed
state machine with very different virtualization challenges compared to typical cloud services. 
%Recent industry efforts in network function virtualization of the core are mainly focused
%on private cloud where tenants have control over the data center robustness and infrastructure,
%and core network elements are typically virtualized by simply running the same software stack
%in a cloud environment.
In this paper we present a novel cellular core network architecture, called
\echo{},\footnote{After ``Echo, the Nymph of Steady Reply'' from Greek
mythology.}
particularly suited to public cloud deployments,
where the availability guarantees might be an order of magnitude worse 
compared to existing (redundant) hardware platforms.
We present the design and implementation of our approach and evaluate its functionality
on a public cloud platform.

\end{abstract}
