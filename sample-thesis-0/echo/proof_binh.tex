\newtheorem{theorem}{Theorem}
\newtheorem{property}{Property}
\newtheorem{assumption}{Assumption}
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}[theorem]{Lemma}

%Here we give a sketch of why ECHO is safe even though components are redundant
%and non-blocking under failure.


The proof first needs to show that a leaf component (a component that does not 
trigger side effects to other components) operates linearizably. That is, 
the leaf component processes requests from a previous component in some total order 
consistent with the request order and the leaf component can only be in either a ``before'' or a ``after'' state. 


\begin{assumption}[Idempotent operation]
\label{idempotent}
Each leaf component instance processes requests from other components idempotently; that is, a retry from other components causes the same effect 
as the first successful request does on the component.
\end{assumption}

The idempotent operation of a leaf component is ensured by the fact that 
only one component instance could update the shared state. Once 
an instance updates the state, the {\em version} number of the state 
increases, thus other concurrent instances holding an old {\em version} number can no longer mutate the state.


With the assumption of idempotent operation of a leaf component, we prove a leaf component operates linearizably.

\begin{lemma}[A leaf component is linearizable]
\label{leaf-linearizability}
Each transition on a leaf component instance is linearizable; that is, it processes operations atomically in some total order consistent with the request order of a previous component.
\end{lemma}



\begin{figure}[h]
  \centering
  \includegraphics[scale=0.7]{echo/figs/echo-leaf-proof.png}
  \caption{\echo's leaf component is linearizable.}
  \label{fig:leaf-component-proof}
\end{figure}

\noindent
\textbf{Proof.}
As shown in Figure~\ref{fig:leaf-component-proof}, an operation takes effect some time between its first invocation (Request $n$) and its acknowledgment (Reply $n$).
%Component instances only process messages transaction ID order, which is consistent with client invocation order.
Given that the update to the shared State Storage is atomic, individually, each request processed for the transaction $T_{n+1}$ with ID $n+1$  results in one of four outcomes: 1) the component instance is not in state $n$ or $n+1$, so the request is ignored (i.e., at step~\ref{lst:line:check_sequence_number}); 2) the operation completes successfully, the component instance updates the State Storage (moving from state $n$ to $n+1$ at step~\ref{lst:line:commit}) and replies, 3) the operation fails before updating the state (i.e., fails in (1) period and leaves the component in state $n$), or 4) the operation fails after updating the state (i.e., fails in (2) period and leaves the component in state $n+1$).

In case 3, since all requests are retried until completion, there must be another 
instance that progresses to either case 4 (in-completed) or case 2 (completed).
In case 4, since the component is already in state $n+1$, the effect is the same 
because the component is idempotent by assumption~\ref{idempotent}. That is, another component instance simply replies 
with a recorded reply at step~\ref{lst:line:check_reply_exists}. This eventually results 
in case 2.
\qed

%By Assumption~\ref{atomicity}, case 4 is equivalent to case 2 or 3.
%In case 2, two cases can be encountered: if the component instance is in state $n$ then it transitions to $n+1$; if the component instance is already in state $n+1$, then the effect is the same by the idempotence assumption.
%Since all operations are retried until completion, case 3 will eventually result in case 2 or 4.
%


In fact, in all components the final update to shared storage serves a linearization point for the processing of each message by a whole component.
It is a single, atomic, FIFO ordered operation that all processed messages must reach before they are acknowledged to the client.
The final update to the top-level component's shared state serves as the linearization point for each transaction.



Given that a leaf component operates linearizably, we can show that \echo system is linearizable.


\begin{lemma}[\echo's linearizability]
\label{linearizability}
\echo is linearizable; that is, it appears to process operations operation atomically in some total order consistent with the request order of clients.
\end{lemma}

\noindent
\textbf{Proof.}
\echo's linearizability could be proved using induction with the base case is the 
{\em leaf component}. 

{\em Base case:} As in lemma~\ref{leaf-linearizability}, a leaf component operates 
linearizably. 

{\em Induction hypothesis:} Now assuming component $M$ operates linearizably, we need to prove component $M+1$ operates linearizably. 

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.7]{echo/figs/echo-linearizability-proof.png}
  \caption{\echo is linearizable.}
  \label{fig:linearizability-proof}
\end{figure}

To prove that component $M+1$ operates linearizably, similarly, we examine an operation 
of Request $n$ on component $M+1$. The request has 4 outcomes: 1) the component instance is not in state $n$ or $n+1$, so the request is ignored; 2) the operation completes successfully, the component instance updates the State Storage (moving from state $n$ to $n+1$) and replies, 3) the operation fails before updating the state (i.e., fails in (1) period and leaves the component in state $n$), or 4) the operation fails after updating the state (i.e., fails in (2) period and leaves the component in state $n+1$).
In case 4, similarly the component is already in state $n+1$ therefore another 
component instance simply replies with a recorded reply (idempotent operation) when 
a retry arrives. This eventually results in case 2.
In case 3, eventually there is an instance triggers the side effect and progresses to case 2. If there are multiple instances trigger multiple side effects, the effect is still linearizable as assumed. Therefore, component $M+1$ is linearizable.
\qed


Lemma~\ref{linearizability} can be immediately strengthened, since the total
order above is precisely FIFO order.


\begin{lemma}[FIFO Processing]
\echo appears to processes operations atomically, in client FIFO order, one-at-a-time.
\end{lemma}

Finally, since the ordinary, unreplicated protocol precisely processes messages atomically, in client FIFO order, one-at-a-time, this gives the essential safety property:

\begin{property}[\echo's Safety Property]
The set of states observed by \echo clients is equivalent to the unreplicated protocol.
\end{property}
