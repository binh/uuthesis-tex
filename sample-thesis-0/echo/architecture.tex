\section{High-level Overview}
\label{sec:architecture}


\begin{figure}[ht!]
  \centering
  \includegraphics[width=0.7\columnwidth]{echo/figs/arch_overview.pdf}
  \caption{\echo components at a high level.}
  \label{fig:arch}
  \vspace{-5mm}
\end{figure}

Figure~\ref{fig:arch} presents a high-level depiction of \echo's operation. 
\echo moves the main components of EPC into the cloud, replicating them
for reliability while connectivity to the UEs is through the access points
which implement our entry point serialization. Connectivity to the Internet
and access points is load-balanced through the existing public cloud load-balancers.

We now discuss in more details the problem space, 
the key observations that guide
\echo{}'s design, and the properties needed to correctly process
control plane transactions. After a design overview,
each component's details are presented.  Finally, we give an argument for
\echo{}'s correctness.


%\subsection{Problem space}

{\bf Problem space:} EPC can be viewed as a distributed state machine comprising multiple components (\S\ref{sec:corehighlevel}).
Each component stores a state for each user.
States must remain consistent both between components and between the user's device and components.
Moreover, the EPC protocols are complex and constantly evolving;
we must avoid modifying the protocols or relying on its implicit semantics for correctness.

Finally, nodes and the connections between them fail in the public cloud (\S\ref{azure_study}), so each component must be replicated over multiple instances. 
Also, messages can be arbitrarily delayed if a node is temporarily slow or loses connectivity.
\echo{} must continue operation despite these failures while
producing the same results as the original
distributed core network that assumes components are reliable.

{\bf Generality:} Note that the term ``component'' here applies to {\em all} control components in 
the EPC mobile core that behaves as state machines: MME, SGW, PGW. 
In the next several sections, we explain the mechanisms in a context of a component (i.e., the MME in particular). 
However, the mechanisms apply {\em exactly the same} on other components.
%% It is important to keep in mind that the mobile core is distributed over sometimes
%% unreliable network so various messages can get delayed even if the components are
%% reliable. Thus ordering of messages between various components is not unique. We only
%% want to make sure that our system processes messages in an order that could have
%% occurred in a conventional core network where all nodes are reliable. 







\subsection{Key observations}
\label{sec:observations}


Our design is guided by observations that are specific to the underlying wireless and core network requirements:

\vskip 4pt
\noindent{\bf Necessary reliable component: }
The access point (eNodeB) is a \emph{necessary reliable} component.
The network connectivity of a mobile device relies on wireless access to the access point.
Connectivity is lost if the access point crashes; there is no point designing the system to deal with access point failures.
This property simplifies \echo{}'s design (\S\ref{sec:entry-point}). 

\vskip 4pt
\noindent{\bf Transactions: }
We define a {\em transaction} to be a complete transition of the distributed state machine of one client from one state to another.
This means that at the end of a transaction, states across all components have transitioned
and that the client's view of that state is consistent.
We maintain consistency by enforcing the appearance of sequential and atomic processing of transactions. 

\vskip 4pt
\noindent{\bf Serialization: }
We enforce sequential processing of transactions by each process by serializing them for each user.
Each user's state is independent of the others, so
%Moreover, state transitions per user are relatively infrequent (they represent control messages, generated at time scale of seconds),
%and the system needs to scale in terms of number of supported users.
serialization of a user's operations does not affect throughput or ability to scale up to support more users.

\vskip 4pt
\noindent{\bf Timeouts are transactions: }
Any component may trigger an application-layer timeout, which triggers a state change.
This is a network-initiated transaction.
We process it the same way as transactions initiated by a user device. 









\subsection{Properties of transaction processing}
\label{sec:tran_prop}



%% %%%% BR: I believed I summarized it but please check
%% These observations and the examples of Section~\ref{sec:mme-requirements} shape the solution. First,
%% control plane elements must be partitioned and replicated for scalability and
%% to avoid lost state. Second, operations must be executed at different replicas,
%% but this must not result in non-FIFO execution.

%% {\bf BR: is this ``State restoration in systems of communicating processes'' by Russell from 1980?}
%% The first requirement can be met with techniques similar to the State Restoration Server: critical
%% state can be stored in partitioned, replicated stores.  The second requirement
%% is harder.  It is impossible to eliminate repeating steps to ensure the
%% completion of side effects when nodes crash; however, it is possible to
%% prevent arbitrary operation reordering.  Instead of reworking the entire set
%% of protocols to make them safe under arbitrary reordering, we introduce
%% an end-to-end sequence number on each operation that preserves FIFO processing
%% throughout the control plane. Each operation and each side effect induced
%% by the operation is tagged with the operation's sequence number, so control
%% plane elements can detect when they should skip processing stale or repeated
%% messages.

%% Overall, our solution preserves the semantics of the unreplicated control plane
%% with reliable point-to-point network connections as near as possible, leaving the core
%% protocol unchanged.




%% BR: Ryan, can we remove this or shall we keep it?
%% Following a long line of works on State Restoration Server (starting from~\cite{} 
%% {\bf BR: is this ``State restoration in systems of communicating processes'' by Russell from 1980?
%%   Do we need this citation or is it obvious? Or am I misunderstanding? })
%% one can avoid state loss by storing it and processing on different replicas. 
%% However, to guarantee consistency across replicas we also need to enforce serialization and atomicity.

We enforce the appearance of sequential and atomic transactions through the following high-level properties we impose on our transaction processing:

\noindent
\textbf{Monotonicity.}
Message delays and retransmissions can cause operations to be processed concurrently.
We must ensure transactions are processed in the correct order,
and that each component instance can only commit the next unacknowledged transaction
to avoid stale effects and inconsistency. 

\vskip 4pt
\noindent
\textbf{Atomicity and Idempotence.}
Component instances modify local session state in isolation, and they install shared session state changes atomically.
When processing a transaction, if a component instance fails or detects at commit time that another instance has completed the transaction, its local effects are not externalized, so atomicity is preserved.
A component instance may produce a side effect request for a transaction that fails to commit, but side effects are idempotent since they themselves are locally atomic and monotonic.

% stutsman: Replaced with hopefully simpler Atomicity and Monotoncity properties.
% \vskip 4pt
% \noindent \textbf{Idempotent operations.}
% In some cases, a component instance may fail after processing a message and updating the session store but before it can successfully acknowledge the change.
% It is also possible that multiple component instances could process a message for the same transaction at the same time.
% The component must produce the same response to all messages for a specific transaction, and it must produce the same client-visible state; that is, operations must be idempotent.
% 
% \vskip 4pt
% \noindent \textbf{Shared state monotonicity.}
% Arbitrary message delays and retransmissions can cause operations to be processed concurrently.
% We need to ensure that transactions are processed in the correct order,
% and that each component instance only processes a message associated with the next unacknowledged transaction
% to avoid stale information and inconsistency. 


%% BR: We now discuss this in detailed design 
%% This eliminates most problems from reordering and retransmission,
%% but two instances of the same component can process messages (even from different transactions) concurrently.
%% To make this safe, after a component instance processes a message, it must synchronize with other component instances.
%% Components instances do this by tagging all updates to shared session storage with the corresponding transaction ID and instructing the store to reject updates that are inconsistent with FIFO order.
%% Whenever an update to the session store is rejected it means another component has already processed the message and produced the correct effect, so the offending instance drops its stale local state for that client.


% BR: Actually, I realize this is not true from our idempotence example below, but we don't need this property
%% \xxx{stutsman: Need to weave this point into the above this.}
%% State transitions described by core protocols only depend on the initial state and the input message.
%% In our case all the state is shared, so any component will produce the same end state when triggered by the same message.
% BR: we discuss it later, no need to muddle up here
%The only exception is client state which gets updated by messages from components.
%We make sure this is updated only once by adding IDs to these messages as well, as explained in Section~\ref{XXX}. 


\vskip 4pt
\noindent {\bf Eventual completion.}
Each transaction and all of its suboperations must be processed eventually. 
A component failure shouldn't be visible to the user device
(unless there is a major outage whose scale we don't address).
Instead, the entry point persistently retries until an operation completes.










\subsection{Design overview}

%\xxx{K: may be discuss Figure~\ref{fig:overview2} as part of this
%section then, use Figure~\ref{fig:solution_overview} for an execution example
%(which is what it is).}
\begin{figure}[t]
  \centering
  \includegraphics[width=0.7\columnwidth]{echo/figs/overview2.pdf}
  \caption{\echo{} Overview}
  \label{fig:overview2}
\end{figure}

Figure~\ref{fig:overview2} depicts an overview of \echo{}.
Each control plane component (Components 1, 2 and 3)
is replicated (Instances 1~to~$n$) behind a
data center load balancer (LB)~\cite{ananta,MS_loadbalancing}.
Each component instance is refactored into
a stateless processing frontend paired with a
high availability persistent storage backend that
maintains state for all replicas (and all components).
This allows quick replacement of a malfunctioning component and scaling based on demand.

\begin{figure}[t]
  \centering
  \includegraphics[width=0.8\columnwidth]{echo/figs/overview.pdf}
  \caption{\echo{} transaction example.}
  \label{fig:solution_overview}
\end{figure}

Figure~\ref{fig:solution_overview} shows steps in a transaction in \echo{}. 
At step (1), a transaction from client is queued at entry point. 
Step (2), the transaction is forwarded to MME (but fails to be processed). 
The agent resends the request after a timeout at step (3).
The retry at step (3) triggers side effects at steps (4,5).
The side effect at step (4) triggers another side effect at step (6).
Component 3 acknowledges the side effects (5,6) by ack (7,8).
Component 2 after receiving the ack for request (6) sends an ack (9) that 
acks request (4).
Component 1 then send ack (10) to ack request (1).
When generating request (6), component 2 generates a timeout event for this request and send it to the agent at step (11).

%In \echo{}, operations between components (i.e., side effects from \S\ref{sec:corehighlevel})
%are idempotent.
Requests from mobile
devices are issued as transactions
via an entry point at the eNodeB (Figure~\ref{fig:solution_overview}).
%We exploit
%the fact that this ``device proxy'' is a necessary
%reliable component to simplify our design (\S\ref{sec:observations}).
Each transaction gets a unique, sequential ID from the entry point, and it is queued until completion. 
The transaction ID is used by components to order updates to shared state.
A component acknowledges a transaction to its upstream component once all of its downstream messages are acknowledged.
Acknowledgment to the entry point means the transaction has been applied at all components.
After a timeout, the entry point retransmits the pending transaction until it is acknowledged, only after which will it move to the next transaction.


%Figure~\ref{fig:solution_overview} illustrate various interactions on an example.  






%% \subsection{Example execution}


%% The first state transition is triggered by a message (1) from a client.
%% This message is queued at the entry point (eNodeB) and forwarded to component 1 (2).
%% In this example component 1 fails while processing the message, so the entry point doesn't receive an acknowledgement and it resends the message after a timeout (3).

%% This message further triggers messages (4) and (5) as side effects, which cause state changes at components 2 and 3 respectively.
%% Message (4) further triggers message (6) to component 3. It is important to notice that the order of arrival of message (5) and (6) depends on the network latencies and is ambiguous by system design, hence we do not need to enforce any particular ordering. We only need to guarantee that no message from a transaction prior to message (1) will be executed once transaction (1) has started.

%% Once component 3 processes messages (5) and (6) it will send acknowledgements (7) and (8) respectively. Component 2 will send an ACK (9) for message (4) only upon reception of (8). Similarly, component 1 will send ACK (10) only upon reception of (7) and (9). This way, if any of the acknowledgement is missing no downstream acknowledgements will be created and ultimately a timeout will trigger a retransmission of (1). 

%% Finally, in this example component 2 further triggers a timeout (11). This is the state-machine level timeout that happens because an expected state transition has not occured in time (and not because any low-level message was missing). This timeout is sent to the entry point first, where it will be stamped with a transaction ID and queues for transmission. This way we guarantee that all further state changes across components, caused by this timeout, will be executed atomically and not interleaved with another possibly overlaping transaction coming from a client or another timer. 









\subsection{Entry point design}
\label{sec:entry-point}

The entry point is a thin software layer deployed on an access point with a simple API.
The number of users served by each entry point is limited by its wireless resources, so it need not scale. 
The entry point API provides the following.

\noindent{\bf Eventual completion:}
Each transaction starts with a message to the entry point, either by a client or a timer message.
The entry point assigns a transaction ID to the arriving message. 
The ID is sequential for each client, but different clients have independent ID sequences.
The message is queued locally and forwarded to the next component (the MME).
A timeout is also associated with the message;
if it expires before the entry point gets an acknowledgment, the message is resent and the new timeout is set.

\noindent{\bf Timers:}
The entry point allows components to set and cancel timers.
To schedule a timer, a component sends a message with an ID of a user that the timer applies to,
a unique timer ID, and a time after which the timeout should be triggered; the transaction ID of the event is returned.
To cancel a timer event, a component sends a message with the user ID and the previously returned transaction ID of the timer event.

\noindent{\bf State coordination with clients:}
Since transaction IDs are added (and removed) at the entry point, unlike components, client devices cannot rely on them to reliably receive correctly ordered responses.
A failed state update at a component may produce a message that is sent to a client, and
a retry may produce another copy of the same message.
This must be handled by the entry point.
Each client-bound reply is labeled with a transaction ID and the sequence number of the message withing the transaction.
The entry point ignores replies that have already been forwarded to the client.
Transaction retries always produce the same responses, but it is important that one and only one gets forwarded.
Necessary reliability means the client and the entry point can be
expected to maintain a single, ordered, reliable connection (e.g., TCP
connection), which safely deals with message loss on the last hop as long as the
entry point correctly orders replies.

\noindent{\bf Handovers:}
Occasionally, a client moves between two eNodeBs and requires a handover.
Another entry point must handle the client's operations, so state must be transferred between the entry points.
This state is very light; it consists of the contents and ID of the last processed transaction
and any registered timers or control packets.
The handover procedure is augmented so the old entry point sends the context to the new entry point,
which becomes the anchor for the client once the procedure is finished.






\subsection {Cloud component design}

Algorithm~\ref{alg:transaction} shows how components process transactions.
It is designed to overlay nicely with a typical transaction processing in EPC components
(lines~\ref{lst:line:update_session_after_req},~\ref{lst:line:send_side_effect},~\ref{lst:line:receive_side_effect_reply},~\ref{lst:line:update_session_after_side_effect}, also in red).
Figure~\ref{fig:service-request-echo} shows how \echo{} MME processes a message in a Service Request procedure, with additional operations/information highlighted in red.
We next discuss how it achieves properties described in Section~\ref{sec:tran_prop}. 

\begin{algorithm}
  {\small
    \caption{Transaction processing on cloud components}
    \label{alg:transaction}
    \textbf{Input event:} \label{lst:line:input}
    Receive a request from eNodeB's agent $R$, with UE's ID ($R.UE$) and transaction ID ($R.ID$).\\
    \textbf{Output event:} 
    Send reply and timeout message to eNodeB's agent.   \label{lst:line:output}

    \begin{algorithmic}[1]
      \STATE Fetch session from storage: $(session, version)\ =\ read(R.UE)$, where $version$ is version number of znode.  \label{lst:line:fetch_zk}
      \IF {$session$ not found in storage}  \label{lst:line:session_not_found}
        \STATE Create a session locally. Set $session.ID\ =\ R.ID$  \label{lst:line:create_local_session}
        \STATE Go to step~\ref{lst:line:update_session_after_req}. \label{lst:line:jump_to_start}
      \ENDIF
      \IF {$R.ID\ <\ session.ID-1$}   \label{lst:line:check_sequence_number}
        \STATE \COMMENT{Received an obsolete request}
	\STATE Return
      \ENDIF
      \IF {$session.reply$ and $session.timer$ exist} \label{lst:line:check_reply_exists}
        \STATE (Re)send $session.reply$ and $session.timer$ \label{lst:line:send_existed_reply}
        \STATE Return.  \label{lst:line:return}
      \ENDIF
      \STATE {\color{red} Update $session$.} \label{lst:line:update_session_after_req}
      \STATE Increment transaction ID: $session.ID += 1$ \label{lst:line:increment_sequence_number}
      \STATE Set transaction ID in side effect msg: $session.side\_effect.ID=R.ID$. \label{lst:line:set_sequence_number_in_side_effect}
      \STATE {\color{red} Send side effect message: $session.side\_effect$}. \label{lst:line:send_side_effect}
      \STATE {\color{red} Receive side effect reply.} \label{lst:line:receive_side_effect_reply}
      \STATE {\color{red} Update $session$.} \label{lst:line:update_session_after_side_effect}
      \STATE Prepare reply message: $session.reply$, set transaction ID in reply message $session.reply.ID\ =\ R.ID$ \label{lst:line:prepare_reply}
      \STATE Prepare timeout message: $session.timer$, set transaction ID in timeout message $session.timer.ID\ =\ R.ID$ \label{lst:line:prepare_timer_reply}
      \STATE Write session to storage: $write(session,\ version)$ \label{lst:line:commit}
      \STATE If write OK: Send reply and timeout messages: $session.reply$, $session.timer$ \label{lst:line:send_replies}
    \end{algorithmic}
  }
\end{algorithm}
  
  
  
  
\begin{figure}[]
  \centering
  \includegraphics[width=0.85\columnwidth]{echo/figs/service-request-echo.png}
  \caption{\echo MME processes a Service Request message from UE.}
  \label{fig:service-request-echo}
\end{figure}


\vskip 4pt
\noindent\textbf{Monotonicity:}
Concurrent retries of a request, can cause processing of an obsolete message at a component instance.
Without care, this could cause the state in the session store to regress, leading to inconsistency,
as illustrated in Figure~\ref{fig:fifo-violation} in \S\ref{sec:motivation}.
We use transaction IDs to filter out obsolete requests. 
Before processing a request, a component instance checks if the transaction ID of the request 
is less than the last executed transaction ID. If it is, then
the request is obsolete and is discarded (step~\ref{lst:line:check_sequence_number}).
When updating the shared session state, the component increments the transaction ID of the session (step~\ref{lst:line:increment_sequence_number}) and acknowledges
the transaction ID to the entry point.
The entry point marks the transaction as complete, and discards
all information stored for the previous transaction ID
(such as precomputed reply messages). 
In the example from Figure~\ref{fig:fifo-violation}, message \#6 would have been discarded
as its transaction ID, derived from \#1, would have been lower than the one of the last processed message \#3,
whose transaction ID is derived from \#2.

%% BR: Repetition
%% For example, in the experiment shown in Figure~\ref{fig:fifo-violation}, a retry of a \textit{Detach Request} with transaction ID 100
%% could be held for hours on a slow MME instance while other MME instances process requests.
%% Later, the UE and the control plane state could be \textit{Attached} as of transaction ID 150.
%% If \textit{Detach Request-100} comes back and executes,
%% its execution could trigger a \textit{Detach procedure} on the data plane.
%% This would result in a \textit{Detached} control plane view while the UE believes it to be \textit{Attached}.
%% The result is nearly an hour of lost service (\S\ref{sec:motivation}).


\vskip 4pt
\noindent\textbf{Idempotent operations:}
A \textit{side effect} is triggered when one component processes a transaction that generates a message to another component.
Consistency must be maintained across components despite side effects, 
but retries from the entry point 
can create multiple side effect requests. 
Without care, interleaved side effect requests could be processed in different orders and cause inconsistency.

Service Requests illustrate this (Figure~\ref{fig:service-request} and \S\ref{sec:corehighlevel}).
MME instance A receives a Service Request.
In step~\ref{lst:line:send_side_effect} of Algorithm~\ref{alg:transaction},
it sends a \textit{Modify Bearer Request} (request \#1) to the SGW component. 
An SGW instance receives request \#1, creates/installs a tunnel endpoint {\em TEID1}, stores it in persistent storage
and replies to the MME with the information. 
Meanwhile, suppose that the entry point times out and retransmits the Service Request.
Another MME instance B receives the retry and sends \textit{another} \textit{Modify Bearer Request} (request \#2)
in step~\ref{lst:line:send_side_effect}. 
Later the SGW receives request \#2, and it \textit{overwrites} and replaces {\em TEID1} with a new tunnel endpoint {\em TEID2} and replies. 
The MME ignores the second reply because it already moved to a new state when the first reply arrived.
In the end, the MME contains {\em TEID1} while the SGW records {\em TEID2}; this inconsistency state breaks the data plane. 

To keep multiple side effect requests from mutating component state,
retries of a side effect must induce the \textit{same} effect on the target component
(side effects must be idempotent).
Transactions (Algorithm~\ref{alg:transaction}) enforce this.
When a message is processed,
the response is recorded in the session store with its corresponding message ID,
so lost responses can be reproduced without repeating execution.
If a component receives a retry, it replies with the reply stored in 
its {\em session} (steps~\ref{lst:line:check_reply_exists},~\ref{lst:line:send_existed_reply},~\ref{lst:line:return}).
%This ensures that retries won't mutate the already installed state on a component.
If a component receives a request and the committed session contains a reply,
then another component instance has already executed the transaction, and
it only needs to reply. 
Since responses are recorded in persistent storage, they can be obtained by other instances,
in case the current instance crashes before replying.


%% \textbf{Handling node crashes:}
%% If a component instance crashes after commit a session without sending the reply message, 
%% a retry from the entry point will arrive after a timeout. When a new component instance receives the retry, 
%% it has no way to tell if a reply was sent successfully 
%% by the previous component or not. To prevent 
%% the component redo a transaction again, a committed transaction also 
%% contains a copy of the reply message in it (step~\ref{lst:line:commit}).
%% If a component receives a request and finds out the 
%% committed session contains a reply, this means 
%% a previous component already executed the transaction and 
%% it only needs to reply (steps~\ref{lst:line:check_reply_exists},~\ref{lst:line:send_existed_reply},~\ref{lst:line:return}). 
%% Otherwise, it will need to go through the transaction (steps~\ref{lst:line:update_session_after_req}-~\ref{lst:line:send_replies}).
%% A retry has a transaction ID equals to the commited transaction ID $-$ 1 
%% therefore it will be considered a retry.


        % BR: This is now discussed in the implementation:
        %% As multiple retries will result in multiple replies, the entry point need s
	%% to filter out duplicated replies from the component. This could be done by using the transaction ID on the agent.


%% BR: This is in essence also idempotence
%% \textbf{Prevent inconsistency of interleaving transactions:}
%% Interleaving transactions from component instances could receive 
%% the same request and execute the transaction. 
%% These component instances could mutually modify the session and cause inconsistency.
%% For example, component instance A holding a retry of a request 
%% gets stuck at step~\ref{lst:line:update_session_after_req}
%% while another component instance B goes through 
%% the entire transaction and commits the session. Component instance B 
%% is able to progress following requests. Some time later, component instance A 
%% gets unstuck and goes through the transaction and commit the session. This 
%% commit is obsolete and should have been prevented.   

Similarly, to prevent inconsistency caused by interleaved processing of the same transaction across instances of the same component,
we use atomic conditional writes provided by the persistent storage.
%(we discuss our persistent storage implementation in Section~\ref{sec:implementation}). 
When committing changes to the reliable storage (step~\ref{lst:line:commit}), 
each component instance ensures that the stored session remained unmodified 
while it was processing the transaction. If the conditional write fails, then another component instance has already processed the transaction, so
this instance discards the local session state and backs off.
This assures even though multiple component instances 
can process the same request, only one instance is able to commit the changes 
at step~\ref{lst:line:commit}, guaranteeing atomicity.
%In the previous example, component instance A cannot commit the session because component B already committed and the A should detected that commit. 












\subsection{Correctness}
\iffalse
Here we give a sketch of why \echo{} is safe even though components are redundant
and non-blocking under failure. 
%Due to space, we provide a high-level argument,
%and we leave proof to the final version of the paper.

\echo{} safety hinges on the fact that the set of states observed by \echo{}
clients is equivalent to the unreplicated protocol (note, clients only observe
\echo{} through state transitions). The unreplicated protocol processes
operations atomically, in client FIFO order, one-at-a-time, so showing that
\echo{} {\em appears} to process operations atomically, in client FIFO order,
one-at-a-time demonstrates safety.

Demonstrating that \echo{} is linearizable~\cite{linearizability} (that the system
appears to process operations atomically in a total order consistent with
client invocation and response) is nearly sufficient. Assuming \echo{} is
linearizable, it must execute operations atomically, in FIFO order because
clients never issue a new transaction until the prior one has been acknowledged
(i.e. the above total order {\em is} client FIFO order).

To see that \echo{} is linearizable, first consider a component that invokes no
cross-component side effects (a {\em leaf} component). Each instance of that
component processes each operation to completion in isolation and the
processing only affects local state. We assume that if the instance crashes,
the illusion of atomicity is preserved because the instance could not have
externalized any changes.  If an operation completes and attempts to update the
component shared state in an order inconsistent with FIFO order, then that
instance rolls back the effect, preserving the illusion of order and atomicity.
Since operations appear to be atomic and are totally (FIFO) ordered,
leaf components behave linearizably.

This argument applies to non-leaf components too, except they produce external
effects. Their safety hinges on an inductive assumption that the side effects
they induce are atomic and idempotent; leaf components are
the base case for the induction. Non-leaf component
operations remain FIFO and atomic because they only induce side effects on
components, whose operations are FIFO and atomic.

\echo{} is linearizable and it enforces the appearance of client FIFO order.
For a set of input events, all execution histories it can produce are
equivalent to some history produced by the unreplicated protocol.
\fi

\input{echo/proof_ryan}
