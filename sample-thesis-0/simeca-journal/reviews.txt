----------------------------- Summary --------------------

Reviewer 1: Weak reject
===================
Strength:
  -

Weakness:
  - Somewhat obvious architecture: avoid core, computation+storage closed to users.
  - Not comparing to other architecture: 5G, others.
  - Deal with mobility when user moves accross SDN controller.

Comments:
  - Mobility: hierachichal SDN controllers or a single controller seeing all base station?
    + If a single controller: how establising a session is lower than today's centrallized approach?
    + If not a single controller: user moves between base stations under different controllers.
  - If no centralized controller, how the current complicated session initiation could be avoided?
  - Evaluation: 
    + Fig c: why saving a few bytes matter in overhead? What is the trade-off.
    + Tag-based: per-tag state in router? limit to the number of tags?
  - Related works: 
    + Fog Computing based Radio Access Networks: Issues and
          Challenges
    + Tackling the increased density of 5G networks; the
          CROWD approach
    + Edge Cloud and Underlay Networks: Empowering 5G Cell-Less
          Wireless Architecture
  - Section 3.1: why IoT network slices need to be programmable? What kind of features would an IoT service provider offer? Might their goals conflict with teh cellular network provider's?




----------------------------------
Reviewer 2: WJ
========================
Strength:


Weakness:
  - Unfair comparisons:
    + Hardware-based components: counter example is OpenEPC software instances.
    + Attach procedure: EPC includes authentication, encryption that SEMICA does not.
    + 53% HO overhead is not justified.
  - NFV and SDN are parts of 5G architecture, not a contribution.


Comments:


-------------------------------------
Reviewer 3: WJ
================
Strength:
  - Less control and data plane overhead, low latency.


Weakness:
  - Should focus on what's unique.
  - Edge cloud as gateways is not new: cellular, data core, L4-7 services for cloud applicants.
  - Not related to other IoT works: CROWD.
  - Single SDN controller or not?




Comments:



-------------------------- Raw text ---------------------
===========================================================================
                          SOSR 2016 Review #35A
---------------------------------------------------------------------------
     Paper #35: SIMECA: SDN-based IoT Mobile Edge Cloud Architecture
---------------------------------------------------------------------------


                     Overall merit: 2. Weak reject
                Reviewer expertise: 2. Some familiarity

                        ===== Paper summary =====

Guided by the concern that there will be an order of magnitude more devices
attempting to connect to cellular networks, this paper proposes a
lighter-weight architecture alternative to today's embarrassingly centralized
cellular networks.  The architecture is an exercise in applying SDN to cellular
networks in a way that (a) permits more decision making and computation closer
to the users (so as to lower latencies), and (b) reduces communication overhead
by reducing header sizes and attachment messages.

The architecture is described mostly at a high level (as one should expect from
a short paper), but with some details about forwarding provided.  An evaluation
in PhantomNet shows the (expected though impressive) latency and computation
offloading benefits of increased locality.

The overall design strikes this reviewer as being somewhat obvious: avoid the
cellular network's centralized core by having SDN controllers perform routing
across a subset of base stations, and by pushing some computation/storage
closer to the users.  Indeed, the high-level approach is similar to prior
proposals (5G's design itself is hierarchical, and others have proposed schemes
with edge clouds: references in comments for author), yet this paper does not
compare to this myriad work.

It is further not clear to this reviewer how mobility works when a user moves
from one SDN controller's purview to another's; as this is one of the fundamental
problems one faces when trying to solve mobility in a decentralized fashion, this
reviewer expected more detail on this matter.

                     ===== Comments for author =====

Regarding mobility:

It is not clear to this reviewer how mobility works when a user moves from one
SDN controller's purview to another's.  Section 3.1 (and Figure 1) describe a
system in which an IoT service provider can obtain network slices and its own
controller, and that SDN is applied at the edge.  At a more basic level: is
there a global SDN controller that has a view of the entire network and all
base stations?  Or are there SDN controllers, each assigned a subset of the
base stations (as the latencies in Figure 5 seems to indicate)?

   - If there is a global SDN controller, how can the latency for
     establishing a session be lower than today's centralized approach?
   - And if there is no global SDN controller, what happens when a user moves
     from one controller's set of base stations to another?  I.e., how did the
     source base station in Figure 2 know at which destination base station
     IP12 was located if the source and destination base stations did not
     share a controller?

On the one hand, the paper seems to assert that there is no global SDN
controller ("smart edge, dumb core"), but it also claims it can avoid the
existing complicated session initiation; how are both of these properties
achieved simultaneously?


Regarding the evaluation: 

The evaluation describes the overhead savings by percentages, but they equate
to the order of tens of bytes.  Why does saving such few bytes matter in
overhead?  It is not clear to this reviewer what must be traded off to achieve
these savings, and if those trade-offs are reasonable: Is it worth requiring
the core to be able to maintain per-tag state in routers?  Can this state be
represented in an efficient manner that permits forwarding at line rates?  Is
there a limit to the number of tags?

This reviewer appreciated the negative result regarding the additional storage
overhead from fully distributing the mobility anchors.  But why is it that
"every base station keeps a destination matching rule even for the same
destination" (end of Section 4)?  Is there no way to aggregate this
information?


Regarding related work:

The overall idea of distributing the cellular network workload and putting
cloud resources closer to the edge (and even using SDN as a means of mediation)
have been discussed elsewhere.  This reviewer would have found it useful to
understand what features of SIMECA distinguish it from other, relatively
straightforward designs: 


   - Initiatives for moving from "cloud" to "fog" (cloud computing at the
     edge), such as:
        * Peng et al. "Fog Computing based Radio Access Networks: Issues and
          Challenges" http://arxiv.org/pdf/1506.04233.pdf
   - The CROWD project (SDN + cloud in dense wireless networks), including:
        * Sanchez et al. "Tackling the increased density of 5G networks; the
          CROWD approach" 2015
        * Auroux et al. "CROWD Dynamic Network Reconfiguration in Wireless
          DenseNets" 2014
   - The edge cloud + 5G:
        * Li et al. "Edge Cloud and Underlay Networks: Empowering 5G Cell-Less
          Wireless Architecture", 2014


Other:

Several of the references are broken, lacking an author and venue [2],
improperly formatting a group as author [5], and lacking the publication venue
[18].

Section 3.3 describes the controller receiving "a handover acknowledgement
(step 5)" -- should this not have been a handover *request*?

Section 2: "upto" should be "up to"

NBI_{np} and SBI_{np} are used in Section 3.1 before being defined.

Section 3.1 asserts that "We apply SDN to the network edge to... make [IoT
slices] programmable" -- why do they need to be programmable?  What kinds of
features would an IoT service provider offer?  Might their goals conflict with
the cellular network provider's, and if so, how would those be resolved?

===========================================================================
                          SOSR 2016 Review #35B
---------------------------------------------------------------------------
     Paper #35: SIMECA: SDN-based IoT Mobile Edge Cloud Architecture
---------------------------------------------------------------------------


                     Overall merit: 2. Weak reject
                Reviewer expertise: 3. Knowledgeable

                        ===== Paper summary =====

The paper presents SIMECA, an architecture to incorporate IoT devices to future cellular networks. SIMECA replaces GTP tunnels with lightweight tagging and introduces local breakout of IoT traffic.

                     ===== Comments for author =====

The reviewer appreciates the goal of introducing a lightweight architecture for IoT in cellular networks. Moreover, the reviewer appreciates the authors' effort to prototype the proposed architecture. 

That being said, the comparison to the current architecture is unfair at places. For example, the paper suggests that the current EPC architecture dictates the use of a small number of specialized hardware devices (i.e., S/PGW). This is not true. The counter point is that the OpenEPC software used in the paper implements all the EPC functionality and runs on commodity x86 hardware.

Furthermore, the comparison of the attach procedure in the current EPC to the one proposed for SIMECA is unfair since the EPC attach procedure includes message exchanges for authentication, authorization and encryption that SIMECA does not define even though they will be necessary in IoT scenarios. Likewise, the paper claims that SIMECA reduces the overhead of the handover procedure by 53% without providing any justification about the underlying causes of this improvement. 

Last but not least, the paper claims the use of NFV and SDN as a contribution of the SIMECA architecture even though NFV and SDN are major parts of the proposed 5G architecture.

===========================================================================
                          SOSR 2016 Review #35C
---------------------------------------------------------------------------
     Paper #35: SIMECA: SDN-based IoT Mobile Edge Cloud Architecture
---------------------------------------------------------------------------


                     Overall merit: 2. Weak reject
                Reviewer expertise: 2. Some familiarity

                        ===== Paper summary =====

Proposes an architecture for IoT services that relies on a mobile edge cloud.  The paper argues that the architecture is a good fit for IoT applications because it is lightweight in both control and data path overhead yet still operates under IoT specific constraints such as sub 10ms latency demands.

                     ===== Comments for author =====

Due to the amount of similar work in this area, in order to provide a clear contribution, this work should really be a longer paper and focus on what is particularly unique to this space.  Proposing edge clouds as gateways is not new and has been applied to many problem domains including cellular, data core, and L4-7 services for cloud applicants.  While IoT may offer unique challenges, these aren't adequately articulated relative to other proposals in this area.  That is, it isn't clear to me why CROWD or any other the other similar proposals wouldn't be sufficient.

I also found the description of the system to be unclear.  Even after multiple passes on the architecture section, I don't understand if there is a single global controller that ties together the operator infrastructure controllers, or not.  I understand that there is a hierarchy between the Device Controllers and the Operator Controller, and even here I have basic questions about scalability of the breakdown of functions and scalability.  While I don't doubt that there is a concrete architecture in the minds of the writers, it doesn't come out in the paper.

Ultimately, I think this would be better suited for a longer paper with a much more detailed description of the system, and a comparison with similar proposals.