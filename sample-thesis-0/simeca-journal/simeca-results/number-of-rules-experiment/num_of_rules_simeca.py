#!/usr/bin/python
import random
from collections import defaultdict

NUM_OF_DEVICE=100
NUM_OF_BS=3
PERCENT_C2S=0.6
PERCENT_P2P=0.4
PERCENT_INTER_REGION=0.3
BS=defaultdict(list)
C2S_RULE=2  #intra-region, +2 for each C2S attach
P2P_RULE=4  #intra-region, +4 for each P2P attach
C2S_RULE_INTER=2    #inter-region
C2S_RULE_INTER_GS=1
P2P_RULE_INTER=4      #inter-region
P2P_RULE_INTER_GS=1
NUM_OF_REPEAT=5


def setup():
    device_per_bs = int(NUM_OF_DEVICE/NUM_OF_BS)
    for i in range(0,NUM_OF_BS):
        BS[i] = []

    for i in range(0,NUM_OF_BS):
        print "Generate devices for bs %s ..." % i
        for j in range(0,device_per_bs):
            device_type = 0    #0-C2S-intra, 1-P2P-intra, 2-C2S-inter, 3-P2P-inter.
            r = random.randint(0,100)
            if r >= 60:
                device_type = 1 #P2P.
            r = random.randint(0,100)
            inter_region = 0
            if r >=30:
                inter_region = 1

            if inter_region == 1:
                device_type += 2

            BS[i].append(device_type)

def get_num_of_rules(base_station_id=0):
    total = 0
    total_gs = 0
    for device_type in BS[base_station_id]:
        if device_type==1:
            total += P2P_RULE
        if device_type==0:
            total += C2S_RULE
        if device_type==2:
            total += C2S_RULE_INTER
            total_gs += C2S_RULE_INTER_GS
        if device_type==3:
            total += P2P_RULE_INTER
            total_gs += P2P_RULE_INTER_GS
    print "# of rules for base station %s %s %s" % (base_station_id, total, total_gs)
    return total, total_gs

def calculate_total_rule():
    T = 0
    T_GS = 0
    for i in range(0,NUM_OF_BS):
        total, total_gs = get_num_of_rules(i)
        T += total
        T_GS += total_gs
    return T, T_GS

def multiple_runs():
    for i in range (0,NUM_OF_REPEAT):
        setup()
        total, total_gs = calculate_total_rule()
        print "R %s: %s %s" % (i, total, total_gs)

#setup()
multiple_runs()