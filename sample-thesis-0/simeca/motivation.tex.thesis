\subsubsection{Motivation}

With reference to the current LTE/EPC architecture~\cite{sae} 
our architecture targets three limitations 
in the current \emph{EPC core} architecture. 
I.e., limitations in the radio access (including 
power consumption dues to radio protocols) and security aspects 
are not in our current scope and will be 
considered as future works.

%more flexible deployment options 
%and improved scalability, network overhead and performance for IoT services. 
%Below we consider specific limitations of the current architecture,
%suggest architectural changes that would address the limitations
%and describe the challenges involved in realizing that.

\textbf{New service abstraction for IoT:} 
%\xxx{New service abstraction for IoT:\\
%  - C2S and P2P.\\
%  - P2P: only server inside the APN network (behind PGW) knows about the identity of 
%  the 2 ends and being able to make a P2P connection between them (e.g., NAT traversal).\\
%  - LTE Internet service or Cooperation VPN vs. having services host inside the cellular network infrastructure: connect to service.\\
%  - SDN and NFVs could enable flexible service deployment: register and make new services on-demand.\\
%}
While built according to well-defined standards,
current LTE/EPC network equipment is proprietary and closed. 
%As a result, mobile networks provide very specific services 
%without the flexibility to quickly deploy new ones, especially for third parties.
%that is not optimized for IoT devices. 
As a result, the LTE/EPC architecture simply offers a ``pipe'' service that 
connect UEs to different Access Point simecas (i.e., APNs, such as the Internet,
IMS, or other VPN networks). This leads to three draw-backs for IoT devices:
(1)~A device \emph{must} use the LTE/EPC abstraction if it needs connectivity
even when the abstraction is not optimized for its characteristic, i.e.,
LTE/EPC incurs high latency and overhead for IoT devices, (2)~devices cannot establish 
(native) peer-to-peer (P2P) communication, as the architecture does not 
offer a direct way to do it, the result is P2P connections must
be enabled by services running inside the APN networks (e.g., P2P enabled by
NAT traversal in Skype)~\cite{ryan.p2p,ghavimi2015m2m} and PGW is the only
anchor point for P2P connectivity, (3)~it is difficult to scale network resources
up/down (e.g., MME) when hundred thousands of devices are added to the network~\cite{arijit.conext.2015}.


Consider an example IoT healthcare application where 
a diabetic patient's condition is monitored remotely and 
where the administration of insulin is similarly remotely controlled
from a cloud based analytics application, taking into
account the patient's history as wells other relevant data sources~\cite{nebula}.
Realizing such a healthcare application in current
4G networks would require the application
to run as an overlay on top of ``LTE Internet service'', instead 
of having a healthcare 
service with a network architecture/protocols that better fit their requirements 
and computational resources available inside the network.

%with dedicated network 
%and computational resources inside the network. 
%In the current 
%LTE/EPC network, there is no easy way for 3rd parties to 
%define and/or deploy their service using the network infrastructure. 
%There are services inside LTE/EPC network such as VoLTE, Visual Voicemail etc. 
%However, those services are highly specialized and tight with 
%network operators/owners.  
%For example, new services such as VoLTE, Visual Voicemail are designed, 
%deployed and monitored by network operators who own the networks.
%Moreover, proprietary hardware-based implementations of
%mobile equipment results in inflexible with regards to resource management. 
%For example, when a specific network element, e.g., a PGW, approaches its capacity limit,
%new equipment need to be deployed, even if other network elements 
%have spare capacity available.
%This makes it difficult to dynamically allocate 
%new EPC functions and expose the resources.
%For example, if a PGW is overloaded, 
%the operators need to purchase a new hardware PGW which could lead to over-provisioning for the current demands.



  \emph{Proposed architecture and challenges:} 
  %We argue that 
  %to enable IoT services, network operators should 
  %manage both network and computation resources and 
  We argue that there should be a more suitable service abstraction 
  for IoT devices that should: replace the 
  ``pipe'' abstraction 
  %tunneling abstraction 
  in LTE/EPC by a new abstraction that is \emph{more suitable} for 
  IoT communication models \emph{and} at the same time
  enables service 
  %inside the network, 
  deployment directly after the Radio Access Network (RAN). 
  %The abstraction should enable devices to set up P2P connections, 
  The abstraction should be more flexible in term of 
  scaling up/down network resources and resources should be fully controlled. 
  %expose them as an \emph{IoT service abstraction} to
  %3rd party IoT service providers to deploy their services
  %and applications.
%  Specific to mobile environments, network operators should enables 3rd parties to 
%  instantiate and ``own'' a separate Mobility abstraction  
%  for their services. We call this Mobility Abstraction
%  as a Service. 
%\xxx{on-demand therefore NFV and SDN}
  
  %Such a service abstraction can be enabled by refactoring mobility  
  %functions as NFVs, cloud computing 
  %for computational resource management, 
  %and flexibly connecting end-points to 
  %services using SDN. 


  %Combining NFV, SDN, and cloud computing allows both (i) flexible new service 
%deployments and (ii) network resource management in edge cloud. A 3rd party service provider  
%could specify its resource requirements and deploy its own service on top of the infrastructure. 
%SDN provides the network programmability capability and network resource isolation for multiple services, while 
%cloud computing and NFV allows the infrastructure owner to manage computation resources 
%and dynamic allocate resources to service providers.  
%with each provider having their own network slice and compute resources which can be scaled on demand, while 
%the network operator will be responsible for resource allocation and infrastructure management.

  %\emph{Challenges:} 
  %While providing the flexibility for a programmable network 
  %and service deployments, SDN fundamentally changes  
  %the LTE/EPC network architecture and protocols. For example, 
  %there will be no S-PGW specialized network components 
  %in the data path yet SDN itself does not support mobility.
  The main challenges in realizing \simeca{}
  are defining the ``right''
  %\simeca must have an interface/
  abstraction for IoT devices to access services based on their unique 
  requirements. For example, a question to ask is: 
  what is a proper interface for IoT devices given 
  their communication models? (i.e., do they need 
  only client-to-server communications or also P2P communications?)
  Moreover, what technologies should be used to realize 
  a flexible and fully-controlled end-to-end connectivity abstraction?
  


  \emph{Solution}: \simeca proposes a new IoT service abstraction (ISA)
  based on typical IoT communication models. 
  The abstraction is realized by two separated components, one deals 
  with mobility specific procedures and interfaces with IoT devices 
  while another deals with network path realization.
  The result is that \emph{ISA} hides the complexity of supporting \emph{mobility} and \emph{connectivity} on top of a mobile infrastructure and exposes a simple interface for IoT devices to initiate both client-to-server and P2P communications. The two components of the \emph{ISA} are realized using 
  NFVs/SDN that run on a local edge cloud to enable flexible resource scaling  
  and full end-to-end control (\S ~\ref{sec:maas}).
  %re-factored mobility functions running as NFVs and SDN abstraction allows flexible resource scaling. Also, the mobility functions interact with SDN to fully control 
  %end-to-end path implementations (\S ~\ref{sec:maas}).

  %How to 
  %How could SDN be integrated with mobility functions running as NFVs 
  %to enable end-to-end connectivity for IoT devices? What is the 
  %interface between SDN and NFVs? and so on. 
  %while still supports mobility 
  %given a mobile environments?
  %The abstraction realized by SDN/NFV 
  %should be flexible enough to support both flexible end-to-end path 
  %set up and mobility.
  %Also, to enable P2P connectivity 
  %the abstraction must deal with mobility that is a nature of mobile networks.
  %3rd party IoT service
  %providers to 
  %realize/use both their mobile network infrastructure and computational resource 
  %to run their services. 
  %To do so, \simeca 
  %must refactor the current mobility functions in LTE/EPC 
  %and allow flexible deployment of those functions (i.e., using NFVs).  
  %Also, as multiple mobility abstractions are running on the same mobile network/edge cloud infrastructure, 
  %the realization of these abstractions should allow 
  %co-existence and isolation amongst service instances.
  %there must be resource allocation and isolation mechanisms for them to 
  %co-exist (section~\ref{sec:maas}).



\textbf{Heavy weight data and control plane}:
The current LTE/EPC network was designed to support devices 
streaming large amount of high quality data/voice (e.g., a smart phone streaming 
a video or making a voice call) rather than \emph{massive} IoT/M2M devices with 
sporadic, best effort traffic (e.g., millions of meters sending 
a temperature sample every 20 mins).
The result is the the current LTE/EPC control signaling associate with end-to-end connectivity causes significant overhead, 
especially for massive IoT devices.
Specifically, LTE/EPC introduces the notion of an EPS bearer in the EPC core network. 
While EPS bearers support high QoS data transmission (i.e., via GTP tunnels), 
setting up or maintaining them incurs significant overhead~\cite{kim2012directions}.
For example, a standard LTE initial attach procedure incurs up to 28 control messages, while 
a service request procedure incurs up to 14 control messages ~\cite{3gpp23401}. 
%This control overhead is due to the cost of maintaining tunnels in 
%the current LTE/EPC networks.
Additional control overhead comes from the combined
effect of maintaining tunnels (for EPS bearers) 
and dealing with devices
going idle to safe power consumption.
When a device goes idle, the base station 
releases radio resources and the EPC core network 
releases the device's corresponding data/control paths 
(i.e., S1-U/C tunnels between eNodeB-SGW and eNodeB-MME)~\cite{nas.protocols}. 
When the device has data to send or there is downlink traffic to the device, 
it needs to set up the tunnel and control channel   
again before being able to send/receive data. 
This results in extra control signaling overhead and latency (i.e., ``Service Request'' and ``Paging'' procedure). 
This control signaling is a significant factor contributing to the load in MMEs: 
paging generate 28.7\% of load in MMEs; during peak hour, a device could generate 45 service requests 
which translate to nearly a thousand control messages (i.e., 61.9\% of load) in MME~\cite{mmeload}.

%In the other hand, many IoT devices such as sensors or actuators might be stationary. 
%With sporadic traffic, those devices require low control overhead over mobility. Therefore, extra 
%overhead due to mobility assumption could affect devices efficiency.

%     \begin{figure}[]
%        \centering
%        \includegraphics[scale=0.5]{simeca/figs/overhead-problem-epc.pdf}
%        \caption{EPC header overhead for increasing proportion of small payload sensors}
%        \label{fig:overhead-problem}
%      \end{figure}

In the data plane, EPS bearers correspond to a large number of forwarding states 
in the EPC core: each EPS bearer (which consists of 4 GTP tunnels uplink and downlink) 
corresponds to 8 tunnel end-points (i.e., 2 at eNodeB, 4 at SGW, and 2 at PGW).
Also, the GTP tunnels inside LTE/EPC core adds data plane overhead 
to IoT traffic. This overhead becomes relatively more significant 
if the packet's payload is small, which is often the case for IoT traffic 
such as sensors and actuators. For example, a body temperature sensor 
generates 1.5 bytes of data per sample per second~\cite{sensorworkload}. 
To deliver 1.5 bytes of data, LTE/EPC incurs 36 bytes of additional tunneling overhead (i.e., GTP/UDP).
Moreover, as the RAN capacity is expected 
to dramatically increase with emerging access technologies in 5G~\cite{ngmw},
increases in the amount of traffic would exacerbate the problem. 
Figure~\ref{fig:overhead-problem} shows the packet header overhead 
incurred in LTE/EPC core network as a function of the percentage of small payload (i.e., 2-Byte and 20-Byte) traffic. 
The packet header overhead could be significant if the portion of small payload traffic increases. 
For example, if 90\% of the traffic consists of large packets (e.g., smart phones 
streaming videos with 1400B TCP packets), the EPC core incurs only 7\% of packet overhead.
However, if 90\% of the traffic consist of small packets (e.g., meters sending 2B  temperature samples 
to a server), the overhead increases to about 47\%. 

  \emph{Solution:} 
  \simeca proposes a \emph{light-weight IoT control and data plane} to realize the
  IoT service abstraction (ISA). 
  Specifically, the \simeca design eliminates the notion of bearers in the EPC core network. 
  Instead, \simeca proposes to use light-weight, best-effort data forwarding in the data path right after the RAN. 
  \simeca leverages an intelligent-edge dumb-core principle: packet classification 
  happens only at the base stations while the dumb-core network simply forwards 
  the packets.
  Instead of adding tunneling header into packets, \simeca's packet
  classification scheme \emph{translates/replaces} packet header at 
  the network edge (i.e., base stations) for forwarding, thereby reduces packet 
  header overhead (\S ~\ref{sec:data}.)

  By eliminating tunnels inside its network, 
  \simeca reduces the control plane signaling involving end-to-end connectivity. 
  Removing tunneling also reduces the number of fine-grain forwarding states 
  (i.e., tunnel IDs) in the network. 
  Moreover, \simeca sets up the end-to-end data path using OpenFlow control 
  messages that are slightly more light weight than bearer modification control
  messages in EPC which are loaded with fine-grain options (\S ~\ref{sec:control}.)

  %The data forwarding scheme is designed to reduce number of forwarding states and 
  %packet header overhead.
  %and control signaling involving end-to-end connectivity. 
  %The control plane leverage interactions between virtualized mobility functions (NFVs) and SDN to reduce the amount of control signaling involving end-to-end connectivity.
  %We propose to leverage SDN flow modification messages to realize a light-weigth control plane 
  %(i.e., set up paths in the core network and mobility functions such as ``Device Attach'', ``Device Handover'', etc). 
  %Our control plane sets up 
  %paths in the core network by using SDN flow modification messages.
  %Further, our protocol design eliminates the notion of bearers/tunnels in the core, therefore 
  %eliminating the signaling overhead to setup/maintain them. 
%(i.e., eliminating the need for 
%GTP-C messages to setup/maintain tunnels between S-PGW and base stations.)
  %In the data-plane, instead of tunneling, 
  %Our design propose a address translation forwarding scheme 
  %that is more light-weight than tunneling while support mobility. 
  %and an inter-region routing protocol. 
  %Our design also enables peer-to-peer direct 
  %communications.
  % to reduce peer-to-peer communication latency.

%integrating lightweight mobility 
%functions running as NFVs and SDN to reduce signaling overhead. 
%The control plane 
%The interaction between mobility functions and SDN allows our control plane 
%to monitor and switch paths according to mobility events such as device attachments or 
%handovers.
  
  %\emph{Challenges:} 
  %Besides supporting  
  %similar functionalities as in LTE/EPC, 
  %Designing a low overhead data plane and control plane 
  %while retaining mobility functionalities such that handover is challenging: 
  %\simeca data and control plane needs to 
  %assure seamless mobility and routing 
  %in a mobile environment 
  %with less packet header and number of forwarding states (\S ~\ref{sec:data}). 
  %Compared to EPC, the control plane needs to preserve and \emph{extend} the API 
  %(e.g., enable P2P communications) with less control signaling overhead (\S ~\ref{sec:control}).

  %and scalability for inter-region routing. 
  %The data plane and control plane should also support scalable peer-to-peer communications for IoT devices.
  %\xxx{peer-to-peer challenge here}
  %Given a highly distributed architecture and device-to-device communication model, 
  %\simeca data plane needs to optimally allocate anchor points to reduce forwarding states 
  %in the base stations (Section~\ref{sec:scalable-route}).

    \begin{figure*}
      \begin{subfigure}{.33\textwidth}
        \centering
        \includegraphics[height=4.5cm, width=5cm]{simeca/figs/packet-header-analysis.pdf}
        \caption{EPC header overhead with increasing proportion of small payload traffic}
        \label{fig:overhead-problem}
      \end{subfigure}%
      \begin{subfigure}{.33\textwidth}
        \centering
        %\vspace{-0.15in}
        \includegraphics[height=4.5cm, width=5cm]{simeca/figs/arch.pdf}
        \caption{Mobile Edge Network \& Cloud Infrastructure}
        %\vspace{-0.15in}
        \label{fig:framework}
      \end{subfigure}
      \begin{subfigure}{.33\textwidth}
        \centering
        \includegraphics[height=4.5cm, width=5cm]{simeca/figs/IoT-abstraction.pdf}
        \caption{IoT service abstraction (ISA)}
        %\vspace{-0.15in}
        \label{fig:mobility-abstraction}
      \end{subfigure}
    \caption{EPC header overhead, \simeca architecture overview}
    \label{fig:results}
    \vspace{-0.15in}
    \end{figure*}


%\begin{itemize}
\textbf{Centralized deployment of core mobile network functions:} 
In current LTE/EPC networks, core mobile network functionality,
e.g., data-path elements that forward traffic to the Internet (i.e.,
SGW and PGW), as well as control plane elements dealing
with mobility (i.e., MME) and policy (i.e., PCRF),
are specialized hardware-based equipment or virtualized NFVs  
%, and 
%highly specialized. 
%They are typically proprietary and expensive and 
deployed in a limited number of physical locations. 
%Since all traffic must go through the PGW to reach the Internet or 
%other UEs, t
Traffic between endpoints need to travel to these centralized 
locations, 
%even if the communication is with an endpoint
%that is physically near by. This 
adding extra latency to the end-to-end
path.
% either UE-to-server or UE-to-UE communications. 
%On the other hand, t
The vision for 5G networks~\cite{ngmw,5gvision} calls
for services that will require significantly lower latencies (e.g., $<$5ms for 
tactile Internet), which the current LTE/EPC architecture will not be
able to support.

On the other hand, IoT/M2M devices are mostly static or have a low degree 
of mobility~\cite{m2mworkload,ngmw}. This gives an opportunity 
to move away from a centralized network architecture (e.g., a large geographical area shares a PGW) 
to a more distributed architecture with computation and 
connectivity mostly local (e.g., spanning a metro area). A more ``local'' 
environment would allow local deployment 
of computational resource (i.e., services) and therefore reduce end-to-end latency. 
%That being said, a more ``local'' environment would also mean 





%\xxx{
%- IoT/M2M devices communications are expected to be mostly local, therefore the need of local deployment of 
%computation and abstraction.\\
%- Trade-off between local region (fast and low overhead) and scalability: need multiple regions and inter-region communication 
%mechanism.\\
%}

%\xxx{Why not EPC NFV closed to the edge? 1) don't have or limited programmability - different services 
%does not have network resource isolation. eg, network isolation across EPC instances, 
%2) This is because services and network are not closely integrated which 
%causes extra management overheads}


  \emph{Solution:}  This suggests a more distributed architecture where 
  core mobile network functions, as well as compute resources, are placed closer to the mobile network edge, i.e., 
  a \emph{mobile edge network and edge cloud} architecture. The \simeca{} architecture 
  consists of regions, each covers a relatively small geographical area.
  The distributed 
  edge cloud and edge network bring up multiple benefits: (1) mobility  
  functions and services are deployed closer to users that reduce network latency, 
  (2) a more local environment allows 
  optimizations in the control/data plane designs to reduce overhead and signaling.
  %Specifically, we argue for
  %the mobile edge architecture where \emph{both} the network and the cloud
  %use ``software defined'' technologies, i.e., SDN and NFV and cloud, to provide the needed flexibility.
  %In the other hand, in order to scale to a large geographical area (e.g., a country), regions should that are interconnected.
%With NFV, core mobile network functions could be deployed on common compute infrastructure~\cite{ocp} 
%close to the edge, i.e., an edge cloud. This edge cloud also consists of 
%other computation nodes that host services.

  %\emph{Challenges:} 
  While distribution helps increase the 
  availability and network latency, it also
  requires proper interaction mechanisms 
  between regions when devices move across regions. 
  For inter-region communications
  \simeca uses gateway switches (GS) at the border 
  of regions to tunnel inter-region traffic. 
  Once traffic reaches the border GS, it is forwarded 
  using SDN-controlled paths. \simeca proposes 
  a mechanism to select GS to minimize the number of SDN 
  rules within a region (\S ~\ref{sec:inter-region}.)
  
  %A more distributed architecture 
  %would consist of hundreds of thousands 
  %of SDN switches and thousands of computation nodes at the edge. 
  
  %The control plane now consists of multiple 
  %SDN controllers each controls a smaller 
  %set of resources (region). The question is how do 
  %they interact with each other and with the mobility functions 
  %to provide connectivity across regions in a scalable manner(\S ~\ref{sec:control})?.

  %A single SDN controller is 
  %obviously not sufficient for such a large environment. A natural 
  %approach is to divide the edge into multiple regions each controlled by a SDN controller.  
  %This would require a scalable inter-SDN domain interaction mechanism that 
  %support intra and inter domain mobility and communication (\S ~\ref{sec:control}).

%This would benefit the end-to-end latency: (i) reduce latency 
%for traffic from UE to services located in the edge cloud, (ii) reduce latency 
%for UE-to-UE communications.
%Each bearer/tunnel in the EPC core corresponds to a 
%forwarding state (GTP tunnel ID) in the eNodeB, SGW, PGW. 
%With a large number of IoT devices, the increasing amount of 
%states adds significant load onto the EPC components. 
%To reduce the network state associated with legacy LTE/EPC,
%we use smart SDN access edge at eNodeBs 
%and a dumb SDN forwarding core. The access edge specifies 
%a forwarding tag in the packet header while the forwarding core simply forwards the 
%packets by matching on this tag.
%This allows us to (i) reduce the number of SDN rules on 
%the access edge compared to EPC tunneling and (ii) 
%keep the number of SDN forwarding 
%rules inside the forwarding SDN core bounded.


%LTE/EPC 
%encapsulates and decapsulates IP packets for forwarding 
%inside the EPC core. This 
%GTP tunneling overhead could be significant 
%for IoT services. For example, a body temperature sensor 
%generates 1.5 bytes of data per sample per second~\cite{pereira2014towards}. 
%To deliver 1.5 bytes of data, the LTE/EPC incurs 36 bytes of additional tunneling overhead (i.e.., GTP/UDP).
%Given a LTE RAN with 100Mbps bandwidth, this 24x overhead could generate 2.4Gbps 
%of bandwidth in the core network. 
%With the expected increase in %RAN capacities in the future 5G access, 
%the number of IoT 
%devices and the amount of IoT traffic, this packet overhead problem could add 
%significant pressure to the transport infrastructure.
%To address this, our data plane uses light-weight tagging as compared to GTP 
%tunneling to reduce data plane overhead for IoT traffic. 

%\end{itemize}


%In summary, the fundamental question we try to answer in this work is how to provide i) ``scalability'' and ``flexibility'' for massive IoT %devices/services, ii) with ``a wide spectrum of device types/service requirements''.
