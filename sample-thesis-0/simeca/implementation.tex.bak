\section{Prototype implementation}
\label{sec:prototype}
%  \xxx{
%    - SDN-based base station: OAI\\
%    - Control plane: MF, MC, Location table:\\
%      + MF: OPENEPC.\\
%      + MC: Ryu-controller with REST API.\\
%  }



  \textbf{SDN-enabled base station:} 
  Figure~\ref{fig:oai-basestation} shows our base station architecture. 
  We implemented the SDN-enabled base station (S-BS) by combining a node
  running a refactored OpenAirInterface (OAI) eNodeB implementation~\cite{OAI},
  with a node running Open vSwitch (OVS)~\cite{OVS}.
  We reuse OAI radio stack and radio control plane (i.e., RRC, PDCP, RLC, MAC/PHY) and 
  modify the core network data path. Specifically, on the uplink 
  direction, the radio stack PDCP (Packet Data Convergence Protocol) task associated with a radio bearer, places IP packets in an 
  Intertask Interface (i.e., ITTI) queue. A rawsocket task reads 
  the queue and forwards IP packets 
  %to the kernel's IP routing system 
  %to send via the base station's NIC. There is a default IP route 
  %in the IP route system that forwards packets 
  to the node running the Open vSwitch, which in turn connects to the SDN edge network. 
  Similarly, downlink packets from the rawsocket task will be 
  placed in the ITTI queue. There is a relay demultiplexer 
  that keeps the mapping between radio bearers (or radio task) %(i.e.., RNTI number) 
  and packets destination IP addresses (the mapping is recorded during 
  bearer setup and device attach/handover). This relay associates the downlink 
  packets to corresponding PDCP radio tasks based on the packet destination IP address.  
  The mobility SDN controller interacts with the base station Open vSwitch 
  via the OpenFlow protocol. 
  Note that this SDN-enabled architecture could also co-exist with conventional 
  LTE/EPC eNodeBs as it has similar relay functionality in the data path.
 % With the relay, it could distinguish LTE/EPC bearers from \simeca bearers 
 % and forward packets to the corresponding task (i.e., LTE/EPC's GTPU task or \simeca's raw socket task for uplink traffic). 
 % For LTE/EPC bearers, similarly the mapping between PDCP-GTPU task 
 % could be set up during LTE/EPC device attachment. 


  \textbf{Mobility Function component:} We implemented the MF component 
  by refactoring an OpenEPC~\cite{openepc} MME implementation. The MF reuses 
  the MME authentication functionality\footnote{Generally, it is dedicated to
  IoT services so any IoT-optimized authentication protocol can be implemented
  for specified IoT services, e.g., SSTP \cite{sstp} for smart grid.} but implements \simeca{}'s simplified
  device attach and handover 
  procedures: The MF shortcuts the attach and handover 
  procedure by removing control messages to setup/modify tunnels 
  at SGW and PGW. When it receives an ``Attach request'',
  the MF notifies MCs to install an end-to-end path 
  and sends ``Attach accept'' to notify the device when
  SDN setup is complete.
  For communication between MF and MCs, we have designed RESTful APIs.
  % which can be easily extended for additional functions.

  \textbf{Mobility SDN controller:} We implemented the Mobility SDN controller 
  using Ryu controller. MCs constantly listens for HTTP  
  requests from the MF and installs SDN rules onto SDN-enabled base stations accordingly.
  For intra-region routing we used shortest path forwarding protocol matching 
  on IP prefix. In our implementation, we used IP addresses for \simeca{} device identity. 


    \begin{figure}[tb]
    %  \begin{subfigure}{.38\textwidth}
        \centering
        \includegraphics[height=5cm, width=7cm]{simeca/figs/evaluation-setup-regions.pdf}
        \caption{\simeca prototype implementation}
        %\vspace{-0.1in}
        \label{fig:prototype}
    %\end{subfigure}
    %\caption{Forwarding in EPC and \simeca, implementation and evaluation}
    %\label{fig:implementation}
    \vspace{-0.15in}
    \end{figure}


%  As shown in Figure~\ref{fig:prototype}, \simeca IoT controller
%  consists of a lightweight MME (MME$^{Lite}$) and a Ryu SDN controller.
%  We used Open vSwitch
%  as software forwarding switches for the IoT edge cloud.
% Our programmable base station is implemented using two components: the current OpenEPC implementation of eNodeB and an OVS switch
% The OVS switch plays the role of the access edge switch and provides the mobility anchor point.
% Our implementation follows the design described in Section~\ref{sec:architecture-overview}. We provide
% relevant implementation details below.

  


\iffalse
  \begin{figure}[]
    \centering
    \includegraphics[scale=0.5\textwidth]{simeca/figs/prototype_eval_regions.pdf}
    \caption{\simeca prototype implementation}
    \label{fig:prototype}
  \end{figure}

  \textbf{Data plane:}
\iffalse
    \textbf{Access edge:}
  %On the data plane, \simeca prototype consists of
  %emulated client node, emulated eNodeB and Open vSwitch.
  On the uplink, the IP packets encapsulated using the GTP Tunnel ID and arriving at the base station from the radio side are decapsulated
  at the Access switch. The uplink GTP Tunnel ID is obtained from the ``Attach accept'' message sent from the IoT controller during device
  attachment process. The ``Access switch'' decapsulates GTP packets to get IP packets and forwards to the SDN network after making the necessary
  packet modifications \footnote{Since tagging requires extra overheads in Phantomnet \cite{phantomnet} we use header modifications rather than attaching tag ID to the IP packets.}.

  On the downlink, IP packets arriving at the ``Access switch'' from the SDN network
  will be encapsulated using on the downlink the GTP Tunnel ID that the Base station
  provided in the ``Attach request'' message. The GTP encapsulated downlink
  packets will then be forwarded to the base station which delivers the packet
  to the corresponding radio bearer to reach the IoT device.

  \textbf{Packet forwarding:}
  \fi
  We implemented the forwarding scheme in \simeca using IP header modification.
  Each base station has a unique station ID and each device under that station has a unique device ID. Inside the network core,
  packets are forwarded using the station ID and shortest path forwarding.
  At the access edge, packets are processed using device ID.
  For example, an IP header in \simeca prototype looks like
  192.168.1.11/24 where 192.168.1.0/24 identifies the base station and
  11 identifies a device in that base station. Forwarding switches in
  the core forward packets using the station ID (e.g., 192.168.1.0/24).
  %When the packets arrive at the eNodeB, the access switch
  %determines which UE the packets heading to by looking at the device ID field (e.g., 11).

\iffalse
  For client-to-server forwarding in the uplink direction, the access switch modifies the IP header of the packet
  to \emph{station-ID.Device-ID}. This IP packet is sent to the forwarding core
  where forwarding is done using destination IP (e.g.,\emph{server-IP}).
  For downlink, forwarding core simply uses the destination address (e.g., \emph{station-ID})
  in the IP header to reach the base station. The access switch then translates the destination IP of the downlink packet
  back to the actual IoT device's IP address and delivers to the IoT device.


  For peer-to-peer forwarding, the difference is that
  the access switch modifies both destination IP and source IP.
  Destination IP is also \emph{station-ID.Device-ID} where \emph{station-ID}
  is target Base station's ID and \emph{Device-ID} is target IOT device's ID.
  The IoT controller keeps track of the IoT device's IP and \emph{station-ID.Device-ID}
  in its tag table (see Section ~\ref{subsec:control}). Similarly in the reverse direction
  for P2P, the destination IP address is translated back to IoT device's IP address.
\fi

\textbf{Handover:}
%  In \simeca prototype implementation, when a source IoT device moves to a new base station,
%  the target base station allocates a new \emph{station-ID.Device-ID} for that IoT device.
%  However, on the uplink direction the IoT device now has a new identifier which causes
%  the server to drop the on-going connection thus the handover is not seamless.
  To realize seamless handover, the IoT controller assigns a unique
  \emph{station -ID, Device-ID} to each IoT device (i.e.., when it first attaches to
  the network). This identifier is unchanged regardless of the attachment point
  of the IoT device. When the IoT device has a handover, the IoT controller installs rules
  to translate new assigned identifier to the unique identifier on the uplink
  direction. In other words, the source address of uplink packets need to be modified to the unique identifier \emph{station-ID.Device-ID}
  so that it appears the server always communicates with the same devices.
  The downlink packets can be directly routed to the target base station with a proper header modification at server and target access switches but its source address is unchanged
  (i.e.., server's IP address).

%  \begin{figure}[]
 %   \centering
  %  \includegraphics[scale=0.7]{simeca/figs/p2phandover.pdf}
   % \caption{P2P handover scenario}
    %\label{fig:p2phandover}
  %\end{figure}

%  Similarly, if an IoT device has a handover when it is in a Peer-to-Peer connection,
%  the destination address translation happens on the forward direction at the target base station.
%  In the return direction the packets are simply rerouted to the target base station with a proper header modification at the destination base station.

  \textbf{Control plane:}
  The \simeca prototype adopts a simplified EPC
  control plane. \simeca has a lightweight
  MME (MME$^{Lite}$) running on a compute node performing mobility procedures with the
  eNodeBs using the same interface and message structure
  as in legacy LTE/EPC \cite{s1ap.3gpp}: attach request, attach accept, handover
  request etc with the control messages going through S1AP interface
  on top of SCTP (1 in figure~\ref{fig:prototype}). Unlike the
  LTE/EPC MME, MME$^{Lite}$ performs
  the protocol described in section~\ref{subsec:control} where no bearer modification/creation messages are involved. \simeca's SDN controller listens to the S1AP interface
  between base stations and the MME$^{Lite}$ to capture S1AP messages
  during attach/handover events. It then sets up/modifies SDN paths
  as described in the protocol (step 2 in figure~\ref{fig:prototype}).
\fi
