\subsubsection{Introduction}
\label{sec:intro}

Future networks, including future mobile networks (i.e., 5G), 
will need to support a large number of Internet-of-Things (IoT) devices;
around 26 billion by 2020 according to some estimates~\cite{gartner}.
Most existing IoT devices are using short-range radio access technologies,
e.g., IEEE 802.15.4, but emerging IoT devices are expected to also use cellular
radio access technologies to take advantage of mobile network features, such as
wider coverage, better support for mobility, well-managed and secured~\cite{5gvision}.
The massive number of devices implies a fundamental question of how well
the current cellular architecture would support IoT and what
might need to change to both IoT applications, without adversely
affecting the network.
%These numbers imply not only a massive increase in the number of devices,
%but also a variety of IoT use cases, with a wide spectrum of service requirements
%when compared to the primary capability of current mobile networks, i.e., mobile broadband access.
%Most existing IoT devices are using short-range radio access technologies,
%e.g., IEEE 802.15.4, but emerging IoT devices are likely to also directly use cellular
%radio access technologies to take advantage of mobile network features, such as
%radio coverage, resource management, security, and mobility~\cite{5gvision}.
%This will be accelerated if new 5G air interfaces, e.g., Universal Filtered Multi-Carrier (UFMC)
%as a new waveform, can optimize cellular radio access for sporadic traffic generating devices~\cite{ufmc}.
% UFMC: http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=6877912&tp=?ALU=LU1024832

Details of the 5G mobile network architecture are still emerging and
some aspects are expected to be radically different from the current
4G architecture~\cite{what5g}. Nonetheless, 5G will be
informed and derived from 4G principles and approaches, and as such 
the 4G architecture forms a pragmatic base from which to explore future mobile network
architectures~\cite{softmow,softcell,Banerjee15Efficient,arijit.conext.2015,nfv.sdn.lte}.
In this work we follow a similar approach by considering the limitations
of 4G networks, in particular the EPC core network, 
in supporting IoT and then combining evolved
and refactored 4G mechanisms with software-defined networking (SDN)
and network function virtualization (NFV) to realize an IoT friendly
mobile (core) network architecture.
 
4G networks were designed to run as an overlay service on top
of a packet based substrate. Despite the fact that this is a completely
packet based approach, the resulting architecture is heavy-weight and
optimized for human-to-human and human-to-machine communication.
%overly sophisticated and does not effectively support emerging IoT services.
%For example, LTE/EPC networks are based on GTP tunnels over UDP and
%providing Internet access to a mobile device (user equipment (UE)) 
%involves the creation of multiple tunnels, each of which corresponds to per-device
%state in network elements, e.g., eNodeB, SGW, PGW.
Therefore, the 4G architecture works well for smartphone
applications that  generate relatively high volumes of traffic. 
This is, however, a poor fit
for IoT devices, where we might expect 
a much larger number of devices with different traffic patterns.
%much more diversity
%in terms of traffic volume. 
For example, 
%IoT devices 
%can span the range
%from 
millions of simple sensors that might sporadically send only a few bytes of data 
every half hour. 
Such applications might 
prefer low overhead over high quality data delivery.%to
%devices that support augmented reality or tactile Internet scenarios
%and might generate significant volumes of traffic.


Further, the control signaling in 
4G networks has grown significantly and already 
present significant pressures on network components even 
just to support conventional devices and services. Control traffic 
grows 50\% faster than data traffic while generates 
no revenue~\cite{signalgrowth.nokia}. MME (a main control entity in LTE/EPC 
network) already experienced up to 1500 messages per User Equipment (UE) 
per hour under adverse conditions~\cite{mmeload}. Scaling  
the control plane in 4G~\cite{arijit.conext.2015} 
would help mitigate the load problem. 
However, significant overhead in the protocols remains, especially when a large 
number of IoT devices arrive at the network. This suggests 
fundamental changes in both network control and data planes
in order to support IoT devices and services.

In terms of service abstractions, current 4G networks, 
 i.e., long term evolution (LTE) and evolved packet core
(EPC) networks, offer connectivity services   
for user devices to connect to 
other networks via a centralized gateway (packet data network 
gateway - PGW). Although this abstraction is suitable for 
human-centric devices where the network mostly 
needs to deliver an ``Internet'' service (e.g., web browsing), 
it is ill-suited for machine-to-machine or machine-to-human type communications. 
For example, peer-to-peer (P2P) communications (e.g., Skype application), 
which are expected to be prevalent in IoT space,  
are not natively supported by the architecture and must be enabled 
by mechanisms such as NAT traversal on the gateways (PGWs)~\cite{ryan.p2p,ghavimi2015m2m}; in current 4G networks, the network element between the 
core mobile network and the Internet, i.e., the packet data network
gateway (PGW), is a specialized and proprietary hardware platform
with poor modular support, which forces operators to deploy them in a limited
number of physical locations~\cite{kim2012directions}.
Since all network traffic must go through these centralized gateway functions,
extra latency is introduced in the end-to-end path, especially for P2P traffic whose end points might be geographically close. Also, 
since the operators offer only network connectivity service 
via their mobile infrastructure, end-services 
are typically deployed in the Internet. This hurts 
the end-to-end latency and forces IoT devices to use 
the LTE service despite the unsuitable service abstractions.



Mobile network operators are considering (or in some cases already deploying)
highly distributed small-sized data centers and clouds at the network edge~\cite{distributednfv}.
Industry efforts are also underway to deploy distributed cloud platforms 
close to the mobile edge to support cloud based radio access network (RAN)
functionality~\cite{cloud.ran}, as well as mobile edge computing efforts in support
of traffic-offloading for low latency applications~\cite{patel2014mobile,etsi.mec}.
This trend of highly flexible platforms close to the mobile network edge,
offers a new opportunity for network operators to more efficiently provide existing
or emerging services and is specifically well aligned
with emerging IoT services, some of which support an enormous number of 
devices, with low network latency requirements.
%extremely high performance metrics,
%e.g., below 10ms end-to-end latency for vehicular safety applications~\cite{ngmw}.

%Recently Telco operators have been experimenting with two key technologies: network function virtualization
%(NFV), and software defined networking (SDN).
%In the mobile networking environment, the confluence of NFV and SDN is expected to enable
%a highly adaptable and evolvable network framework to support emerging 
%services like IoT.
%We note that an SDN/NFV enabled mobile network can also enable cloud-like business
%models where third-parties could flexibly acquire network resources and run their own
%services on top of a shared infrastructure (i.e., IaaS). 
%With a more flexible mobile edge cloud model, network operators
%can provide an API which allows the service provider (or other providers) to specify their
%own IoT networks with a set of servers to run their own IoT services within the network.

Towards realizing this vision,
in this work we present our work on an SDN-based IoT Mobile Edge Cloud Architecture (\simeca).
In \simeca we address the shortcomings of current \emph{mobile core network}  
architectures to efficiently
support IoT and explore opportunities offered by SDN, NFV and cloud technologies.
Specifically, we leverage SDN for forwarding and NFV for network function
deployment to realize a new service API that replaces
LTE/EPC and is geared towards
IoT devices and services (e.g., support P2P communications) (\S ~\ref{sec:maas}). 
The new service API is realized by a novel control and data plane 
that eliminates bearer notions in 4G network 
to reduce control signaling and packet header overhead while 
supports mobility (\S ~\ref{sec:data}, \S ~\ref{sec:control}).
We propose a distributed architecture leveraging NFV, SDN and cloud
computing to enable mobility functions and IoT services to
be flexibly deployed at the network edge to reduce service latency
and to scale to multiple regions in a large geographical area.
% (\S ~\ref{sec:inter-region}).
%\simeca enables new business models where service
%providers specify and operate their own services on a shared network operator
%infrastructure. 
%We demonstrate that, by leveraging NFV and SDN, \simeca's novel
%control and data planes provide better support for IoT services in terms of latency, and control signaling
%and data plane overhead. 
We make the following contributions:

\begin{trivlist}
%\begin{itemize}
  \item $\bullet$ We propose a novel mobile edge cloud architecture, \simeca, that leverages
  NFV, SDN and cloud computing to enable a new service abstraction 
  that is more suitable for IoT devices and services.
  %a novel cloud-like 
  %business model to provide 
  %flexible IoT service deployments for multiple service providers.
  %Our architecture significantly reduces network latency while being scalable and 
  %fully supports mobility. 
  Our service abstraction enables P2P direct communication for end devices. 
  Our distributed architecture reduces the EPC core network latency 
  significantly while being able to scale to multiple geographic areas.
  %to $\leq$10ms, which translates to latency reductions of 10x for client-server communications and 9x for
  %peer-to-peer communications compared to a typical LTE/EPC deployment while being able to scale
  %to a large (local) geographic areas.
  %\item $\bullet$ We propose a cloud-liked business model in cellular networks that allows 
  %multiple services sharing the same infrastructure.
  \item $\bullet$ We propose novel control and data planes 
  %to support 
  %the new cloud-liked business model in mobile networks 
  which scale better and have lower overhead to better support IoT services, 
  compared to the current LTE/EPC control plane. Our design involves
  an IP header translation forwarding scheme which
  reduces data plane overhead by up to 20\% for small sensor payload sizes (e.g., $<$100 Bytes). 
  Our SDN-based control plane reduces control plane overhead by up to 37\% with a
  63\% reduction in number of data path forwarding states compared to LTE/EPC while performs up to 76\% faster.
  %Our control plane also performs 76\% faster than typical LTE/EPC control plane. 
  % while fully supports mobility.
  \item $\bullet$ We validate our architecture by prototyping and evaluating our approach
  by refactoring pre-commercial EPC software and using an SDR-based eNodeB to realize \simeca. 
  Using applications running typical IoT protocols, our evaluations show
  promising results of \simeca compared to LTE/EPC.
  %able to support a large number of IoT devices with reasonable 
  %initiation time and low data/control plane overhead.
  
%\end{itemize} 
\end{trivlist}
