\paragraph{Lightweight IoT Data Plane}
\label{sec:data}
    %\simeca applies intelligent edge - dumb core principle for 
    %packet routing. Network edge (i.e., SDN-based base stations) 
    %classifies uplink packets and the SDN network forwards 
    %packets by matching its header. 
    %Specific to mobile environments and IoT, 
    In contrast to heavy-weight tunneling with QoS 
    guarantees in the standard EPC core, 
    \simeca's best-effort forwarding data plane 
    could benefit IoT services. It: (1) 
    reduces packet header overhead for IoT devices, 
    (2) reduces the number of forwarding states inside the network, 
    while (3) enabling seamless 
    mobility.
    %, i.e., maintain traffic flows for applications 
    %when devices move across base stations. 
    %The 
    %data plane should also be scalable given the distributed 
    %infrastructure.    
        
 %       \xxx{
 % Data plane:\\
 % - Best effort instead of high quality QoS: SDN flows instead of GTP tunnels.\\
 % - Header translation instead of adding GTP tunnels: reduce packet header overhead, support mobility.\\
 %- Intelligent edge dumb core: reduce number of states in the core network, only maintain states at network edge (base stations).\\
 % Control plane:\\
 % - Setting up SDN flows is more light weight than setting up GTP tunnels: for each C2S connection setup, only 2 flows need to be installed at 
 % the base station, while LTE/EPC requires 4 GTP tunnels at eNodeB, SGW, PGW. In Simeca, only controller - base station interaction is needed 
 % while in LTE/EPC, MME needs to exchange messages with SGW, PGW, eNodeB to set up a tunnel.\\
 % - In LTE/EPC radio bearers on eNodeB has to associate with GTP tunnel at SGW (S1-U bearer, S1-C control bearer). The result is when radio bearers 
 % release, the S1-U/C bearers also releases. This leads to control signaling when radio bearers set up again (i.e., service request, paging). 
 % In Simeca, as the packet classification happens only at eNodeB, there is no overhead to set up GTP tunnel when the radio bearers release (i.e., reduce 
 % signaling overhead for service request, paging).
%}

 %   \xxx{
 %       - Dumb-core, inteligent edge: (1) only 2 states needed at the edge, share core forwarding. (2) less interaction in the control plane.\\
 %       - Light-weight header translation and identity separation for seamless mobility: identity, identity translation, seamless mobility.\\
 %       - P2P forwarding: (1) translate from DI to RI for correct routing, (2) tracking RI when device handover, (3) if need paging: base station 
 %       can't find device -> contact MME for paging the device.\\
 %       - Intra-region forwarding example.\\
 %   }

   
    {\bf Device identity and forwarding identity. }     
    A device in \simeca is identified using two identities: 
    \emph{device identity} (DI) and \emph{device routing identity} (RI).
    
    \begin{trivlist}
        \item $\bullet$ \emph{Device identity-DI:} Each device has a unique DI 
        assigned by the network when the device attaches to the network. 
        This DI is associated with the device and \emph{does not change} 
        until the device detaches from the network. 
        As this DI does not change upon device mobility, 
        applications running on the device 
        could use this DI to enable continuous operation, i.e., seamless mobility.
    
        \item $\bullet$ \emph{Routing identity-RI:} 
        In the SDN edge network, packets are forwarded using 
        a separate RI (different from the DI). 
        This RI is also allocated by the network when a 
        device attaches to the network. Unlike DI, RI is specific 
        to a base station and \emph{changes} when a device 
        moves to another base station. Under a specific base station, 
        there is a unique mapping between DI and RI.

          \begin{figure}[h]
            \centering
            \vspace{-0.1in}
            \includegraphics[scale=0.5]{simeca/figs/routing-id.pdf}
            \caption{Routing identity header structure}
            \vspace{-0.15in}
            \label{fig:routing-id}
          \end{figure}
    \end{trivlist}



    {\bf Lightweight header translation.}
    Unlike in EPC where extra tunnel overhead is added to packets 
    at eNodeB and S/PGW for delivery over a transport network, \simeca does not add 
    \emph{any} extra overhead to packets. \simeca instead 
    uses header translation mechanism to translate DI-RI at network edge (i.e., base stations). 
    Inside the SDN edge network, packets  
    have source and destination RIs in their header for forwarding. 
    When the packets reach end-points, they have DIs 
    as their source and destination for seamless mobility. 
    When a device moves across base stations, it has 
    a new RI and therefore the DI-RI mapping also 
    gets updated accordingly. To realize this DI-RI mapping, 
    an SDN controller installs SDN rules to base stations 
    in a \emph{proactive} manner during device attach/handover. 


    {\bf Seamless mobility.} 
    %When a device attaches to a base station, the SDN mobility controller of 
    %that base station allocates a new \textit{routing identity (RI)} for that device.
    %The SDN network then forwards traffic reaching the device 
    %via the attached base station by matching the \textit{RI} in the destination field.
    \simeca support seamless mobility as it 
    separates device identity, which is unchanged by mobility, 
    and forwarding identity which changes with mobility.
    When a device moves to a new base station, 
    it is assigned a new RI that has the new base station's BID.
    %With this new RI, packets are no longer 
    %forwarded to the old but to the new base station. 
    Note that the DI of the device does not change therefore  
    from the end-points (device, server) perspective, the 
    RI change is \emph{transparent}. 
    %This is the key for 
    %seamless mobility in \simeca.

    For example, device A with DI of \textit{192.168.3.33} 
    attaches to base station 1 and gets RI \textit{192.168.10.10/24}. 
    When it moves to base station 2 it gets a new RI \textit{192.168.20.10/24}. 
    Packets inside the SDN edge network are now forwarded to base station 2 where device A is currently attached 
    using the new BID \textit{192.168.20.0/24}. 
    However, packets arriving at device A and server still have 
    device A's DI \textit{192.168.3.33} in the header. 
    %This 
    %allows applications on device A to continue work regardless of the mobility.



    {\bf P2P forwarding and device location tracking.} 
    Unlike EPC, \simeca naturally enables peer-to-peer communications 
    in which a device is reachable via its DI. 
    This type of communication is enabled 
    when a device requests a \emph{P2P-attach} to the other device 
    with the destination device's DI specified (e.g., ``Device DI1 
    attaches to device DI2'', Section~\ref{sec:maas}). 
    When receiving a P2P-attach request, \simeca control plane 
    needs to know the RI of the destination device 
    to install SDN rules in the source base station for DI-RI header translation. 
    This DI-RI mapping information is dynamic as the peer device 
    moves across base stations. 
    \simeca control plane stores this DI-RI mapping in a centralized table 
    (location tracking table) and updates the table accordingly when a device 
    changes its attachment point (hands over to another base station). 
    %For intra-region P2P communications, a local version of 
    %the table keeps track of devices in that region to improve look-up performance.
    % for intra-region communications.

    %In order for the SDN network to forward packets to the destination base station, the 
    %source base station needs to translate destination device's \textit{DI} 
    %to its \textit{RI}. As device's \textit{RI} changes 
    %depending on the current attachment point, the mapping between a device's \textit{DI} 
    %and its \textit{RI} also changes.

    %When the source base station classifies uplink packets to 
    %insert correct destination \textit{RI} for packet forwarding, it 
    %needs to lookup the \textit{RI} in a centralized database. 

    Table~\ref{table:location_table} shows an example of location tracking table 
    in \simeca. A new entry is created by \simeca control plane 
    when a device attaches \emph{and} registers 
    itself as a reachable service (Section~\ref{sec:maas}). 
    It is updated when the registered device 
    moves across base stations (more details in Section~\ref{sec:control}).
    %Upon a device handover, the MC updates an existing entry 
    %with the new \textit{RI} at the target base station. 
    %Upon a device detach, the MC delete the corresponding entry in 
    %the table. More details are in
    %section~\ref{sec:control}


                \begin{table}[htbp]
                    \vspace{-0.1in}
                    \centering
                    \caption {Attachment point tracking table for P2P communications}
                    \label{table:location_table}
                    {\small
                    \begin{tabu} {|c||c|}
                        \hline
                        \textbf{Device identity}      & \textbf{Routing identity}    \\ \hline \hline
                        192.168.3.33                    & 192.168.10.10/24      \\ \hline
                        192.168.3.34                    & 192.168.11.11/24         \\ \hline
                    \end{tabu}
                    }
                    \vspace{-0.1in}
                \end{table}


%                \begin{table}[htbp]
%                    \vspace{-0.1in}
%                    \centering
%                    \caption {Location tracking table for P2P communications}
%                    \label{table:location_table}
%                    {\small
%                    \begin{tabu} {|c||c|c|}
%                        \hline
%                        \textbf{Device identity}      & \textbf{Routing %identity}  & \textbf{Dst Tunnel IP}   \\ \hline \hline
%                        192.168.3.33                    & 192.168.10.10/24       & 10.10.10.1    \\ \hline
%                        192.168.3.34                    & 192.168.11.11/24       & 10.10.11.1    \\ \hline
%                    \end{tabu}
%                    }
%                    \vspace{-0.1in}
%                \end{table}


   % \xxx{
   % -   routing identity centralized management? which is allocated/free, per service - local routing id pool, global (across services)\\
   % -   local pool release/acquire global pool.
   % }
 
     \begin{figure*}[tb!]
       \begin{subfigure}{.33\textwidth}
        \centering
        \includegraphics[height=4cm, width=5.5cm]{simeca/figs/intra-region-routing-p2p.pdf}
        \caption{Packet header translation: packet forwarding example}
        %\vspace{-0.1in}
        \label{fig:p2p-intra-region}
      \end{subfigure}
      \begin{subfigure}{.33\textwidth}
        \centering
        \includegraphics[height=4.5cm, width=5cm]{simeca/figs/simeca-vs-epc-control-plane.pdf}
        \caption{Tunneling in EPC vs. SDN rules in \simeca}
        %\vspace{-0.1in}
        \label{fig:simeca-epc-compare}
      \end{subfigure}%
%      \begin{subfigure}{.33\textwidth}
%        \centering
%        \includegraphics[height=4.5cm, width=5cm]{simeca/figs/control-plane-interaction.pdf}
%        \caption{Intra-region forwarding}
%        \vspace{-0.04in}
%        \label{fig:controller_interact_intra}
%      \end{subfigure}%
      \begin{subfigure}{.33\textwidth}
        \centering
        \includegraphics[height=4.5cm, width=5cm]{simeca/figs/control-plane-interaction-ho-intra-region.pdf}
        \caption{Handover interaction}
        \vspace{-0.04in}
        \label{fig:controller_interact_intra_ho}
      \end{subfigure}
    \caption{\simeca operation}%Multiple ISA instances share a infrastructure, forwarding in \simeca}
    \label{fig:forwarding}
    \vspace{-0.1in}
    \end{figure*}
   
   
    {\bf Example of packet forwarding.}
    Figure~\ref{fig:p2p-intra-region} shows the translation of 
    device identity to routing identity at base station for 
    intra-region forwarding: P2P forwarding on the left and 
    C2S forwarding on the right.
    For P2P forwarding, device 1 initiates a flow to device 2 in the \textit{same} region. 
    The packet header at device 1 is $[DI1,DI2]$ where $DI1$ and $DI2$ 
    are device 1 and 2's \textit{device identity}. Base station 1 has 
    a SDN flow rule that \textit{replaces} uplink packets header with 
    device 1 and 2's \textit{routing identity} and forwards the packets 
    to the SDN edge network. 
    The SDN edge network has pre-installed SDN rules 
    that matches on base station ID in packet header (i.e., $[RI1, RI2]$). 
    Upon arriving at destination base station 2, 
    the packet header is translated back to $[DI1,DI2]$ and delivered to device 2.

    Similarly for C2S forwarding, base station 3 translates packets source 
    address from $DI3$ to $RI3$. The destination address which is the server address does not change 
    (i.e., A in figure ~\ref{fig:p2p-intra-region}). The SDN edge network 
    forwards uplink packets based on server destination address. For downlink packets 
    from the server, the SDN edge network simply forwards packets by matching the destination 
    address in the packet which is embedded in the uplink packets (i.e., $RI3$). 

  
    %The intra-region forwarding between the device and 
    %the gateway switch is based on 
    %the prefix of destination that gets updated 
    %when a service registers (more details in
    %section~\ref{sec:control}).
   {\bf Reduce number of forwarding states.}
        To reduce the number of forwarding states 
        inside the SDN edge network, \simeca uses 
        an intelligent-edge dumb-core principle. 
        In the SDN edge network, RI has a
        predefined structure that aggregates 
        end-points in the same base station.  
        Specifically, as in Figure~\ref{fig:routing-id}, 
        a RI consists of a \textit{Base station ID-BID} and a \textit{End-point ID-EID}. 
        The BID specifies a base station in the network 
        and EID specifies 
        a device in that base station. Inside the SDN edge network, 
        packets are forwarded by matching on BID \emph{only}. 
        When packets arrive at the edge (base stations), the base stations 
        demultiplex packets to corresponding devices using EID. 
        Therefore, the number of forwarding states in the SDN edge network 
        only depends on the number of BIDs allocated regardless of the number of devices. 
        %Compared to LTE/EPC core network, this is significantly less states 
        %as LTE/EPC core keeps one state per \textit{tunnel (i.e., bearer)} to the devices. 
        

        For example, if using the IP address structure, a RI would look like \textit{192.168.10.10/24} 
        where \textit{192.168.10.0/24} is BID 
        and \textit{192.168.10.10} is EID. 
        Inside the SDN edge network, packets are forwarded using the BID \textit{192.168.10.0/24}.
        Upon reaching a base station, the EID \textit{192.168.10.10} is used 
        to deliver the packets to the corresponding device 
        (using DI-RI mapping in the base station).

        For each end-to-end connection, 
        %\simeca significantly reduces number of forwarding 
        %states maintained in the data plane for each end-to-end connectivity. 
        there are only 2 SDN rules are installed at base station in \simeca 
        (the rules in the SDN edge network are shared among devices of 
        the same base station), while there are 8 GTP tunnel IDs 
        are maintained per EPS bearer in the EPC core network (i.e., 2/4/2 
        GTP tunnel IDs at eNodeB/SGW/PGW per bearer).

\iffalse
    {\bf Adaptive optimization of P2P connectivity.}
    We can further optimize the P2P IoT data plane by exploiting an
    inverse relationship between its performance, (i.e., latency), and overhead,
    (i.e., total number of forwarding rules in the network).
    One extreme, i.e., fully centralized, is equivalent to the current LTE/EPC
    architecture which forces any type of communications to go through a
    centralized PGW.
    If SDN-enabled base stations allow P2P IoT devices to directly communicate 
    with no intervention of any edge or core network component, it is another
    extreme case, i.e., fully distributed,
    which is optimized for low-latency IoT services.
    This service category is one of our target IoT service requirements
    but might require high overhead, e.g., the number of (duplicated)
    forwarding rules towards IoT devices involved in active P2P communications at each base station.
    Therefore, within a given IoT service delay budget, each capacity-limited
    or overloaded base station offloads a portion of forwarding rules to upstream
    edge switches as anchoring points with help from the mobility controller in
    terms of the distribution of forwarding rules.
    Those rules from all downstream switches or base stations can be aggregated by eliminating duplicated rules, resulting in less overhead in despite of a little detour.
    SIMECA enables such an architectural flexibility to deal with heterogeneous IoT services, depending on their diverse service requirements.
\fi
