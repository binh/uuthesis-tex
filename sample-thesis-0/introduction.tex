\chapter{Introduction}

 
With the increasing popularity of smart phones and wireless connected devices, mobile networks (the current 4G-LTE and next generation 5G) 
have become a critical infrastructure; Smart phone traffic will exceed PC traffic 
by 2020; Global mobile data traffic will grow 3X as fast as fixed IP traffic from 2015 to 2020 and will be 
16\% of total IP traffic by 2020~\cite{cisco.white.paper}. Although details of the 5G mobile network architecture 
are still emerging, the 5G architecture is expected to enable numerous use cases with a wider range of characteristics than 
what is possible today, from broadband access 
to massive Internet of Things to ultrareliable communications (e.g., E-health services) 
and so on~\cite{ngmw}. Given the criticality of the network and the emerging services it will be hosting, 
making the network more scalable and reliable are among the most important challenges of future mobile network architecture. 


Following broader industry trends, including efforts related to 5G, this dissertation applied Network Function Virtualization (NFV) and Software-defined Networking (SDN)  
as well as big data processing and distributed system techniques to enhance scalability and reliability aspects of the current mobile core network architecture.
It addressed limitations and problems with the current 4G mobile core network architecture 
in supporting a large number of Internet-of-Things (IoT) devices and low-latency services (Chapter~\ref{chapter:chap1}). 
It enhanced mobile network reliability using a data analytics approach by developing a novel monitoring methodology and system to enable early detection of service failures 
in the network (Chapter~\ref{chapter:chap2}). It designed a reliable control plane architecture 
to improve mobile network availability in public clouds (Chapter~\ref{chapter:chap3}).  


%\textbf{Dissertation statement:} Emerging technology trends in software defined networking, data analytics and distributed system can be used to provide new functionalities and enhance scalability and reliability of current and future mobile core networks.

%\section{Overview}
%In this section we provide an overview of the works that are proposed.\\
\pagebreak
\section[Introduce new functionalities and enhance scalability in the Mobile Core Network]{Introduce new functionalities and enhance scalability\\ in the Mobile Core Network}

Future mobile networks (i.e., 5G), 
will need to support a large number of Internet-of-Things (IoT) devices, 
around 26 billion by 2020 according to some estimates~\cite{gartner}.
Although most existing IoT devices are using short-range radio access technologies,
e.g., IEEE 802.15.4, emerging IoT devices are expected to also use cellular
radio access technologies to take advantage of 
a wider coverage, support of mobility, well-managed and secured network~\cite{5gvision}.
The massive number of devices implies a fundamental question of how well
the current cellular architecture would support IoT devices and what
the network might need to change to favor them.


Despite the fact that 4G networks are completely
packet based, the architecture was designed and 
optimized for human-to-human and human-to-machine communications.
The result is the 4G architecture that works well for a \emph{limited} 
number of connections that generate relatively high volumes of \emph{high quality} traffic (e.g., smart phones streaming videos or voice calls) 
rather than a \emph{massive} number of devices with a sporadic traffic pattern 
(e.g., room temperature sensors uploading a few Bytes of traffic every half an hour).  
For example, the Evolved Packet Core (EPC) architecture introduces Evolved Packet System (EPS) bearer notion in the EPC core network. 
While EPS bearers support high QoS data transmission (i.e., via GTP tunnels with 9 QoS Class Identifiers (QCIs)~\cite{3gpp23203}   
representing different delay and throughput requirements), 
setting up or maintaining them incurs significant overhead~\cite{kim2012directions}.
For example, a standard LTE initial attach procedure incurs up to 6 control messages 
to set up a bearer, while a service request procedure incurs up to 4 control messages ~\cite{3gpp23401} every time a device has data to send. 
%This overhead is not only generating significant amount of control signaling 
%in the network~\cite{mmeload} but is also inefficient for IoT devices.

Moreover, in the current mobile network, 
data-path elements (i.e.,
SGW and PGW) that forward traffic to the Internet , as well as control plane elements dealing
with mobility (i.e., MME) and policy (i.e., PCRF),
are specialized hardware-based equipment or virtualized VNFs  
deployed in a limited number of physical locations (i.e., central offices). 
This leads to a high end-to-end latency for mobile services; 
traffic between endpoints need to travel to these centralized 
data centers, adding extra latency to the end-to-end
path. On the other hand, the vision for the 5G network~\cite{ngmw,5gvision} calls
for services that will require significantly lower latencies (e.g., $<$5ms for 
tactile Internet), which the current 4G architecture will not be
able to support. 


The above limitations suggest improvements in the 4G architecture 
to support emerging devices and services. We argue that end services 
and the mobile core network's mobility functions should be placed closer to users 
in a distributed manner to improve network latency. Moreover, in order to support 
a massive number of IoT devices with sporadic traffic, the network must have lower overhead.
We argue that the mobile architecture should be changed fundamentally 
both in control and data planes to reduce network overhead for a massive number of IoT devices.
Specifically, we proposed a Software Defined Network offloading architecture (SMORE - section~\ref{sec:smore}) 
to \emph{seamlessly} offload a subset of mobile data traffic (e.g., IoT traffic) to distributed infrastructure hosting 
services at the network edge.
We proposed a novel Software Defined mobile edge cloud architecture (SIMECA - section~\ref{sec:simeca}) that 
uses best-effort traffic delivery 
to reduce control and data plane overhead for a massive number of IoT devices.



\section[Enhance reliability in mobile networks with data analytics and novel failure detection mechanism]{Enhance reliability in mobile networks\\ with data analytics and novel\\ failure detection mechanism}

Many critical services such as first response and public-safety are already running on current mobile networks. 
The 5G networks are expected to be even more reliable in order to host emerging services such as 
remote control (e.g., cloud robotics), E-health (e.g., remote surgery), etc. ~\cite{ngmw,5gvision}. 
Network operators have spent significant efforts on monitoring and detecting failures 
in a timely manner to maintain high service availability. 
However, despite the existing monitoring systems, detecting all service disruptions in an operational network is surprisingly challenging.


Somewhat counter-intuitively, network elements that support service functions are not always able 
to alarm on conditions which are in fact service impacting. This may be the result of, for example, 
software bugs in the network elements' 
firmware, or in the EMS (Element Management System) for the network elements, or due to configuration errors. 
It is possible that, even though all network metrics indicate a healthy network, customers might be experiencing degraded service or a complete service disruption. 
For example, deployment of a new service feature or a software upgrade to address a bug might trigger an unintended 
side effect (or indeed a new bug) that the monitoring system is not equipped to detect. 
We define such service disruptions that are not captured by network monitoring as {\it silent failures}.
Furthermore, it is difficult to infer service quality perceived by customers using the status of the network. 
There is a complex relationship between the status of the network and the service quality the users experience. Because of redundancy mechanisms within the network, a particular network failure does not necessarily imply customer impact. 
For example, users associated with a failed cell tower could be picked up by neighboring towers as long as they are in the coverage range of the neighboring towers and the neighbors still have enough resources to handle the users' traffic~\cite{towerscan}. 


Moreover, end-to-end user experience could be affected by problems happening beyond the network itself. In fact, if the problem happens on the devices themselves there will be little evidence on 
the network to detect this kind of disruption. For example, a bug in a firmware update could 
cause incompatibility of the device's Radio Stack with the Radio Network Controller (RNC). 
This could result in a large number of devices (e.g., all iPhone 6 devices in the U.S.) not being able to use the network.
This kind of service disruptions is usually revealed by the customer-care database (e.g., customer calls) 
long after the failure occurred and typically has large customer impact before being noticed and fixed 
by the network operator.

This raises the question of \emph{what} to monitor to efficiently capture all possible service disruptions (i.e., \emph{silent failures}) with 
\emph{high fidelity}. 
We propose \absence, a \emph{passive service monitoring} mechanism to detect service disruptions based on customers' usage. \absence argues that users' usage, or lack thereof, is a reliable indicator of service outages or severe performance 
degradations in mobile networks. Although the approach sounds simple, \absence needs 
to solve  the following challenges: How should we aggregate customer usage in a way that increases 
the fidelity of a detection (e.g., \absence must be able to distinguish a normal drop in customer usage and a service disruption)? 
How can we monitor services across multiple dimensions (e.g., device model, geographical location, etc)? How can 
we design a system that scales to an operational network with hundreds of millions of users in real time?
We implemented and deployed \absence in an operational network. \absence was able to process Tera-Bytes of call record data per day and detected real failures in the network operator (Chapter~\ref{chapter:chap2}).



%\xxx{Problems with current moniroting system: only monitoring network component which is not sufficed in indicating end-to-end user experience. Argure that user experience is the most accurate and the ultimate metric. Question is whether usage is a good metric (example of small and large group) and how to do it with scale in real time?}


\section[Enhance reliability of the mobile network in public clouds]{Enhance reliability of the mobile network in\\ public clouds}

While detecting failures and fixing them in real time help increase service availability, it is more ideal to design and build a highly available network in the 
first place. In fact, a mobile network that is not available for a short amount of 
time could cause many users not to have services for tens of minutes; a crash 
in the mobile network could cause up to an hour of service outage on the users.


A mobile network is a set of distributed state machines. There are per-user states maintained on the core network's components and on the user device. 
It is required that the 
mobile core network should be ``always'' available and the per-user states must be 
consistent across the mobile core components and the device. 
Conventional mobile networks rely on hardware reliability to ensure availability and state consistency. 
Mechanisms such as active - standby or persistent UE context storage require  
either redundant or highly reliable hardware~\cite{lucent.srs}. Typical 
cellular grade network components are built with ``five-9s'' reliability (i.e., availability of 99.999\%)~\cite{ericsson.high.availability,nokia.9471.wmm}.


On the other hand, future mobile core networks expect to use Virtualized Network Functions (VNFs) 
running on commodity servers or VMs in data centers or public clouds. 
In contrast to the highly-reliable hardware blades, 
public clouds often are not comparable in terms of reliability; 
public clouds are subject to planned or unplanned maintenance and 
VM migration. Some surveys show that a typical cloud service experiences unplanned outage for hours in a month~\cite{cloud.outage} (i.e., ``two-9s'' or 99\% of availability for 1 hour of outage per month). 


This suggests a software-based solution to build a reliable mobile core network component on top of an unreliable infrastructure in public clouds. 
%The reliable MME must have the following characteristics; It must be available even when some of the nodes 
%crash or network partitioned; It must maintains consistent state with the actual UE even when nodes crash. 
We enhance the reliability of the mobile network by introducing redundancy and distributed system techniques 
into the current mobile core architecture. 
Our proposal, called \echo, provides the same properties that mobile core networks  guarantee today. However, 
\echo is built on top of different assumptions about the infrastructure hosting the mobile core network. 
%The role of the EPC is to manage user devices; 
%this is driven by a distributed state machine that stores user device states across multiple components,
%as well as on the user's mobile device.
%All these states need to be consistent in spite of potential component and network failures.
%Inconsistencies can lead to long service outages (Section~\ref{sec:control-plane-requirements}).
%This is fundamentally different 
%from the requirements of conventional middleboxes where state is typically shared only across multiple instances of the same appliance.
\echo introduces a reliable agent at eNodeBs to guarantee eventual completeness of 
a request. To enhance availability, \echo separates state from the EPC state machines 
and turns each EPC component into stateless redundant instances sharing a reliable storage. However, redundancy in \echo creates problems of maintaining 
state consistency. First, given that multiple instances of a component can process 
a request in the same time, how to make the shared state consistent. Second, given multiple instances of a component could create multiple duplicated state change requests to other components, how to make sure the requests create a single consistent state change across the EPC components.


\echo deals with the first problem by making the component instances process 
requests {\em monotonically} and each instance updates the shared state {\em atomically}. This ensures only one component instance is able to update the shared state and no 
stale requests could reverse the updated state. \echo deals with the second problem 
by making the component idempotent; duplicated requests will cause 
the same effect on a component. This, together with the first solution, ensures that 
a side effect will always be processed {\em linearizably}, which is 
equivalent to the processing in a conventional mobile core network. Moreover, the solutions in \echo allow 
component instances to process requests in a nonblocking manner 
to improve availability.

We implemented \echo using commercial smart-phones, LTE small cells and 
pre-released EPC core software. We evaluated \echo in Microsoft Azure. \echo 
showed a significant improvement in terms of reliability while introducing   
small latency overhead that is not perceivable by users.


%\xxx{What happen if UE context on MME is lost? what if the state machines are inconsistent? Hardware-based reliability is no longer available in cloud. Instead, VMs are %subject to failure node crashes, network partition. Need a mechanism to enhance reliability. How to do this with scale and elasticity?}



\section{Contributions}
This dissertation makes the following contributions:
\begin{trivlist}
  \item $\bullet$ \textbf{Novel offloading and mobile edge cloud architecture and novel control/data planes to support a massive number of IoT devices}\\
    We present SMORE, an architecture to realize offloading in
    an LTE/EPC mobile network. Specifically, we show how offloading
    for selected traffic of subscribed users can be realized
    without modifying the standard LTE/EPC interactions, even
    when devices are mobile (Chapter~\ref{chapter:chap1}.) We propose SIMECA, a novel mobile edge cloud architecture  
    that leverages NFV, SDN to enable a new service abstraction 
    that is more suitable for IoT devices and services. The new service abstraction in \simeca 
    is realized by novel control and data planes 
    which scale better and have lower overhead to better support IoT devices, 
    compared to the current LTE/EPC control plane. 
    We evaluate SMORE and SIMECA 
    using a prereleased EPC mobile core software, Software-defined Radio base stations, 
    OpenVswitch and SDN controller (Chapter~\ref{chapter:chap1}.)
  \item $\bullet$ \textbf{Passive service monitoring system to detect failures in mobile networks}\\
    We propose a novel service disruption detection technique 
    and a system called \absence that infers service disruptions by monitoring aggregated customer usage. 
    Our design is informed by a data driven exploration of the problem domain using real operational data from a major mobile operator.
    \absence uses a scalable Hadoop-based implementation of our approach which is capable of performing service disruption detection by processing huge volumes of anonymized CDR data (e.g., hundreds of millions of records every hour for mobile data service) in a streaming manner, for all the mobile services associated with an operational mobile network.
    Using data from the same operational mobile network, we perform a systematic data-driven evaluation of our approach by introducing a comprehensive synthetic set of both network and mobile device failure scenarios. 
    We also compare our results with ground truth from actual service disruption events and present a number of case studies showing the effectiveness of our approach (Chapter~\ref{chapter:chap2}.) 
  \item $\bullet$ \textbf{Novel architecture design to enhance reliability of mobile network components in public clouds}\\
  We propose \echo{}, a reliable EPC architecture for public clouds.
  \echo is based on the observation that EPC runs user-specific distributed state machines in parallel.
  We proposed the main building blocks for a reliable implementation of the distributed state machine,
  and we then mapped these blocks to the original EPC system components.
  We proposed a mechanism to ensure consistent state change across replicated 
  components in a mobile network. 
  %We demonstrate the feasibility of the proposed architecture by implementing it in full.
  %We implement the entry-point agent software and deploy it on a COTS LTE small cell~\cite{ipaccess}.
  We implemented the required EPC modifications into OpenEPC~\cite{openepc} and deployed \echo on Microsoft Azure cloud.
  We performed an extensive evaluation of the system using real mobile phones as well as synthetic workloads.
  We showed that \echo{} is able to cope with host and network failures, including 
  several data-center component failures, without end-client impact.
  \echo showed performance comparable to commercial cellular networks today. 
  \end{trivlist}

\section{Organization}
This dissertation is organized as follows. The next chapter (Chapter~\ref{chapter:background}) will provide basic background knowledge in order to understand motivations and contributions in the following chapters. The chapter describes the mobile network's  architecture, its data service, connection abstractions, mobility, and network stacks. The three chapters after that describe the three contributions of the dissertation. Specifically, 
Chapter~\ref{chapter:chap1} describes enhancing scalability and introducing new functionalities in the Mobile Core Network using Software Defined Networking, novel network architecture and network protocols. Chapter~\ref{chapter:chap2} describes 
enhancing reliability in Mobile Networks with data analytics and a novel failure detection mechanism. Chapter~\ref{chapter:chap3} describes enhancing reliability in Mobile Networks with distributed system techniques and a distributed mobile control plane. The last chapter (Chapter~\ref{chapter:conclusion}) concludes the contributions and discusses future research directions related to the topics of this dissertation.


%Different generations of mobile networks from 2G, 3G come with different RAN technologies 
%and a new mobile core network architecture. While 2G and 3G mobile network mainly support circuit switch and voice call services, 
%the evolution of the current 4G-LTE architecture (and the future 5G to be expected) were shaped significantly by 
%the evolution of handheld devices that become more and more powerful. 
%The result is a fully packet-based architecture geared towards human devices (smartphones) accessing Internet service. 
%Despite being a fully packet-based architecture, the mobile network has its own 
%historical reasons and requirements to design a network with specific protocols and procedures: tunnels supporting QoS 
%are used for data delivery; the network needs to perform a number of procedures to support device mobility. 


%The challenges in detecting failures: network complexity, redundancy.


%The NFV trends and needs for enhancing reliability in clouds with software-based mobile network.











%- Significant to be scalable: 5G requirements, large number of devices.\\
%- Significant to be reliable: 5G, everything is on cellular, including critical service.\\
%- Thus, scalability and reliability are two important aspects of current/future cellular networks. 
%However, doing reliably and scalably is not easy:\\
%  + section 1.1: The network were designed to support mobility as an overlay, support high quality human-centric devices. The result is an high overhead %network. Moverover, network is hardware-based and proprietary equiments, only deployed in limited locations.\\
%   - The needs for scalability: 5G designs with IoT, control and data plane, latency.\\
%      + SMORE: SDN offloading.\\
%      + SIMECA: SDN base station and SDN control, network architecture, IoT. \\
%  + section 1.2: Cellular network are faily complex: bug triggers bug, not clear relationship between user experience and the health of the network %element.\\
%    - The needs for reliability: complexity of the network, hardware based to software based reliability.\\
%      + ABSENCE: failure detection, usage based, network management.\\
%  + section 1.3: Recently the trends are to move to NFV based cellular networks and data center. These reduce the cost and increase flexibility however %posting a new challenger in designing an architecture for reliable network.\\
%    + REPLICA: reliable control plane in public clouds.\\
%- Contributions:\\
%  +  We proposed a novel SDN-based offloading and SDN-base mobile core architecture to enable flexibility in EPC. WE proposed a control/data plane that %have lower overhead that favors large number of devices (IoT). We prototyped the architectures using SDN and Software-defined radio.\\
%  +  Proposed a novel passive mechanism to detect failures with scale: Usage-based failure detection in large scale operation networks. We design a %system using big data processing and show high detection rate and deployed.\\
%  +  Proposed a novel EPC control plane architecture to enhance reliablity with scale in public clouds.\\
