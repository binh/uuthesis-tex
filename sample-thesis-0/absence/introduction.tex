\vspace{-5mm}
\section{Introduction}
\label{sec:introduction}

\iffalse
\xxx{Outline:

- What is the problem we are addressing?

- What have others done to address this problem? Why is that inadequate?

- What are we proposing to do?

- What specific contributions are we making?

Contributions:

- Novel approach: customer usage based silent failure detection.

- Implementation: real system.

- Thorough evaluation:

= using data from an operational network

= synthetic evaluation

= comparison with known found  truth use cases

}
\fi

The proliferation of sophisticated mobile devices like smart phones, tablets 
and wearable devices~\cite{index2015global} have
made them an integral part of today's society. 
%A recent report suggests
%that the global number of mobile devices grew to 7.4~billion in 2014~\cite{index2015global},
%suggesting a mobile device for nearly every human being on the 
%planet~\cite{worldpopulation}.
%%\footnote{Indeed by the end of 2014 the 
%%number of mobile devices are expected to exceed the human population~\cite{index2014global}.}
%The same report suggests a 45\% increase in the average volume of data used
%by smart phones in 2014; more than 800~MB per month. Finally, there were nearly
%109 million wearable devices in 2014, generating 15 petabytes of monthly traffic.
The growth in the number of mobile devices, the data usage of each device
and the types of mobiles devices of course implies
an increased reliance and dependence on mobile networks.
To address this demand, mobile operators are continuously investing in new 
mobile networks and technologies. In recognition of the
importance of the underlying network, mobile operators
are building redundancy into nearly all components
of their infrastructure and developing sophisticated systems to
monitor the health of their networks and to rapidly respond to
any customer impacting events~\cite{crk.book}. 
%In parallel, the networking research community are
%developing tools to analyze the performance of mobile
%networks~\cite{choffnes.pam,mobiperf,namehelp.mobile,netalyzr.mobile}
%as well as performing detailed studies to understand the impact
%of mobile networks on the performance of higher layer protocols
%and applications~\cite{performance.mobile.zmao,lte.performance,silent.tcp,tcp.performance}.

%Despite these efforts, the inherent complexity of mobile networks may result in
%service disruptions that go undetected by monitoring the network elements.
%Hence modern mobile operators adopt the strategy of deploying {\it service} monitoring, 
%in addition to the {\it network} monitoring, on the customer service experience and 
%on the customer feedback (e.g., customer care calls, online chats, or
%social media).
%The purpose of such service monitoring is to capture \emph{service disruptions} that are either are not detected
%by existing network monitoring systems or detected by network monitoring systems but haven't received immediate attention due to unknown customer impact.
%There are several reasons why service disruptions occur in mobile networks and why they
%are difficult to detect. 

Despite these efforts, the inherent complexity of mobile networks and their environments (customer devices, applications)
may result in service disruptions that go undetected by monitoring the network elements.
%or get detected by network monitoring but are assigned an inadequate severity level to prompt for immediate attention. 
Hence, in addition to {\it network} monitoring, modern mobile operators adopt the strategy of deploying {\it service} monitoring.
%Hence modern mobile operators adopt the strategy of deploying {\it service} monitoring, in addition to the {\it network} monitoring, on the customer service experience. 
Service monitoring is designed to continuously monitor the end-to-end experience that customers receive from their network-based services. This contrasts with network monitoring, in which the status of individual network elements and links are monitored for failures and impairments (e.g., link losses). 
Service monitoring is vital as a second line of defense -- capturing network, customer device or application issues as well as interaction issues  among them which may not be detected by the network/applications/devices themselves. Service monitoring is also crucial to quantifying the service impact of known network problems for prioritizing issue resolution.

Somewhat counter-intuitively, network elements that support the service functions are not always able 
to alarm on conditions which are in fact service impacting. This may be the result of, for example, 
software bugs in the network elements' 
firmware, or in the EMS (element management system) for the network elements, or due to configuration errors. 
It is possible that, even though all network metrics indicate a healthy network, customers might be experiencing degraded service or a complete service disruption. 
For example, the deployment of a new service feature or a software upgrade to address a bug might trigger an unintended 
side effect (or indeed a new bug) that the monitoring system is not equipped to detect. 
We define such service disruptions that are not captured by network monitoring as {\it silent failures}.
Furthermore, it is difficult to infer service quality perceived by customers using the status of the network. 
There is a complicated relationship between the status of the network and the service quality the users experience. For example, because of redundancy mechanisms within the network, a particular network failure does not necessarily imply customer impact. 
For example, users associated with a failed cell tower could be picked up by neighboring towers as long as they are in the coverage range of the neighboring towers and the neighbors still have enough resources to handle the users' traffic~\cite{towerscan}. 

However, monitoring service performance across a mobile network is extremely challenging. Traditional active monitoring approaches -- techniques which send test traffic across the network -- simply don't scale, courtesy of the very large number of cell towers (end points) that need to be monitored and the diverse set of services supported by mobile networks. One could alternatively naively imagine looking for service disruptions by looking for drops in traffic volumes on network elements. However, the inherently dynamic nature of a mobile network environment makes it difficult to infer service impact by monitoring individual network elements, e.g., routers or base stations, to distinguish between changes in traffic volume that are simply the result of the normal operation of the network, and changes that are the result of anomalous network behavior.

In this work we present our work on the \absence system to address the detection of service disruptions in mobile networks in a proactive manner. Our key insight is that service disruptions often impact the traffic consumed by customers, which very likely  reflects in customers' usage. 
This seemingly obvious observation, combined with a suitable mechanism to monitor customer usage, allows \absence to rely on customer usage data to detect the possible presence of service disruptions. Specifically, \absence uses aggregated (e.g., zip code level and handset manufacturer/model level) usage data for different mobile services (e.g., voice call, data, and short message) calculated from anonymized call detail records (CDRs).
%The aggregations are calculated by processing anonymized call detail records (CDRs) generated in near real time based on actual user activities.  
\absence uses the historical aggregated usage data to predict the expected customer usage for different mobile services at appropriate aggregations of customers, and compares this with real time customer usage data. A deviation from the predicted customer usage is highly likely an indication of a service disruption.  

%A key practical benefit of our approach is that we avoid all the issues described above associated with state-of-the-art approaches that attempt to infer customer perceived service quality based on direct monitoring of network conditions~\cite{3gppts32.450}. 
We make the following contributions:

$\bullet$  We present the design of \absence, a novel service disruption detection system for mobile networks that infers service disruptions by monitoring aggregate customer usage. Our design is informed by a data driven exploration of the problem domain using data from an operational mobile network. 

$\bullet$ We present a scalable Hadoop-based implementation of our approach which is capable of performing service disruption detection by processing huge volumes of anonymized CDR data (e.g., hundreds of millions of records every hour for mobile data service) in a streaming manner, for all the mobile services associated with an operational mobile network.

$\bullet$ Using data from the same operational mobile network, we perform a systematic data-driven evaluation of our approach by introducing a comprehensive synthetic set of both network and mobile device failure scenarios. Our results show that: (i)~Our variable-scale temporal aggregation improves detection by an order of magnitude over fixed interval aggregation. (ii) We achieve overall detection rates of 88\%, while we achieve 98\% or better detection rates for service disruption that have 
over 10\% usage impact within the corresponding aggregation (e.g., zip code and handset device model).

$\bullet$  We compare our results with ground truth from actual service disruption events and present a number of case studies showing the effectiveness of our approach. For a set of confirmed service disruptions, \absence achieves 100\% detection rate.

%First, somewhat counter-intuitively, it is difficult to
%infer service quality perceived by customers  using the status of the network. 
%There is a complicated relationship between the status of the network and
%the service quality the users experience. For example, because of redundancy
%mechanisms within the network, a particular network failure
%does not necessarily imply customer impact. For example, users
%associated with a failed cell tower could be picked up by neighboring
%towers as long as they are in the coverage range of the neighboring
%towers and the neighbors still have enough resources to handle the
%users' traffic~\cite{towerscan}. 
%Conversely, it is possible that, even though
%all network metrics indicate a healthy network, customers might
%be impacted. For example, deployment of a new service feature
%or a software upgrade to address a bug, might trigger an unintended
%side effect (or indeed a new bug) that the monitoring system is
%not equipped to detect. Similarly, because of the inherently
%layered nature of mobile network architectures, a failure
%or performance issue at a lower layer, e.g., the IP-based backhaul
%transit network, or the layer-2 access network, might impact
%the mobile service layer. But the cross layer dependency might not
%be known, or captured as relevant to the mobile layer.
%
%Second, one could naively assume that we could look for drops in traffic volumes on network elements 
%to identify service disruptions. However, the inherent dynamic nature of a mobile network environment makes it difficult to monitor individual network elements, e.g., routers or base stations, to distinguish between changes in traffic volume that are simply the result of the normal operation of the network, and changes that are the result of anomalous network behavior.
%%the inherent dynamic nature of a mobile network
%%environment makes it difficult to monitor individual network elements, 
%%e.g.,  routers or base stations, to distinguish between
%%changes in traffic volume that are simply the result
%%of the normal operation of the network, and changes that
%%are the result of anomalous network behavior. 
%For example, dynamic routing and load-balancing 
%mechanisms could introduce sudden and dramatic
%changes in traffic volume on individual network elements, but they are typically not customer impacting.
%Similarly, network elements are subject to
%planned maintenance and traffic grooming activities
%which might also result in changes in traffic volumes on the network elements,  but
%again is not indicative of a customer impacting event.
%
%Among service monitoring technologies, tracking customer feedback serves as the last line of defense, 
%as customer may not initiate contact to customer care immediately after experiencing 
%service disruption, and operation escalation may only be triggered when multiple
%customer complaints report on a likely related problem.
%%(to avoid falsely escalating customer-specific issue such as problem due to a malfunctioning phone). 
%Hence, relying on customer feedback may result in hours of delay to fix and recover from service disruptions.
%It is important for mobile operators to proactively monitor customer experience to detect service disruptions.

%According to our past experience, 
%dozens of this kind of \emph{silent failures} could occur in 
%a mobility network every year that are firstly detected via the customer feedback monitoring system.
%As the customer feedback monitoring system typically only captures silent failures that impact a large number of customers, there could be more small-scale silent failures that currently fly under the radar. 

%In this work we present our work on the \absence system to address
%the detection of service disruptions in mobile networks in a proactive manner, which complements the current service monitoring technologies.
%Our key insight is that service disruptions
%often impact the traffic consumed by customers, which directly reflects customers' usage. This seemingly
%obvious observation, combined with a suitable mechanism
%to monitor customer usage, allows \absence to 
%rely on customer usage data to detect the possible
%presence of service disruptions
%Specifically, \absence uses aggregated 
%(e.g., zip code level and handset manufacturer/model level) usage data for different 
%mobile services (e.g., voice call, data and `short message) by 
%aggregating anonymized call detail records (CDRs) generated in near real
%time by mobile networks based on actual user activities.
%\absence uses the historical aggregated usage data to predict the expected
%customer usage for different mobile services at appropriate
%aggregations of customers so that a deviation from the predicted 
%customer usage is highly likely an indication of a service disruption.  
%A key practical benefit of our approach is that we
%avoid all the issues described above associated
%with state-of-the-art approaches that attempt to
%infer customer perceived service quality based on
%direct monitoring of network conditions~\cite{3gppts32.450}.
%
%We make the following contributions:
%\begin{itemize}
%
%\item We present the design of \absence, a novel service disruption
%detection system for mobile networks, based on customer
%usage data. Our design is informed by a data driven exploration
%of the problem domain using data from an operational mobile network.
%
%\item We present a scalable Hadoop-based implementation of our
%approach which is capable of performing silent failure detection by mining huge volumes of anonymized CDR data (e.g., hundreds of millions of records every hour for mobile data service) in a streaming manner, for all the mobile services associated with
%an operational mobile network.
%
%\item Using data from the same operational mobile network,
%we perform a systematic data-driven evaluation of our approach by
%introducing a comprehensive set of both network and mobile device failure scenarios.
%Our results show that: (i)~Our variable-scale temporal aggregation improves detection
%by an order of magnitude over fixed interval aggregation. (ii) We achieve overall
%detection rates of 88\%, while we achieve 98\% or better
%detection rates for service disruption that have over 10\% usage impact within the corresponding aggregation (e.g., zip code and handset device model).
%
%\item We compare our results with ground truth from actual
%service disruption events and present a number of case studies
%showing the effectiveness of our approach. For a set of confirmed
%service disruption, based on customer-care data from the provider, \absence
%achieves 100\% detection rate.
%
%\end{itemize}
