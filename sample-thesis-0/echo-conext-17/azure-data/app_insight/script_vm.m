%%% Metrics 
%  Percentage CPU 0
%  Network In 1
%  Network Out 2
%  Disk Read Bytes 3
%  Disk Write Bytes 4
%  Disk Read Operations/Sec 5
%  Disk Write Operations/Sec 6

% time, metricName, count, total, minimum, maximum




iTime = 1;
iMetric = 2;
iCount = 3;
iTotal = 4;
iMin = 5;
iMax = 6;

%data = load('mout_vm.txt');
load('mout_vm.mat');


% CPU
figure(2);
clf(2);
ind = find(data(:,iMetric) == 0);
d = sortrows(data(ind,:));
plot(d(1:end-1, iTime), diff(d(:,iTime)));



metrics = unique(data(:,iMetric));
% DEBUG
metrics = 0;

figure(1);
clf(1);
subplot(4,1,1)
hold on
for im = 1:length(metrics)
  metric = metrics(im);
  ind = find(data(:,iMetric) == metric);
  d = sortrows(data(ind,:));
  plot(d(:,iTime), d(:, iCount), '.');
end

subplot(4,1,2)
hold on
for im = 1:length(metrics)
  metric = metrics(im);
  ind = find(data(:,iMetric) == metric);
  d = sortrows(data(ind,:));
  plot(d(:,iTime), d(:, iTotal), '.');
end

subplot(4,1,3)
hold on
for im = 1:length(metrics)
  metric = metrics(im);
  ind = find(data(:,iMetric) == metric);
  d = sortrows(data(ind,:));
  plot(d(:,iTime), d(:, iMin), '.');
end

subplot(4,1,4)
hold on
for im = 1:length(metrics)
  metric = metrics(im);
  ind = find(data(:,iMetric) == metric);
  d = sortrows(data(ind,:));
  plot(d(:,iTime), d(:, iMax), '.');
end
