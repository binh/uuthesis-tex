 %%% Resources
% bdeploy1openepcstorage 0
 %%% Locations
% US : TX-San Antonio 0
% US : IL-Chicago 1
% FR : Paris 2
% IE : Dublin 3
% US : VA-Ashburn 4
% BR : Sao Paulo 5
% US : FL-Miami 6
% RU : Moscow 7
% US : CA-San Jose 8
% HK : Hong Kong 9
 %%% Test cases
% bdeploy1testlb 0
% bdeploy1testvm1 1
% bdeploy1testvm2 2
% bdeploy1testvm3 3
% [variables('test_status_name') 4 (BBC)


str_loc = {'US : TX-San Antonio 0', ...
	   'US : IL-Chicago 1', ...
	   'FR : Paris 2', ...
	   'IE : Dublin 3', ...
	   'US : VA-Ashburn 4', ...
	   'BR : Sao Paulo 5', ...
	   'US : FL-Miami 6', ...
	   'RU : Moscow 7', ...
	   'US : CA-San Jose 8', ...
	   'HK : Hong Kong 9'};


str_test = {'bdeploy1testlb 0', ...
	    'bdeploy1testvm1 1', ...
	    'bdeploy1testvm2 2', ...
	    'bdeploy1testvm3 3', ...
	    'bdeploy1testbbc 4'};

Tests = (1:length(str_test))-1;
Locations = (1:length(str_loc))-1;


% time, location, test, result

iTime = 1;
iResource = 2;
iLocation = 3;
iTest = 4;
iResult = 5;


%% data = load('mout.txt');
%% data = sortrows(data);

%% Tests = unique(data(:,iTest));
%% Locations = unique(data(:,iLocation));

%% time = data(1,1) : (30*60) : data(end,1);
%% data_aggr = zeros((length(time)-1) * length(Tests) * length(Locations), 4);
%% for it = 1:(length(time)-1)
%%   it 
%%   for il = 1:length(Locations)
%%     for ite = 1:length(Tests)
%%       te = Tests(ite);
%%       loc = Locations(il);
%%       ind = find(data(:,iTime) >= time(it) & data(:,iTime) < time(it+1) & data(:,iLocation) == loc & data(:,iTest) == te);
%%       data_aggr((it-1) * length(Tests) * length(Locations) + (il-1) * length(Tests) + ite, :) = ...
%%       		[time(it), loc, te, mean(data(ind,iResult))];
%%     end
%%   end
%% end

%% ind_lb = find(data(:,iTest) == 2);
%% ind_bbc = find(data(:,iTest) == 1);

%% figure;
%% plot(data(ind_lb, iTime), data(ind_lb, iResult), '.', data(ind_bbc, iTime), data(ind_bbc, iResult), '.');
%% plot(data(ind_lb, iTime), data(ind_lb, iResult), '.'); %, data(ind_bbc, iTime), data(ind_bbc, iResult), '.');
%% ylim([-0.5 1.5]);


%% save('data_aggr.mat', 'data_aggr', 'data');

%% 'Done converting'


load('data_aggr.mat');
%data_aggr(:,1) = data_aggr(:,1) - data_aggr(1,1);
%data(:,1) = data(:,1) - data(1,1);



Tests = unique(data(:,iTest));
Locations = unique(data(:,iLocation));
Time = unique(data_aggr(:,iTime));

fprintf('\n*** Total failures per location and test\n');
for il = 1:length(Locations)
  l = Locations(il);
  for te = [1,2,0,3]
    it = te + 1;
    ind = find(data(:,iTest) == te & data(:,iLocation) == l);
    fprintf('%s, %s: %.5f\n', str_loc{il}, str_test{it}, 1-mean(data(ind, iResult)));
  end
end





fprintf('\n*** Total failures per test from all locations\n');
for te = [1,2,0,3]
  it = te + 1;
  failure = zeros(length(Time), length(Tests));
  for il = 1:length(Locations)
    l = Locations(il);
    ind = find(data_aggr(:,iTest) == te & data_aggr(:,iLocation) == l);
    failure(:,it) = failure(:,it) + data_aggr(ind, iResult);
  end
  failure = failure < 1;
  fprintf('%s: %.5f\n', str_test{it}, mean(failure(:, it)));
end




ind_lb = find(data_aggr(:,iTest) == 2);
ind_bbc = find(data_aggr(:,iTest) == 1);
ind_vm1 = find(data_aggr(:,iTest) == 0);
ind_vm2 = find(data_aggr(:,iTest) == 3);

figure(4);
clf(4);
plot(data_aggr(ind_bbc, iTime) - data_aggr(1, iTime), data_aggr(ind_bbc, iResult), '.', ...
     data_aggr(ind_lb, iTime) - data_aggr(1, iTime), data_aggr(ind_lb, iResult), '.', ...
     data_aggr(ind_vm1, iTime) - data_aggr(1, iTime), data_aggr(ind_vm1, iResult), '.', ...
     data_aggr(ind_vm2, iTime) - data_aggr(1, iTime), data_aggr(ind_vm2, iResult), '.');
ylim([-0.5 1.5]);
xlabel('Time [s]')
ylabel('Success')


figure(5);
clf(5);
CDF = (0:(size(ind_lb,1)-1))/(size(ind_lb,1)-1);
plot(CDF, sort(data_aggr(ind_bbc,iResult)), ...
     CDF, sort(data_aggr(ind_lb,iResult)), ...
     CDF, sort(data_aggr(ind_vm1,iResult)), ...
     CDF, sort(data_aggr(ind_vm2,iResult)));
xlabel('CDF')
ylabel('Success')
legend('BBC', 'LB', 'VM1', 'VM2');

