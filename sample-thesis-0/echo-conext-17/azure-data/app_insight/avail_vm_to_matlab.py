import os
import sys

import json
from pprint import pprint

import dateutil.parser


with open('output_vm.json') as data_file:    
    data = json.load(data_file)


metrics = []
for v in data:
	if not (v['metricName'] in metrics):
		metrics.append(v['metricName'])

	indm = metrics.index(v['metricName'])
	t = dateutil.parser.parse(v['time'])

	c = v["count"]
	tot = v["total"]
	min = v["minimum"]
	max = v["maximum"]
	
	print t.strftime('%s'), indm, c, tot, min, max
		
	
print "%%% Metrics "
for m in metrics:
	print "% ", m, metrics.index(m)

	



# {
# 	"count": 2,
# 	"total": 1.42,
# 	"minimum": 0.58,
# 	"maximum": 0.84,
# 	"resourceId": "/SUBSCRIPTIONS/8BB381B1-DE12-4FB7-A019-AEEEB2071090/RESOURCEGROUPS/OPENEPC0/PROVIDERS/MICROSOFT.COMPUTE/VIRTUALMACHINES/MYVM1",
# 	"time": "2016-10-14T02:00:00.0000000Z",
# 	"metricName": "Percentage CPU",
# 	"timeGrain": "PT1M"
# }
