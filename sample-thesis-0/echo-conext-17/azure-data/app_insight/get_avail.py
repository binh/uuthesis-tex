import os
import sys

from azure.storage.blob import BlockBlobService
from azure.storage.blob import ContentSettings

import json
from pprint import pprint


blob_name = 'openepcstorage'
blob_key = 'JBveOCI4mJIAGUSu6SR88WkbgBeGQUyNxN5fk/jGt2PXMC8cZ2fzWlifrBpDvkF9L0ywdwfM0VK22m5WONidGw=='
blob_container = 'app-insight'
file_name = 'output.json'

outfile = open(file_name, 'w')


bbs = BlockBlobService(account_name=blob_name, account_key=blob_key)
container = bbs.create_container(blob_container)

blobs = bbs.list_blobs(blob_container, include='metadata')



outfile.write("[\n")

first = True

for blob in blobs:
	print " *** ", blob.name
	bbs.get_blob_to_path(blob_container, blob.name, 'localfile.json')

	infile = open('localfile.json', 'r')
	for line in infile:
		dict = json.loads(line)
		for v in dict['availability']:
			do = {'testTimestamp' : v['testTimestamp'],
						'testName' : v['testName'],
						'runLocation' : v['runLocation'],
						'result' : v['result']}
			print json.dumps(do)
			if not first:
				outfile.write("\n, " + json.dumps(do))
			else:
				outfile.write(json.dumps(do))
				first = False


outfile.write("]\n")

outfile.close()



# AppInsight report JSON Template:
# {
#   "availability": [
#     {
#       "testRunId": "f8f7c655-7dc4-4cf9-9e02-03b7fdc20c4f",
#       "testTimestamp": "2016-10-06T12:56:06.112Z",
#       "testName": "testVM1",
#       "runLocation": "FR : Paris",
#       "durationMetric": {
#         "name": "duration",
#         "value": 610000.0,
#         "count": 1.0,
#         "min": 610000.0,
#         "max": 610000.0,
#         "stdDev": 0.0,
#         "sampledValue": 610000.0
#       },
#       "availabilityMetric": {
#         "name": "availability",
#         "value": 1.0,
#         "count": 1.0,
#         "min": 1.0,
#         "max": 1.0,
#         "stdDev": 0.0,
#         "sampledValue": 1.0
#       },
#       "result": "Pass",
#       "message": "Passed",
#       "dataSizeMetric": {
#         "name": "datasize",
#         "value": 0.0,
#         "count": 1.0
#       },
#       "count": 1
#     }
#   ],
#   "internal": {
#     "data": {
#       "id": "53b831d1-8bc4-11e6-8c35-d5a354760784",
#       "documentVersion": "1.61"
#     }
#   },
#   "context": {
#     "application": { },
#     "data": {
#       "eventTime": "2016-10-06T12:56:06.112Z",
#       "isSynthetic": false,
#       "samplingRate": 100.0
#     },
#     "cloud": { },
#     "device": {
#       "type": "PC",
#       "screenResolution": { }
#     },
#     "serverDevice": { },
#     "user": { "isAuthenticated": false },
#     "session": { "isFirst": false },
#     "operation": { },
#     "location": {
#       "clientip": "94.245.72.0",
#       "continent": "Europe",
#       "country": "United Kingdom",
#       "city": ""
#     },
#     "custom": { "dimensions": [ { "FullTestResultAvailable": "false" } ] }
#   }
# }
