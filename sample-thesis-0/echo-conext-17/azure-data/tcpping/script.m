data = load('mout.txt');

%time, resource, vm, gap, get_locality(n[0], resource), 0

iTime = 1;
iResource = 2;
iVM = 3;
iGap = 4;
iLocality = 5;   % 0 - same LB, 1 - same DC, 2 - other DC
iType = 6;       % 0 - LB, 1 - VMs
iDstRes = 7;
iDstVM = 8;


% Summary: from cat *tcpping.summary
% Order:
% bdeploy1myVM1.tcpping.summary
% bdeploy1myVM2.tcpping.summary
% bdeploy1myVM3.tcpping.summary
% deploy1weumyVM1.tcpping.summary
% deploy1weumyVM2.tcpping.summary
% deploy1weumyVM3.tcpping.summary
% deploy1wusmyVM1.tcpping.summary
% deploy1wusmyVM2.tcpping.summary
% deploy1wusmyVM3.tcpping.summary
% deploy2myVM1.tcpping.summary
% deploy2myVM2.tcpping.summary
% deploy2myVM3.tcpping.summary
% deploy2weumyVM1.tcpping.summary
% deploy2weumyVM2.tcpping.summary
% deploy2weumyVM3.tcpping.summary
% deploy2wusmyVM1.tcpping.summary
% deploy2wusmyVM2.tcpping.summary
% deploy2wusmyVM3.tcpping.summary

summary = [
40206311, 27103, .00067409815339686349; ...
23772046, 10186, .00042848646683587941; ...
23559775, 12101, .00051362969298306117; ...
23100551, 15478, .00067002730800663585; ...
14984479, 6490, .00043311482501326872; ...
6479004, 1153, .00017795945179228165; ...
23554331, 7838, .00033276258196422560; ...
23482083, 8765, .00037326330888107328; ...
23555003, 7976, .00033861171658521970; ...
23023040, 15122, .00065682029827511918; ...
23519806, 11004, .00046786100191472667; ...
23546155, 10661, .00045277031430397022; ...
23777763, 10681, .00044920121375589453; ...
23583702, 11655, .00049419722145403635; ...
23398920, 13418, .00057344527012357835; ...
23396951, 9332, .00039885538931974512; ...
23514525, 8418, .00035799149674509691; ...
15864317, 5950, .00037505554131325035
];



LOC = unique(data(:,iLocality));
TYP = unique(data(:,iType));





%% Find VM outages that are seen from all other VMs - suspected VM outage instead of network

ind = find(data(:,iGap) > 5);
d = [data(ind, iDstRes)*10 + data(ind, iDstVM), data(ind, iTime), data(ind, iTime)+data(ind, iGap), ...
     data(ind, iDstRes), data(ind, iDstVM), data(ind, iResource), data(ind, iVM)];
M = unique(d(:,1));
O = [];
for m = M'
    ind = find(d(:,1) == m);
    dd = sortrows(d(ind, 2:end));
    re = dd(1,3);
    vm = dd(1,4);
    j = 1;
    while j < size(dd,1)
        k = j+1;
        %fprintf('J: %d %d %d %d %d %d %d %d\n', j, dd(j,1), dd(j,2), dd(j,2) - dd(j,1), dd(j,3), dd(j,4), dd(j,5), dd(j,6))
        while (dd(j,1) < dd(k,1) && dd(j,2) > dd(k,1)) || ...
              (dd(j,1) < dd(k,2) && dd(j,2) > dd(k,2))
            %fprintf('K: %d %d %d %d %d %d %d %d\n', k, dd(k,1), dd(k,2), dd(j,2) - dd(j,1), dd(k,3), dd(k,4), dd(k,5), dd(k,6))
            k = k+1;
        end
        % We don't observe outages from all nodes because of bug in measurement
        % Some nodes with later outages would have deleted this measurement
        % But anything more than say 3 is certainly an outage
        if (k-j > 8)
            %fprintf('%d %d %d %d %d\n', re, vm, k-j-1, dd(j,1), dd(j,2)-dd(j,1));
            O = [O; [re, vm, k-j-1, dd(j,1), dd(j,2)-dd(j,1)]];
        end
        j = k;
    end
end



%% Find outages when a VM sees entire cluster/DC in outage

ind = find(data(:,iGap) > 5);
d = [data(ind, iResource)*10 + data(ind, iVM), data(ind, iTime), data(ind, iTime)+data(ind, iGap), ...
     data(ind, iDstRes), data(ind, iDstVM), data(ind, iResource), data(ind, iVM)];
M = unique(d(:,1));
O2 = {};
io = 1;
for m = M'
    ind = find(d(:,1) == m);
    dd = sortrows(d(ind, 2:end));
    re = dd(1,3);
    vm = dd(1,4);
    j = 1;
    while j < size(dd,1)
        k = j+1;
        V = [dd(j,[5,6,1,2,3,4])];
        %fprintf('J: %d %d %d %d %d %d %d %d\n', j, dd(j,1), dd(j,2), dd(j,2) - dd(j,1), dd(j,3), dd(j,4), dd(j,5), dd(j,6))
        while (dd(j,1) < dd(k,1) && dd(j,2) > dd(k,1)) || ...
              (dd(j,1) < dd(k,2) && dd(j,2) > dd(k,2))
            %fprintf('K: %d %d %d %d %d %d %d %d\n', k, dd(k,1), dd(k,2), dd(j,2) - dd(j,1), dd(k,3), dd(k,4), dd(k,5), dd(k,6))
            V = [V, dd(k,[3,4])];
            k = k+1;
        end
        if (size(V,2) > 6)
            %V([1:2,5:end])
            O2{io} = V;
            io = io + 1;
        end
        j = k;
    end
end



fprintf('Total availability: %.5f\n', sum(summary(:,2)) / sum(summary(:,1)));
fprintf('Total availability: %.5f\n', sum(data(:,iGap)) / sum(summary(:,1)));



% Total availability across all pairs of VMs that suit description
for thr = [0, 1, 10, 60]
    indVM = find(O(:,5) > thr);
    ind0 = find(data(:,iLocality) == 0 & data(:,iGap) > thr);
    ind1 = find(data(:,iLocality) == 1 & data(:,iGap) > thr);
    ind2 = find(data(:,iLocality) == 2 & data(:,iGap) > thr);

    % summary(:,1) gives us total number of pings per all VMs, which pings 17 other VMs every sec
    % so there are summary(:,1)/17 pings per VM per measurement time
    avm = 1 - sum(O(indVM,5)) / (sum(summary(:,1)) / 17) - 1;
    ac = 1 - sum(data(ind0,iGap)) / (sum(summary(:,1)) * 2 / 17) - 1;
    adc = 1 - sum(data(ind1,iGap)) / (sum(summary(:,1)) * 3 / 17) - 1;
    aadc = 1 - sum(data(ind2,iGap)) / (sum(summary(:,1)) * 3*4 / 17) - 1;
    
    fprintf('Total availability (>%d s): VM=%.9f, DC clust=%.9f, DC=%.9f, inter-DC=%.9f\n', thr, avm, ac, adc, aadc);
end


% O2 has list of cases where more than one end VM was unavailable at a time
% We don't print it specifically here as it is empty
outage = [0,0,0];
for j = 1:length(O2)
    o = O2{j};
    r = o(5:2:end);
    d = o(4)-o(3);
    U = unique(r);
    D = [];
    for u = U
        num = length(find(r == u));
        if num >= 3
          D = [D, u];
        end
    end
    %[d, r]
    outage(1) = outage(1) + d*length(r);
    if (length(D) >= 2)
        %[o(1:2), d, r]
        outage(3) = outage(3) + d;
        outage(2) = outage(2) + d;
    elseif (length(D) == 1)
        [o(1:2), d, r]
        outage(2) = outage(2) + d;
    end
end    


fprintf('Total availability of DC cluster: %.9f\n', 1 - outage(2) / (sum(summary(:,1)) * 6 / 17) );
fprintf('Total availability of DC: %.9f\n', 1 - outage(3) / (sum(summary(:,1)) * 3 / 17));


%aaa


lw = 3;
fs = 24;
sty = {'-.k', ':b', '--r', '-g'};


figure(1);
clf;

% $$$ subplot(1,2,1)
% $$$ 
% $$$ ind0 = find(data(:,iLocality) == 0);
% $$$ ind1 = find(data(:,iLocality) == 1);
% $$$ ind2 = find(data(:,iLocality) == 2);
% $$$ 
% $$$ X0 = sort(data(ind0, iGap));
% $$$ X1 = sort(data(ind1, iGap));
% $$$ X2 = sort(data(ind2, iGap));
% $$$ 
% $$$ Y0 = (0:length(ind0)-1)/(length(ind0)-1);
% $$$ Y1 = (0:length(ind1)-1)/(length(ind1)-1);
% $$$ Y2 = (0:length(ind2)-1)/(length(ind2)-1);
% $$$ 
% $$$ semilogx(X0, Y0, X1, Y1, X2, Y2);
% $$$ ylabel('CDF');
% $$$ xlabel('Gap duration [s]');
% $$$ legend('Same LB', 'Same DC', 'Other DC', 'Location', 'SouthEast');
% $$$ title('Outage duration distribution across all VMs and LBs');



load('../app_insight/gaps.mat');


%subplot(1,2,2)

ind0 = find(data(:,iLocality) == 0 & data(:,iGap) > 1);
ind1 = find(data(:,iLocality) == 1 & data(:,iGap) > 1);
ind2 = find(data(:,iLocality) == 2 & data(:,iGap) > 1);

X0 = sort(data(ind0, iGap));
X1 = sort(data(ind1, iGap));
X2 = sort(data(ind2, iGap));
% These are at least 10 mins as we don't have the starting point
X3 = sort(gaps(:,4)-gaps(:,3) + 600);

Y0 = (0:length(ind0)-1)/(length(ind0)-1);
Y1 = (0:length(ind1)-1)/(length(ind1)-1);
Y2 = (0:length(ind2)-1)/(length(ind2)-1);
Y3 = (0:length(gaps)-1)/(length(gaps)-1);

semilogx(X0, Y0, sty{1}, X1, Y1, sty{2}, X2, Y2, sty{3}, X3, Y3, sty{4}, 'LineWidth', lw);
ylabel('CDF', 'FontSize', fs);
xlabel('Outage duration [s]', 'FontSize', fs);
legend('Same LB', 'Same DC', 'Other DC', 'World-wide', 'Location', 'SouthEast');
set(gca, 'FontSize', fs);
%title('Outage duration distribution across all VMs and LBs');
%title('Outage duration distribution across all pairs of VMs');



aaa


%figure(2);
%clf;
subplot(1,3,2);

ind0 = find(data(:,iLocality) == 0 & data(:,iType) > 0);
ind1 = find(data(:,iLocality) == 1 & data(:,iType) > 0);
ind2 = find(data(:,iLocality) == 2 & data(:,iType) > 0);

X0 = sort(data(ind0, iGap));
X1 = sort(data(ind1, iGap));
X2 = sort(data(ind2, iGap));

Y0 = (0:length(ind0)-1)/(length(ind0)-1);
Y1 = (0:length(ind1)-1)/(length(ind1)-1);
Y2 = (0:length(ind2)-1)/(length(ind2)-1);

semilogx(X0, Y0, X1, Y1, X2, Y2);
ylabel('CDF');
xlabel('Gap duration [s]');
legend('Same LB', 'Same DC', 'Other DC', 'Location', 'SouthEast');
title('Outage duration distribution across VMs');



%figure(3);
%clf;
subplot(1,3,3);

ind0 = find(data(:,iLocality) == 0 & data(:,iType) == 0);
ind1 = find(data(:,iLocality) == 1 & data(:,iType) == 0);
ind2 = find(data(:,iLocality) == 2 & data(:,iType) == 0);

X0 = sort(data(ind0, iGap));
X1 = sort(data(ind1, iGap));
X2 = sort(data(ind2, iGap));

Y0 = (0:length(ind0)-1)/(length(ind0)-1);
Y1 = (0:length(ind1)-1)/(length(ind1)-1);
Y2 = (0:length(ind2)-1)/(length(ind2)-1);

semilogx(X0, Y0, X1, Y1, X2, Y2);
ylabel('CDF');
xlabel('Gap duration [s]');
legend('Same LB', 'Same DC', 'Other DC', 'Location', 'SouthEast');
title('Outage duration distribution across LBs');




    

