\section{Implementation}
\label{sec:implementation}

Section~\ref{sec:architecture} outlines
general design principles \Name{} uses to provide safety and reliability.
Here we discuss specifically how this design applies to a cellular control plane and a public cloud. 
The summary of changes to the standard EPC architecture is illustrated in Figure~\ref{fig:agent-layer}.



\vskip 6pt
\noindent\textbf{\Name agents:}
%\label{enodeb-agent}
\Name{}'s agents are lightweight software proxy agents that provide entry-point functionality on eNodeB
and an interface between eNodeB and MME.
There are two agents, one that resides on eNodeB and one on MME, as illustrated in Figure~\ref{fig:agent-layer}.
The eNodeB's agent is implemented as a separate user-mode daemon written in standard C,
deployed on top of embedded Linux running on a commodity small cell~\cite{ipaccess}.
This allows us to easily port it to any COTS eNodeB without affecting the time-critical LTE radio code. 
The MME's agent is integrated in the source code of the S1AP processing module of OpenEPC~\cite{openepc}.

One of the agent's functions is to proxy S1AP control messages.
3GPP eNodeB and MME use SCTP protocol for S1AP messages. 
However, Azure and other public clouds do not support SCTP protocol, so we implement a proxy agent that replaces SCTP by TCP.
The \Name agent 
on eNodeB opens an SCTP connection to the rest of eNodeB software stack on one side
(which is unmodified and unaware of the agent's existence) 
and a TCP connection to an \Name{} MME agent on the other side. 
The eNodeB agent relays messages between the two connections. 
The agent reestablishes the TCP connection on a failure, in order to attach to a new MME instance (in the same DC or a different DC).
%in case the old one failed.


    \begin{figure}[]
      \centering
      \includegraphics[scale=0.5]{figs/mme-implementation.pdf}
      %\vspace{-3mm}
      \caption{Modifications of \Name{} in LTE/EPC}
      \label{fig:agent-layer}
      %\vspace{-7mm}
    \end{figure}


%% If the TCP connection between eNodeB agent and \Name{} MME agent breaks, 
%% the eNodeB agent also breaks its connection with eNodeB. 
%% This causes the eNodeB to re-establish the connection with \Name{} MME and keep retrying.
%% If \Name{} MME instance restarts, 
%% cloud's Load Balancer (described next) will forward the connection setup request
%% to another available \Name{} MME instance. This way the eNodeB will communicate with 
%% another \Name{} instance immediately after the crash.

Furthermore, the agent implements the entry point design, described in Section~\ref{sec:entry-point}. 
The agent adds an extra network layer (\Name{} or agent layer) into the LTE/EPC control network stack, as shown in Figure~\ref{fig:agent-layer}.
The \Name{} layer header consists of
the \textit{Request ID};
a \textit{UE-ID}, a unique identifier of the UE, composed of tunnel identifiers readily available from S1AP messages; and
a \textit{Timer value}, used to set up timers and to inform components about timer expiry.

%On the MME's side, \Name{} agent acknowledges each S1 (control) request from the eNodeB's agent. 
%It extracts eNodeB's agent messages and passes them to MME's state machine.



\vskip 6pt
\noindent\textbf{Stateless EPC components:}
We have augmented the most important EPC components
(MME, SGW and PGW) in OpenEPC~\cite{openepc} with \Name functionality. 
In the example of MME, our implementation preserved the original implementation that 
extracts information from a received S1AP message, generates side effects and updates the client's state
(e.g., steps~\ref{lst:line:update_session_after_req},~\ref{lst:line:send_side_effect},~\ref{lst:line:update_session_after_side_effect} in the algorithm).
We extended handlers to accept request IDs from the \Name{} layer
and to add duplicate/stale request checks
that adapt processing accordingly (step~\ref{lst:line:check_sequence_number}).
When the original MME code finishes processing a request, \Name{} sends an acknowledgment to the eNodeB agent together with an S1AP reply.
We made SGW/PGW operations idempotent by making the SGW 
reply with a stored message (i.e., with the same bearer information) for duplicate requests from the MME (so, the duplicates don't forward effects to the PGW).
In all, \Name{}'s extensions to OpenEPC required changing 1,410 lines in 12
files.

%Figure~\ref{fig:service-request-echo} shows how \Name{} MME processes a message in a Service Request procedure, with additional operations/information highlighted in red.


%% We extended handlers in conventional MME state machine 
%% to accept sequence ID from the \Name{} layer. 
%% We preserved the MME state machine as defined in 3GPP 
%% and added on top of it the transaction logic as in algorithm~\ref{alg:transaction}.
%% For example, \Name{} MME preserved 3GPP protocols that 
%% extract information in a received S1AP message, generate a side effect 
%% and enter a defined state (e.g., steps~\ref{lst:line:update_session_after_req},~\ref{lst:line:send_side_effect},~\ref{lst:line:update_session_after_side_effect} in the algorithm.) In addition, 
%% before processing a request, \Name{} checks whether a request is duplicated 
%% and processes it accordingly (e.g., steps~\ref{lst:line:check_sequence_number}.)
%% When the 3GPP finishes processing a request, \Name{} sends an acknowledgment 
%% to the eNodeB agent together with an S1AP reply.


We added two additional blocks to the conventional EPC:
an \textit{agent} (described previously) and a \textit{ZooKeeper client} (ZK-client).
The ZK-client provides a \textit{read/write/delete} interface to a ZooKeeper~\cite{zookeeper} (ZK) cluster
that acts as a reliable, persistent storage.
ZooKeeper is a reasonable choice of storage because of its consistency guarantees,
small amount of stored information (a few KBs per UE context) and relatively low 
request rate. 
The UE context (which is extended to include UE replies) is stored as a binary string in a znode in ZK. 
%The UE-ID, an unique identifier of an UE, is used as the znode name. 
\Name uses the \textit{version number} of a znode in ZK to realize an atomic state update at step~\ref{lst:line:commit} of the algorithm; 
ZK only allows updating the znode if the version number hasn't changed since the beginning of the request. 


%% A \textit{version number} of a znode is incremented when the node is updated.
%% Beginning of a transaction when reads a znode, \Name{} MME instance also retrieves the \textit{version} number of that node.
%% Later at the end of the transaction MME passes the read version number together with its write request.
%% Zookeeper only accepts the write if the version number hasn't changed.


%% We already say this in Section 4:
%% We implemented \Name{} MME and SGW based on OpenEPC implementation~\cite{openepc}. 
%% We implemented the idempotent property on SGW/PGW by making the SGW 
%% reply exactly the same message (i.e., with the same bearer information) 
%% when it detects a retry from the MME (without 
%% sending the Modify Bearer Request to PGW.) This way, duplicated requests 
%% from MME instances will not mutate states on SGW and PGW.


%%%%  BR: All this is already discussed before
%After fetching, \Name{} only processes the request if the received transaction ID is one smaller than (a retry) or equals to (a new request) the recorded transaction ID. 
%If the received transaction ID is larger than the recorded ID, \Name{} fetches ZK because the local UE context could potentially be obsolete.
  
%% When receives an UE request, the \Name{} MME state machine 
%% executes the steps in algorithm~\ref{alg:transaction}. Figure~\ref{fig:service-request-echo} 
%% shows how \Name{} MME processes a message in a Service Request procedure.
%% Additional operations/information are highlighted in red.
%% When receives the Service Request (msg. \#1), \Name{} 
%% checks the transaction ID included to decide whether to process it (step~\ref{lst:line:check_sequence_number} in algorithm~\ref{alg:transaction}.)  
%% If the request happens to be a retry, \Name{} simply sends the reply stored in the UE context and returns (step~\ref{lst:line:check_reply_exists}.)
%% Otherwise, it enters 3GPP state machine to process the message and create Modify Bearer Request (msg \#2.)

%% The Modify Bearer Request sent by MME also has the transaction ID embedded.
%% This message is a side effect in step~\ref{lst:line:send_side_effect} in the algorithm. 
%% When receives an acknowledgment for this side 
%% effect (msg. \#6,) \Name{} updates the state for that UE, generates a reply as defined in 3GPP.
%% In addition \Name{} commits UE state (msg. \#7) together with the generated reply into ZK (steps~\ref{lst:line:receive_side_effect_reply}-~\ref{lst:line:receive_side_effect_reply}.) If \Name{} successfully commits, it sends the reply (msgs. \#8) to UE. Otherwise, it knows that another \Name{} MME was faster and already sent the reply therefore it silently returns.

%% Similar algorithm happens on SGW. 
%% When receives the \textit{Modify Bearer Request}, 
%% SGW processes the request in a similar way the MME processed Service Request - 
%% if the embedded transaction ID 
%% implies a retry then SGW simply replies with the (recorded) Modify Bearer Response (msg. \#6). 
%% Otherwise, it processes the request and sends another side effect message to PGW (msg. \#3.) 
%% When receives an acknowledgment from PGW, similarly, SGW commits its 
%% state change into ZK and sends a reply to MME (msg. \#6) it the commit succeeds.







\vskip 6pt
\noindent\textbf{Cloud deployment:}
Multiple instances of the same component are deployed in a private network in Azure behind a load balancer.
The load balancer performs consistent hashing on the connection's 5-tuple,
so a connection sticks with the same instance unless there is a failure or a new instance 
is added. 
%This masks most of the intra-DC failures from the \Name's deployment. 
When \Name{} is deployed across multiple data centers, requests that time out a
few times are retransmitted to another data center by the
\Name{} agent on the eNodeB.




%% Stateless \Name{} MME  instances form a MME pool behind a cloud's Load Balancer (LB.) The LB 
%% performs consistent hashing on eNodeB's source address and port and 
%% provides a sticky connection between eNodeB and a MME instance. 
%% The LB also allows to add/remove MME instances to scale the MME pool.
%% The LB constantly probes the status of MME instances. 
%% In case a MME instance is slow or overloaded, the LB forces  
%% a disconnect to eNodeB which causes re-establish another S1 connection to 
%% a new \Name{} MME instance. For example, a monitoring process on a \Name{} 
%% MME instance could monitor processing latency of requests and notify the 
%% LB if the processing latency exceeds a threshold (a sign of MME overloading.)
%% To increase redundancy further, multiple MME pools could be deployed at different locations (data centers.) 
%% If a LB is not reachable, the S1 connection will timeout (because heartbeats fail) and 
%% the eNodeB will reestablish a new S1 connection.
%% The eNodeB agent keeps a set of available LBs and switch LB if a location 
%% becomes completely unavailable.




