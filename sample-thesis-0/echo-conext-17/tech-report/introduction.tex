\section{Introduction}

%More and more companies move their services to one of the public cloud platforms
%(such as Amazon AWS, Microsoft Azure, Google Cloud Platform, IBM Cloud, etc).
%The main motivation is the economies of scale.
%Data centers offering cloud computing are huge and thus can be made very efficient.
%They allow tenants to quickly scale their resource utilization up or down as required, and be charged only for what they consume. 

%As a parallel trend, telcos and cellular operators are attempting to virtualize their core networks.
%Most of the appliances running in cellular core networks today are built on top of custom hardware.
%This poses challenges in upgrade, scalability, and speed of introducing new services~\cite{etsi_nfv}.
%Instead, the new approach is based on network function virtualization (NFV).
%NFV functions are implemented in software on top of standardized general-purpose hardware (e.g., x86 architecture),
%running in a data center.
%This offers much more flexibility as it allows operators to quickly scale a service up or down, or replace it with another service. 
%By some accounts, most cellular operators will deploy some form of NFV by the end of this year~\cite{nfv_2017}.


Recent years have seen a tremendous uptake of cloud computing. 
More and more companies move their services to the public cloud to take advantage
of the economies of scale, the resource elasticity and scalability that the cloud offers. 
In stark contrast, the telco industry today faces major 
challenges in equipment upgrading, scaling, and introducing new services~\cite{etsi_nfv}. 
Cellular core networks are largely still based on custom-built hardware 
mandated by the strict reliability requirements posed by running a mobile core network.

To alleviate these challenges, telcos and cellular operators are attempting to virtualize their core networks
through network function virtualization (NFV)~\cite{att2.0}. Typically, this is in the form of a move to a private-cloud setting,
where the telco provider has full control of the infrastructure and can optimize the whole stack for 
its particular services. Indeed, owning the whole cloud stack can provide specialized
additional for fault tolerance and management -- open source cloud software stack OpenStack and its OpNFV layer provide 
such services  (e.g., see Vitrage~\cite{nfv_vitrage} and Doctor~\cite{nfv_doctor}). 
However, such a deployment model still cannot take full advantage of the economies of scale a public deployment can offer.
Telco providers will have to manage and maintain the new private cloud deployments, while at the same time, 
super-optimized cloud stacks for a particular core service
might not be able to scale to the size of a public cloud, and may be at odds with the requirements of a new service to be introduced.

Instead, the question we address is whether it is feasible to
implement a cellular core network on top of a \emph{public} cloud, such as Amazon AWS or Microsoft Azure.
To achieve this, one has to address two main challenges. 
First, reliability - a cellular core network today requires ``five 9s'' reliability 
(i.e., availability of 99.999\%)~\cite{ericsson.high.availability, nokia.9471.wmm}.
Typical public cloud availability SLAs are four 9s or less, which 
means an order of magnitude more expected outages. 
Second, service abstractions mismatch. 
Naturally, public clouds are optimized for general workloads,
offering basic network abstractions such as a network node, a private network, or a load balancer. 
Cellular core networks are complex, with multiple different components implementing 
distributed state machines that will need to be redesigned atop the cloud's abstractions.
 
In this paper, we introduce \Name{}, a distributed cellular network architecture for the public cloud.
We focus on the evolved packet core (EPC)~\cite{epc.3gpp}, which is a key component of a cellular network without which a network cannot run.
EPC manages user devices (Section~\ref{sec:corearch}) and
provides core network control and data plane functionality
for all cellular radio technologies (2G, 3G and 4G/LTE).
\Name{} is specifically designed
as a distributed EPC cloud-based architecture.
While operator networks consist of the EPC and middleboxes, \Name{} focuses on the core EPC which is fundamentally different than
other middle-boxes as it requires consistency across multiple types of network components {\em and} end-user devices. 
Although much effort has already been devoted 
to virtualizing conventional middleboxes~\cite{opennf, Sherry_cloud, fayazbakhsh_mb, sekar_mb},
these do not address the main EPC design challenges in distributed environments. 


\Name{} provides the same properties that EPC guarantees, but it also remains
correct and available under failures.
%Today, EPC works as a distributed state machine that stores user states across multiple components and users' mobile devices. 
%State inconsistencies across the components can lead to long service outages (Section~\ref{sec:control-plane-requirements}).
To make EPC safe against failures,
\Name{} must ensure the state machine remains consistent in spite of potential component and network failures. To do this,
\Name{} must ensure two properties: (i) a state change across multiple distributed functional components and mobile devices must appear to be atomic -- 
the distributed state machine must be either in a ``before'' or ``after'' state; 
and (ii) the distributed components must appear to execute requests in the 
order that the requests are generated by the user's mobile device.
%This is fundamentally different 
%from the requirements of conventional middleboxes where state is typically shared only across multiple instances of the same appliance. 
In contrast, conventional middleboxes typically share state only across multiple instances of the same functional component. 

%To achieve consistency, our design leverages the ``necessary'' reliability of access points - 
%mobile devices are only connected to the network as long as their associated access points are operational.
%\Name introduces a thin software layer (entry point agent) on access points  which 
%enforce consistency between the state at the mobile device and corresponding states on components in the cloud.
%The entry point serializes all state updates from a single transaction across all EPC components in the cloud.
%This scales well since each user only executes a limited number of transactions, 
%and the scalability is required across users. 
%This effectively means that EPC components in the cloud can be replicated by refactoring 
%their functionality into a stateless front-end and persistent
%backend storage in which state is stored after each operation. 
%\Name further takes care of a non-trivial task of keeping interactions among these cloud components (which we call side-effects) consistent.
%In doing so, \Name avoids modifying the standard core protocols
%and provide abstractions that nicely overlay with the existing cellular standards. 

Implementing a generic distributed state machine is a challenging task.
\Name{} proposes a novel architecture that is specifically taylored for EPC and thus much simpler.  
To achieve atomicity across distributed components, \Name{} leverages the ``necessary'' reliability of access points - 
mobile devices are only connected to the network as long as their associated access points are operational. 
\Name{} introduces a thin software layer (entry point agent) on access points which 
ensures the eventual completion of each request - the entry point agent 
keeps sending a request over and over until all of the distributed components in 
the core network agree on a state before it moves to the next request. 
If a core component instance crashes in a middle of an execution, 
another instance can safely recover from another retry from the agent. 
\Name{} also guarantees in-order execution of requests generated by the 
user's mobile device.

%To achieve in-order execution of requests, \Name{} introduces 
%a monotonic sequence number on each request from the mobile device. 
%The sequence number is added at the entry point and is used 
%by each of the distributed components  
%so that requests are executed in the order specified by their sequence number.


%In spirit, \Name{} is similar to other reliable distributed cloud services, and
%it relies on well-known techniques like redundant stateless components, external state
%storage, and state machine replication for high availability and strong
%consistency. The key challenge in \Name{} is dealing with distributed side effects,
%which must appear to execute atomically and in order (linearizably) from the client's
%perspective. Ordinarily, this is achieved through mutual exclusion, but \Name{}
%must be fully non-blocking to provide high availability. 


%enforce consistency between the state at the mobile device and corresponding states on components in the cloud.
%The entry point serializes all state updates from a single transaction across all EPC components in the cloud.
%This scales well since each user only executes a limited number of transactions, 
%and the scalability is required across users. 
%This effectively means that EPC components in the cloud can be replicated by refactoring 
%their functionality into a stateless front-end and persistent
%backend storage in which state is stored after each operation. 
%\Name further takes care of a non-trivial task of keeping interactions among these cloud components (which we call side-effects) consistent.
%In doing so, \Name avoids modifying the standard core protocols
%and provide abstractions that nicely overlay with the existing cellular standards. 


%Each core network operation has a unique, in-order ID assigned by the agent,
%that helps enforcing correct execution.




%New NFV functions are getting deployed in operators' private data centers, built for the purpose of serving core networks. 
%The natural question to ask is whether cellular operators can deploy their core networks in a public cloud
%and benefit from their economies of scale. 
%The two main technical challenges are reliability and service abstractions.
%A cellular core network require five 9s reliability (99.999\%)~\cite{ericsson.high.availability, nokia.9471.wmm}.
%A typical public cloud availability is four 9s or less, which means 10 times more expected outages. 
%To achieve five 9s, a virtualized core network in a public cloud has to include fail-over replicas that have to be managed seamlessly in software.

%Implementing fault tolerance is easier with more support from infrastructure.
%For example, a popular open source cloud software stack OpenStack and its OpNFV layer provide alarm management systems
%(Vitrage~\cite{nfv_vitrage} and Doctor~\cite{nfv_doctor}).
%These systems have knowledge of the network infrastructure. This allows them to map a hardware fault on a switch to all downstream components whose network connectivity will be impacted by the fault and quickly reallocate the impacted resources.
%However, such alert system may be difficult to provide at a scale of a public cloud.
%For example, Azure's network load balancer, Ananta~\cite{ananta}, is implemented in software on top of thousands of hosts.
%Adding new functionality to cater for a small fraction of workload may not be economically feasible. 
%The service abstractions provided to all tenants in a public cloud have to satisfy diverse workloads but also be efficient to deploy and manage. 
%For example, a typical public cloud offers three main network abstractions: a network node, a private network, and a load balancer, which is able to regularly poll nodes behind it and reroute traffic to another node if one node stops responding.
%All services built on the cloud have to rely on these network abstraction for providing reliability. 

%A central question we address in this paper is whether it is technically feasible to
%implement a cellular core network on top of a public cloud, given the service abstractions
%and reliability guarantees public clouds offer today.
%We focus in particular on the evolved packet core (EPC)~\cite{epc.3gpp}, which is the main part of existing
%cellular networks today,
%providing core network control and data plane functionality
%for all cellular radio technologies (2G, 3G and 4G/LTE).
%A typical operator's network consists of an EPC and various middle-boxes~\cite{etsi_nfv, Sherry_cloud}. 
%Much effort has already been devoted to virtualizing conventional middleboxes (such as caches, firewalls, traffic analysis, etc; c.f.\cite{opennf, Sherry_cloud, fayazbakhsh_mb, sekar_mb}).
%However, to the best of our knowledge, no one has yet attempted to run an EPC on a public cloud.

%The role of the EPC is to manage users (explained further in Section~\ref{sec:corearch})
%and the main design challenges lay in the fact that it is a distributed state machine that stores user states across multiple components,
%as well as on the user's mobile device.
%All these states need to be consistent in spite of potential components and network failures.
%Inconsistencies can lead to long service outages (as we illustrate in Section~\ref{sec:control-plane-requirements}).
%This is fundamentally different from requirements of other middleboxes where state is typically shared only across multiple instances of the same appliance. 

%In this paper we introduce \Name{}, a distributed EPC architecture for public cloud.
%\Name{} starts from the observations that mobile devices are only
%connected to the network as long as their associated access points are operational.
%It leverages this ``necessary'' reliability of access points and
%introduces a thin software layer (entry point agent) on each of them.
%The entry point agent's role is to help enforce consistency between the state at the mobile device and corresponding states on components in the cloud.
%The entry point also helps serializing all state updates from a single transaction across all EPC components in the cloud.
%This scales well since each user only executes a limited number of transactions, and the scalability is required across users. 
%EPC components in the cloud can be replicated by refactoring 
%their functionality into a stateless front-end and persistent
%backend storage to in which state is stored after each operation. 
%Each core network operation has a unique, in-order ID assigned by the agent,
%that helps enforcing correct execution.


Our contributions can be summarized as follows:

\begin{trivlist}
\vspace{-\topsep}
\item $\bullet$ We propose \Name{}, a distributed EPC architecture for the public cloud.
  \Name{} uses conventional distributed systems techniques like stateless
  redundant components, external state storage, and load balancing for high
  availability and scalability but with a focus on correctness.  Its key
  contribution is that it uses the unmodified EPC protocol while eliminating
  correctness issues and edge cases that otherwise result from unreliable and
  redundant components.
\item $\bullet$ The core of \Name{} is an end-to-end distributed state machine 
  replication protocol for a software-based LTE/EPC network running on an unreliable 
  infrastructure. \Name{} ensures atomic {\em and} in order execution of 
  side-effects across distributed components 
  using a {\em necessarily reliable agent}, an {\em atomic and in-order execution} on cloud components. Cloud components in \Name{} are always {\em non-blocking} to ensure performance and availability. 
\item $\bullet$ We demonstrate the feasibility of the proposed architecture by implementing it in full.
  We implement the entry-point agent software and deploy it on a COTS LTE small cell~\cite{ipaccess}.
  Additionally, we implement the required EPC modifications into OpenEPC~\cite{openepc} and deploy \Name on Azure.
\item $\bullet$ We perform an extensive evaluation of the system using real mobile phones as well as synthetic workloads.
  We show that \Name{} is able to cope with host and network failures, including 
  several data-center component failures, without end-clients noticing it.
  \Name shows performance comparable to commercial cellular networks today. 
  Compared to a local deployment, \Name{}'s added reliability introduces an
  overhead of less than 10\% to latency and throughput of
  control procedures when replicated within one data center.
\vspace{-\topsep}
\end{trivlist}

To the best of our knowledge, \Name{} is the first attempt to run an EPC on a public cloud and the first attempt to replicate the LTE/EPC state machines in an NFV environment. 
\Name{} is a step toward relieving telcos from the burden of managing their
own infrastructure.
%and allow them to focus on offering new services and experiences.
%Moreover, we hope that \Name's flexible architecture 
We hope it will help inspire the next generation of 5G cellular networks,
which will require greater scale and decentralization than the current architecture. 
%\comment{thomas: some visionary last sentence here for future cellular core deployments and new research directions...}



%In the following, we start by a brief description of the EPC architecture and explain how it can be represented as a distributed state machine
%(Section~\ref{sec:motivation}).
%We then demonstrate why reliability is important for EPC and we measure how much reliability we can expect from a public cloud (Section~\ref{sec:mme-requirements}).
%We further give an overview of \Name{}'s design (Section~\ref{sec:architecture}),
%and implementational details (Section~\ref{sec:implementation}).
%We evaluate \Name{}'s performance (Section~\ref{sec:results}) and
%finish with related works (Section~\ref{sec:related}) and conclusions (Section~\ref{sec:conclusions}). 





