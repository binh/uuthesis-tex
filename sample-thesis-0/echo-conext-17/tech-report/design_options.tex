\subsection{Design options}
  \label{sec:design_options}
  \subsubsection{State machine replication}
  \label{sec:ds-techniques}

  The distributed state machine associated with a core mobile network
  presents a significant challenge for cloud deployments. Naturally, 
  cloud deployments imply replication of cloud based core
  network components. 


  Nominally, UE contexts are state machines, so using
  consensus-based state machine
  replication~\cite{paxos,paxos-simple,paxos-live,raft} might seem like an easy
  way to make a redundant and highly reliable MME that behaves identically to a
  single MME instance.
  %
  Unfortunately, the side effects that an MME performs on state
  transitions make this hard; the (distributed) effects fundamentally cannot be
  atomic with the state change in the MME.
  %
  If an MME replica crashes in the middle of interacting with
  other entities, then the intended effect may not have been achieved.
  %
  Another MME replica must complete the work started by the failed MME replica,
  but it cannot know where to resume to avoid duplicating effects.
  %
  Lost side effects are not an option, so the system must work correctly even
  when side effects are performed more than once.

  In current deployments, an SCTP connection between the eNodeB and the MME provides
  retransmission, de-duplication, and request ordering. This works because the
  SCTP connection fails when the MME fails. Retransmission, de-duplication, and
  reordering must all be solved when MME functionality is replicated and spans multiple
  machines and connections.  Compounding things, in some cases MMEs set timers;
  replicating the timers is needed for reliability, but it affects timer
  semantics and raises clock synchronization questions.
  For example, replicas might cause a timer to fire more than once.


  \subsubsection{Stateless design with persistent state storage}
    \label{sec:stateless_design}

    \Name{} separates state from computation and adopts known 
    techniques to improve availability and scalability; 
    it uses redundant stateless instances for computation and external replicated 
    state storage to store state. \Name{} uses atomic write   
    provided by a replicated key-value store (i.e., Zookeeper) to update states. 


    At a high level, \Name{} seems similar to other ACID transactions; 
    it needs to guarantee atomicity of operations and concurrency 
    control of stateless instances.
    However, ACID transactions only guarantee serializability which 
    is not sufficient because to guarantee correctness, 
    requests from a mobile device must be executed by 
    \Name{} in their arrival order (see example~\ref{fig:fifo-violation}) which 
    ACID transactions do not guarantee. Moreover, a request in \Name{} 
    should be processed eventually so that a component failure shouldn't be visible 
    to the mobile device. Lastly, timer events on components must be reliably triggered 
    to be equivalent with the conventional mobile core network.


    To ensure eventual completion and in-order execution, 
    \Name{} proposes adding a {\em necessarily 
    reliable entry point} at each base station that adds a {\em monotonic increasing ID} in 
    each transaction and persistently retries until a request is completed. 
    In the core network, \Name{} 
    proposes a {\em linearizable transaction algorithm} 
    and applies the algorithm {\em recursively} to 
    all components to guarantee atomic and in-order execution across distributed 
    components. To reliably handle timers, \Name{} proposes a {\em distributed timers} 
    mechanism to piggyback timer events on components to the reliable agent which 
    then triggers timeout events as transaction requests. More details about 
    the mechanisms will be in the following section.

%BN: another approach to give an overview of contributions. Now park here for reuse if needed.
%   \noindent
%    \textbf{Monotonicity and Eventual Completeness.} 
%    A mobile core network must reply to requests in the 
%    order that they arrive (monotonicity). 
%    Moreover, each request must be processed eventually (eventual completeness). 
%    A component failure shouldn't be visible to the user device
%    (unless there is a major outage whose scale we don't address).
%    To ensure in-order execution of requests, \Name{} introduces {\em necessarily 
%    reliable agent} at base stations. The agent serializes 
%    requests from the mobile device by adding an monotonic increasing ID into the %requests. The agent also persistently retransmits 
%    requests until there is a reply to guarantee eventual completeness. More 
%    details are in section\S~\ref{sec:entry-point}.


%    \textbf{Atomicity and Idempotence.} 
%    Retries from the agent create duplicated requests on 
%    the stateless instances. At a first sight, \Name{} seems needed to only 
%    solve the concurrency control problem when there are 
%    multiple stateless instances manipulate the same 
%    state in the external storage. However, unlike other 
%    distributed systems, a component in the mobile core 
%    network not only modifies the state but also induces a {\em side effect} 
%    on another component. Moreover, the side effect propagates and 
%    affects states on multiple components.


%    The challenge that \Name{} must solve is to 
%    maintain state consistency across distributed components 
%    when there are side effects. In particular, 
%    \Name{} must ensure the state change across distributed 
%    components is atomic. However, retries from 
%    the agent create multiple side effect requests. Without 
%    care, interleaved side effects could be processed in 
%    different orders and cause inconsistency. For example, 
%    as in figure~\ref{fig:service-request}, a duplicated Modify 
%    Bearer Request could overwrite previously installed 
%    state on the SGW, or a stale Delete Bearer Request 
%    could delete a valid bearer in the SGW, both cases result in 
%    state inconsistency between the MME and SGW.


%    To solve this, \Name{} embedded 
%    the ID from the agent inside a side effect. If receives 
%    a duplicated side effect, the component simply replies 
%    with a {\em logged-reply} in the external storage without 
%    processing the request. If receives a stale 
%    side effect, the component simply ignores.

\iffalse
\xxx{BN: text used in sigcomm here\\
    We enforce the appearance of sequential and atomic transactions through the following high-level properties we impose on our transaction processing:

    \noindent
    \textbf{Monotonicity.}
    Message delays and retransmissions can cause operations to be processed concurrently.
    We must ensure transactions are processed in the correct order,
    and that each component instance can only commit the next unacknowledged transaction
    to avoid stale effects and inconsistency. 

    \vskip 4pt
    \noindent
    \textbf{Atomicity and Idempotence.}
    Component instances modify local session state in isolation, and they install shared session state changes atomically.
    When processing a transaction, if a component instance fails or detects at commit time that another instance has completed the transaction, its local effects are not externalized, so atomicity is preserved.
    A component instance may produce a side effect request for a transaction that fails to commit, but side effects are idempotent since they themselves are locally atomic and monotonic.

    \vskip 4pt
    \noindent {\bf Eventual completion.}
    Each transaction and all of its suboperations must be processed eventually. 
    A component failure shouldn't be visible to the user device
    (unless there is a major outage whose scale we don't address).
    Instead, the entry point persistently retries until an operation completes.
    }
\fi
%  - Stateless design components:\\
%    + {\bf Necessarily reliability:} eNodeB's agent is reliable and the key component to 
%    maintain reliability. The agent also provide serializability at the edge.\\
%    + {\bf Recursive atomic transaction processing:} a transaction on a component 
%    triggers another transaction to another component {\em recursively}. 
%    This technique allows all children transactions are executed atomically in the 
%    order that requests arrive at the system (linearizability).\\
%    + {\bf Distributed timeout:} timeouts must be executed. However, 
%    since components are not reliable, there is no guarantee the timers 
%    are fired on time. This technique allows timer events to 
%    be kept by the {\em necessarily reliable component} and 
%    are treated as transactions.
