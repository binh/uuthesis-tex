\newtheorem{theorem}{Theorem}
\newtheorem{property}{Property}
\newtheorem{assumption}{Assumption}
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}[theorem]{Lemma}

Here we give a sketch of why ECHO is safe even though components are redundant
and non-blocking under failure. Showing that 
\Name{} {\em appears} to process operations atomically, in mobile device's FIFO order (client's FIFO order),
one-at-a-time demonstrates safety. 

First, we 
show that a leaf component (a component that 
does not trigger side effects to other components) operates 
linearizably (in an order consistent with some total order of the client operations). Next, we show that the total order it is consistent with is the client's FIFO order.
Then, using leaf components as the base case, it can be shown that all components appear to process operations atomically in client FIFO order.

%\begin{lemma}[Operation Monotonicity]
%\label{monotonicity}
%Each transition on a {\em component instance} must be
%1) handled in transaction ID order (client FIFO order),
%2) idempotent, and
%3) atomic (when applied it leaves the component instance in a ``before'' state or an ``after'' state).
%\end{lemma}

%\begin{assumption}[Idempotent operation]
%\label{idempotent}
%Each leaf component instance processes requests from other components idempotently; that is, a retry causes 
%the same effect on the component.
%\end{assumption}


%With the assumption of idempotent operation of a leaf component, we prove that a leaf component operates linearizably.

\begin{lemma}[Leaf Instance Linearizability]
\label{leaf-instance-linearizability}
Each leaf component instance appears to process requests atomically in an order consistent with client invocation and response.
\end{lemma}

\noindent
\textbf{Proof.}
Figure~\ref{fig:leaf-component-proof} shows the processing steps of a component instance handling a request.
%an operation takes effect some time between its first invocation (Request $n$) and its acknowledgment (Reply $n$).
%Component instances only process messages transaction ID order, which is consistent with client invocation order.
%the update to the shared State Storage is atomic, 
The compare-and-swap on the shared storage acts as a 
linearization point: before it no (local) effect of the component instance can be perceived, after it all its (local) effects are guaranteed to persist.
%individually, 
Each processed request with ID $n+1$ results in one of four outcomes: 1) {\em aborted}-the component is not in state $n$ or $n+1$, so the request is invalid and ignored; 
%(i.e., at step~\ref{lst:line:check_sequence_number})
2) {\em successful}-the operation completes successfully, the component instance updates the state storage, moves 
the component from state $n$ to $n+1$ at step~\ref{lst:line:commit} and replies; 3) {\em crashed before update}-the operation fails in \circled{1} before updating the state leaving the component in state $n$; or 4) 
{\em crashed after update}-the operation fails in \circled{2} after updating the state 
leaving the component in state $n+1$.
% without a reply.

In case 3, because of the eventual completeness of the entry point, there must 
be a retry arrived at another 
instance that progresses to either case 4 (in-completed) or case 2 (completed).
In case 4, 
%since the component is already in state $n+1$ and the reply is recorded 
%in the State Storage (step~\ref{lst:line:check_reply_exists}), 
%, the effect is the same 
%because the component is idempotent by assumption~\ref{idempotent}. 
%That is, 
when a component instance receives a retry, it simply replies 
with the recorded reply which eventually results 
in case 2. Therefore, a component only executes requests in the specified order, either completely successful or completely failed.
\qed

Because each compare-and-swap is issued from a natural number $n$ to $n + 1$,
only one instance of any component can achieve a successful outcome in the
above proof, so Lemma~\ref{leaf-instance-linearizability} can be strengthened:

\begin{lemma}[Leaf Component Linearizability]
\label{leaf-linearizability}
Each leaf component appears to process requests atomically in an order consistent with client invocation and response.
\end{lemma}

Finally, because the entry point only issues request $n + 1$ after the successful
acknowledgment of request $n$ (that is, operations are synchronous), no
component instance can attempt to apply $n + 1$ to shared storage while the
state in storage is tagged with a request number less than $n$. This
strengthens Lemma~\ref{leaf-linearizability} to:

\begin{lemma}[Atomic FIFO Leaf Components]
  \label{leaf-fifo}
  Every leaf component in \Name{} processes requests atomically in the (FIFO) order client issued them to the entry point.
\end{lemma}


%In fact, in all components the final update to shared storage serves a linearization point for the processing of each message by a whole component.
%It is a single, atomic, FIFO ordered operation that all processed messages must reach before they are acknowledged to the client.
%The final update to the top-level component's shared state serves as the linearization point for each transaction.

Given Lemma~\ref{leaf-fifo} as a base case, components that make nested
calls to other components can be shown to be atomic and FIFO as well using
induction.

\begin{lemma}[Atomic FIFO Components]
  \label{fifo}
  Every component in \Name{} processes requests atomically in the (FIFO) order client issued them to the entry point.
\end{lemma}

\noindent
\textbf{Proof.}
Non-leaf components are identical to leaf components except that they may send
requests and wait for responses just before attempting to update shared
storage. Let $M$ be the {\em height} of a component, which is the number of
nested requests that must succeed before a leaf component is reached. Leaf
components have $M = 1$.

{\em Induction hypothesis:} Assuming a component at height $M$ operates
atomically in client FIFO order, then a component at height $M + 1$ operates
atomically and in client FIFO order.

{\em Base case:} Lemma~\ref{leaf-fifo} proves the case for $M = 1$.

\iffalse
  \begin{figure*}[!ht]
    \begin{subfigure}{.33\textwidth}
      \centering
      \includegraphics[scale=0.4]{figs/echo-leaf-proof.pdf}
      \caption{}
      \label{fig:leaf-component-proof}
    \end{subfigure}%
    \begin{subfigure}{.28\textwidth}
      \centering
      \includegraphics[scale=0.4]{figs/echo-linearizability-proof.pdf}
      \caption{}
      \label{fig:linearizability-proof}
    \end{subfigure}
    \begin{subfigure}{.36\textwidth}
      \centering
      \includegraphics[scale=0.5]{figs/mme-implementation.pdf}
      \caption{}
      \label{fig:agent-layer}
    \end{subfigure}
    \vspace{-5mm}
    \caption{
      (a) \Name's leaf component is monotonic;
      %linearizable;
      (b) \Name is 
      %linearizable;
      monotonic;
      (c) Changes that \Name introduces to conventional EPC architecture (here showing MME as a sample components). 
            }
  \label{fig:proof-and-implementation}
  \vspace{-3mm}
  \end{figure*}
\fi

\iffalse
\begin{figure*}
  \begin{figure}[h]
    \centering
    \includegraphics[scale=0.5]{figs/echo-leaf-proof.png}
    \caption{\Name's leaf component is linearizable.}
    \label{fig:leaf-component-proof}
  \end{figure}

  \begin{figure}[h]
    \centering
    \includegraphics[scale=0.5]{figs/echo-linearizability-proof.png}
    \caption{\Name is linearizable.}
    \label{fig:linearizability-proof}
  \end{figure}

  \begin{figure}[]
    \centering
    \includegraphics[scale=0.5]{figs/mme-implementation.png}
    \caption{Changes that \Name introduces to conventional EPC architecture (here showing MME as a sample components).}
    \vspace{-0.15in}
    \label{fig:agent-layer}
  \end{figure}
\end{figure*}
\fi 
Consider an request arriving at a component at height $M + 1$.
Similar to the leaf component proof above, the request has 4 outcomes: {\em 1-aborted, 2-successful, 3-crashed before update} and 
{\em 4-crashed after update}. The {\em crashed after update} outcome   
eventually results in the {\em successful} case as in the proof in lemma~\ref{leaf-linearizability}. In the {\em crashed before update}  
case, the component instance crashes in \circled{1} and leaves the component $M+1$ 
in state $n$. 
Eventually, there is a retry $n+1$ from the entry point that arrives 
at another component instance and triggers the side effect again. 
If there are multiple retries that trigger multiple side effects to component $M$, 
the effect on component $M$ is still atomic and in client FIFO order
%linearizable 
by the induction hypothesis. 
Therefore, case 4 eventually results in case 2 or 3 (which eventually results in case 2). 
Therefore, component $M+1$ operates requests atomically in client FIFO order.
%linearizable.
\qed
%1) the component instance is not in state $n$ or $n+1$, so the request is ignored; 2) the operation completes successfully, the component instance updates the State Storage (moving from state $n$ to $n+1$) and replies, 3) the operation fails before updating the state (i.e., fails in (1) period and leaves the component in state $n$), or 4) the operation fails after updating the state (i.e., fails in (2) period and leaves the component in state $n+1$).
%In case 4, similarly the component is already in state $n+1$ therefore another 
%component instance simply replies with a recorded reply (idempotent operation) when 
%a retry arrives. This eventually results in case 2.
%In case 3, eventually there is an instance triggers the side effect and progresses to case 2. If there are multiple instances trigger multiple side effects, the effect is still linearizable as assumed. Therefore, component $M+1$ is linearizable.
%\qed

%shorten version
\iffalse

%Finally, since the ordinary, unreplicated protocol precisely processes messages atomically, in client FIFO order, one-at-a-time, this gives the essential safety property:

\begin{property}[\Name's Safety Property]
\Name appears to processes operations atomically, in client FIFO order, one-at-a-time.
Or the set of states observed by \Name clients is equivalent to the unreplicated (EPC) protocol.
\end{property}
\fi
Finally, since the ordinary, unreplicated protocol precisely processes messages
atomically, in mobile device's FIFO order, one-at-a-time, this gives the
essential safety property:

\begin{property}[ECHO Safety Property]
The set of states observed by ECHO clients is equivalent to the unreplicated protocol.
\end{property}
