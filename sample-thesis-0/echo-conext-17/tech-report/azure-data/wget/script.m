data = load('mout.txt');

%time, resource, vm, gap, get_locality(n[0], resource), 0

iTime = 1;
iResource = 2;
iVM = 3;
iGap = 4;
iLocality = 5;   % 0 - same LB, 1 - same DC, 2 - other DC
iType = 6;       % 0 - LB, 1 - VMs





LOC = unique(data(:,iLocality));
TYP = unique(data(:,iType));

ind0 = find(data(:,iLocality) == 0);
ind1 = find(data(:,iLocality) == 1);
ind2 = find(data(:,iLocality) == 2);

fprintf('Total gaps with different locality - 0: %d, 1: %d, 2: %d\n', ...
	length(ind0), length(ind1), length(ind2)); 

figure(2);
clf;

subplot(1,3,1)

ind0 = find(data(:,iLocality) == 0);
ind1 = find(data(:,iLocality) == 1);
ind2 = find(data(:,iLocality) == 2);

X0 = sort(data(ind0, iGap));
X1 = sort(data(ind1, iGap));
X2 = sort(data(ind2, iGap));

Y0 = (0:length(ind0)-1)/(length(ind0)-1);
Y1 = (0:length(ind1)-1)/(length(ind1)-1);
Y2 = (0:length(ind2)-1)/(length(ind2)-1);

semilogx(X0, Y0, X1, Y1, X2, Y2);
ylabel('CDF');
xlabel('Gap duration [s]');
legend('Same LB', 'Same DC', 'Other DC', 'Location', 'SouthEast');
title('HTTP Outage duration distribution across all VMs and LBs');



%figure(2);
%clf;
subplot(1,3,2);

ind0 = find(data(:,iLocality) == 0 & data(:,iType) > 0);
ind1 = find(data(:,iLocality) == 1 & data(:,iType) > 0);
ind2 = find(data(:,iLocality) == 2 & data(:,iType) > 0);

X0 = sort(data(ind0, iGap));
X1 = sort(data(ind1, iGap));
X2 = sort(data(ind2, iGap));

Y0 = (0:length(ind0)-1)/(length(ind0)-1);
Y1 = (0:length(ind1)-1)/(length(ind1)-1);
Y2 = (0:length(ind2)-1)/(length(ind2)-1);

semilogx(X0, Y0, X1, Y1, X2, Y2);
ylabel('CDF');
xlabel('Gap duration [s]');
legend('Same LB', 'Same DC', 'Other DC', 'Location', 'SouthEast');
title('HTTP Outage duration distribution across VMs');



%figure(3);
%clf;
subplot(1,3,3);

ind0 = find(data(:,iLocality) == 0 & data(:,iType) == 0);
ind1 = find(data(:,iLocality) == 1 & data(:,iType) == 0);
ind2 = find(data(:,iLocality) == 2 & data(:,iType) == 0);

X0 = sort(data(ind0, iGap));
X1 = sort(data(ind1, iGap));
X2 = sort(data(ind2, iGap));

Y0 = (0:length(ind0)-1)/(length(ind0)-1);
Y1 = (0:length(ind1)-1)/(length(ind1)-1);
Y2 = (0:length(ind2)-1)/(length(ind2)-1);

semilogx(X0, Y0, X1, Y1, X2, Y2);
ylabel('CDF');
xlabel('Gap duration [s]');
legend('Same LB', 'Same DC', 'Other DC', 'Location', 'SouthEast');
title('HTTP Outage duration distribution across LBs');




