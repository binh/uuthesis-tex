import os
import sys

from azure.storage.blob import BlockBlobService
from azure.storage.blob import ContentSettings

import json
from pprint import pprint


blob_name = 'openepcstorage'
blob_key = 'JBveOCI4mJIAGUSu6SR88WkbgBeGQUyNxN5fk/jGt2PXMC8cZ2fzWlifrBpDvkF9L0ywdwfM0VK22m5WONidGw=='


# VM logs
blob_container = 'insights-metrics-pt1m'
file_name = 'output_vm.json'

# LB log
#blob_container = 'insights-logs-loadbalancerprobehealthstatus'
#file_name = 'output_lb.json'

outfile = open(file_name, 'w')


bbs = BlockBlobService(account_name=blob_name, account_key=blob_key)
container = bbs.create_container(blob_container)

blobs = bbs.list_blobs(blob_container, include='metadata')


print bbs
print container
print blobs


outfile.write("[\n")

first = True

for blob in blobs:
	print " *** ", blob.name
	bbs.get_blob_to_path(blob_container, blob.name, 'localfile.json')

	with open('localfile.json') as data_file:    
		dict = json.load(data_file)
	
		for v in dict['records']:
			print json.dumps(v)
			if not first:
				outfile.write("\n, " + json.dumps(v))
			else:
				outfile.write(json.dumps(v))
				first = False
				

outfile.write("]\n")

outfile.close()



# VM report JSON Template:
# {
# 	"records": 
# 	[
#		
# 		{
# 			 "count": 2,
# 			 "total": 1.42,
# 			 "minimum": 0.58,
# 			 "maximum": 0.84,
# 			 "resourceId": "/SUBSCRIPTIONS/8BB381B1-DE12-4FB7-A019-AEEEB2071090/RESOURCEGROUPS/OPENEPC0/PROVIDERS/MICROSOFT.COMPUTE/VIRTUALMACHINES/MYVM1",
# 			 "time": "2016-10-14T02:00:00.0000000Z",
# 			 "metricName": "Percentage CPU",
# 			 "timeGrain": "PT1M"
# 		}
# 	]
# }
