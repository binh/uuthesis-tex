% time, port, totalDipCount, dipDownCount, healthPercentage


iTime = 1;
iPort = 2;
iTotalDip = 3;
iDip = 4;
iHealth = 5;

data = load('mout_lb.txt');


data(:,iTime) = data(:,iTime) - min(data(:,iTime));
data(:,iTime) = data(:,iTime) / 3600 / 24;

ports = unique(data(:,iPort));

figure(3);
clf(3);
subplot(3,1,1)
hold on
title('Dip down');
for ip = 1:length(ports)
  port = ports(ip);
  ind = find(data(:,iPort) == port);
  d = sortrows(data(ind,:));
  plot(d(:,iTime), d(:, iDip), '.');
end

subplot(3,1,2)
hold on
title('Total dip');
for ip = 1:length(ports)
  port = ports(ip);
  ind = find(data(:,iPort) == port);
  d = sortrows(data(ind,:));
  plot(d(:,iTime), d(:, iTotalDip), '.');
end

subplot(3,1,3)
hold on
title('Health percentage');
for ip = 1:length(ports)
  port = ports(ip);
  ind = find(data(:,iPort) == port);
  d = sortrows(data(ind,:));
  plot(d(:,iTime), d(:, iHealth), '.');
end

