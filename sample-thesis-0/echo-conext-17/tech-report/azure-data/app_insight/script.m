 %%% Resources - mout0
% bdeploy1openepcstorage 0
 %%% Locations
% US : TX-San Antonio 0
% US : IL-Chicago 1
% FR : Paris 2
% IE : Dublin 3
% US : VA-Ashburn 4
% BR : Sao Paulo 5
% US : FL-Miami 6
% RU : Moscow 7
% US : CA-San Jose 8
% HK : Hong Kong 9
 %%% Test cases
% bdeploy1testlb 0
% bdeploy1testvm1 1
% bdeploy1testvm2 2
% bdeploy1testvm3 3
% [variables('test_status_name') 4 (BBC)


%%% Resources - mout
% deploy2openepcstorage 0
% deploy1weuopenepcstorage 1
% deploy2weuopenepcstorage 2
% deploy1wusopenepcstorage 3
% deploy2wusopenepcstorage 4
 %%% Locations
% RU : Moscow 0
% HK : Hong Kong 1
% BR : Sao Paulo 2
% US : IL-Chicago 3
% US : CA-San Jose 4
% US : TX-San Antonio 5
% IE : Dublin 6
% US : VA-Ashburn 7
% US : FL-Miami 8
% FR : Paris 9
 %%% Test cases
% deploy2testvm1 0 - 4
% deploy2testvm3 1 - 5
% deploy2testlb 2 - 6
% [variables('test_status_name') 3
% deploy2testvm2 4 - 8
%
% deploy1weutestvm2 5 - 9
% deploy1weutestvm3 6 - 10
% deploy1weutestvm1 7 - 11
% deploy1weutestlb 8 - 12
%
% deploy2weutestvm1 9 - 13
% deploy2weutestlb 10 - 14
% deploy2weutestvm2 11 - 15
% deploy2weutestvm3 12 - 16
%
% deploy1wustestvm3 13 - 17
% deploy1wustestvm2 14 - 18
% deploy1wustestlb 15 - 19
% deploy1wustestvm1 16 - 20
%
% deploy2wustestvm3 17 - 21
% deploy2wustestvm2 18 - 22
% deploy2wustestvm1 19 - 23
% deploy2wustestlb 20 - 24



str_loc = {'US : TX-San Antonio 0', ...
	   'US : IL-Chicago 1', ...
	   'FR : Paris 2', ...
	   'IE : Dublin 3', ...
	   'US : VA-Ashburn 4', ...
	   'BR : Sao Paulo 5', ...
	   'US : FL-Miami 6', ...
	   'RU : Moscow 7', ...
	   'US : CA-San Jose 8', ...
	   'HK : Hong Kong 9'};


str_test = {'bdeploy1testlb 0', ...
	    'bdeploy1testvm1 1', ...
	    'bdeploy1testvm2 2', ...
	    'bdeploy1testvm3 3', ...
	    'bdeploy1testbbc 4'};

Tests = (1:length(str_test))-1;
Locations = (1:length(str_loc))-1;


% time, location, test, result

iTime = 1;
iResource = 2;
iLocation = 3;
iTest = 4;
iResult = 5;


if 0
  % Preprocess:
  data0 = load('mout_0.txt');
  data = load('mout.txt');
  save('datap.mat', 'data0', 'data');
end

if 0
  % More preprocessing:
  load('datap.mat');
  start = min([data(1,iTime), data0(1,iTime)]);
  % Remove BBC failures
  ind0 = find(data0(:,iTest) == 4);
  data0(ind0, iTest) = -1;
  ind = find(data(:,iTest) == 3);
  data(ind, iTest) = -1;
  % Increase resource counter for the second group
  data(:,iResource) = data(:,iResource)+1;
  
  % Increase test case counter for the second group
  ind = find(data(:,iTest) ~= -1);
  data(ind,iTest) = data(ind,iTest)+4;
  
  
  %%% Locations
  % RU : Moscow 0 7
  % HK : Hong Kong 1 9
  % BR : Sao Paulo 2 5
  % US : IL-Chicago 3 1
  % US : CA-San Jose 4 8
  % US : TX-San Antonio 5 0
  % IE : Dublin 6 3
  % US : VA-Ashburn 7 4
  % US : FL-Miami 8 6
  % FR : Paris 9 2
  
  % Harmonize location ID
  map = [0 7; 1 9; 2 5; 3 1; 4 8; 5 0; 6 3; 7 4; 8 6; 9 2];
  map(:,1) = map(:,1) + 10;
  data(:, iLocation) = data(:, iLocation) + 10;
  for j=1:size(map,1)
      ind = find(data(:,iLocation) == map(j,1));
      data(ind, iLocation) = map(j,2);
  end
  
  % Merge two measurements
  data = [data0; data];
  % Skip first 4 days as we had setup troubles
  ind = find((data(:,iTime)-start)/3600/24 > 4);
  data = data(ind, :);

  data = sortrows(data);
  save('data.mat', 'data');
end

load('data.mat');

start = min(data(1,iTime));

TESTS = unique(data(:,iTest));
TESTS = TESTS(find(TESTS >= 0));
LOC = unique(data(:,iLocation));

if 0
  gaps = [];
  fail = [];
  for t = TESTS'
    for l = LOC'
      ind = find(data(:,iTest) == t & data(:,iLocation) == l);
      indf = find(data(:,iTest) == t & data(:,iLocation) == l & data(:,iResult) == 0);
      fail = [fail; [t, l, length(indf), length(ind), length(indf) / length(ind)]];
      
      r = data(ind(1), iResource);
      
      D = diff(data(ind, iResult));
      ii = find(D ~= 0);
      if mod(length(ii), 2) ~= 0
	fprintf('Err: %d %d %d\n', t, l, length(ii));
      end
      DD = diff(data(ii, iTime));
      jj = 1;
      while jj <= length(ii)/2
	sti = ind(ii(jj)+1);
	eni = ind(ii(jj+1)+1);
	st = data(sti, iTime);
	en = data(eni, iTime);
	str = data(sti, iResult);
	enr = data(eni, iResult);
	if (str ~= 0 || enr ~= 1)
	  fprintf('Err2: %d %d %d %d\n', t, l, str, enr);
	end
	jj = jj + 2;
        % test, location, start, end, startindex, endindex
	gaps = [gaps; [t, l, st, en, sti, eni, r]];
      end
    end
  end
  
  save('gaps.mat', 'gaps', 'fail');
end

load('gaps.mat');

%gaps(:,3:4) = gaps(:,3:4) - start;




%% Find outages when a VM sees entire cluster/DC in outage


% location, test, start, end
d = [gaps(:,2), gaps(:,3), gaps(:,4), gaps(:,1), gaps(:,7)];
M = unique(d(:,1));
Ov = {};
Or = {};
io = 1;
for m = M'
    ind = find(d(:,1) == m);
    dd = sortrows(d(ind, 2:end));
    j = 1;
    while j < size(dd,1)
        k = j+1;
        V = [m, dd(j,[1,2,3])];
        R = [m, dd(j,[1,2,4])];
        %fprintf('J: %d %d %d %d %d %d\n', j, dd(j,1), dd(j,2), dd(j,2) - dd(j,1), dd(j,3), dd(j,4))
        while k <= size(dd, 1) && ((dd(j,1) < dd(k,1) && dd(j,2) > dd(k,1)) || ...
              (dd(j,1) < dd(k,2) && dd(j,2) > dd(k,2))) 
            %fprintf('K: %d %d %d %d %d %d\n', k, dd(k,1), dd(k,2), dd(j,2) - dd(j,1), dd(k,3), dd(k,4))
            V = [V, dd(k,3)];
            R = [R, dd(k,4)];
            k = k+1;
        end
        if (length(V) > 4)
            Ov{io} = V;
            Or{io} = R;
            io = io+1;
        end
        j = k;
    end
end


outage = [0, 0, 0];

for io = 1:length(Or)
    o = Or{io};
    r = o(4:end);
    d = o(3)-o(2);
    U = unique(r);
    D = [];
    for u = U
        num = length(find(r == u));
        if num >= 2
          D = [D, u];
        end
    end
    [d, r]
    outage(1) = outage(1) + d*length(r);
    if (length(D) >= 2)
        outage(3) = outage(3) + d;
        outage(2) = outage(2) + d;
    elseif (length(D) == 1)
        outage(2) = outage(2) + d;
    end
end


total = max(data(:,iTime)) - min(data(:,iTime));

fprintf('Total availability over VM: %.9f\n', 1 - outage(1) / (6*3*total));
fprintf('Total availability over DC cluster: %.9f\n', 1 - outage(2) / (6*total));
fprintf('Total availability over DC: %.9f\n', 1 - outage(3) / (3*total));


%fprintf('Total VM availability: %.9f\n', 1-sum(fail(:,3)) / sum(fail(:,4)));


aaa


%ind = find(data(:,iResult) == 0 & data(:,iTest) ~= -1);
%data_err = data(ind, :);
%figure; plot((data_err(:,iTime)-start) / 3600/24);


figure(1);
clf

subplot(1,2,1);

X = sort(gaps(:,4)-gaps(:,3));
Y = (0:length(gaps)-1)/(length(gaps)-1);

semilogx(X, Y)
ylabel('CDF');
xlabel('Gap duration [s]');
title('Outage duration distribution across probes and DCs');


%figure(2);
%clf;

subplot(1,2,2);

X = sort(fail(:,5)');
Y = (0:length(fail)-1)/(length(fail)-1);

plot(X, Y)
ylabel('CDF');
xlabel('Fraction of failed tests');
title('Fraction of failed tests across probe,DC pairs');





