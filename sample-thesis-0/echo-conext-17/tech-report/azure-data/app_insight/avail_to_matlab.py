import os
import sys

import json
from pprint import pprint

import dateutil.parser


with open('output.json') as data_file:    
    data = json.load(data_file)


locations = []
tests = []
for v in data:
	if not (v["runLocation"] in locations):
		locations.append(v["runLocation"])
	if not (v["testName"] in tests):
		tests.append(v["testName"])
	indl = locations.index(v["runLocation"])
	indt = tests.index(v["testName"])
	t = dateutil.parser.parse(v["testTimestamp"])
	if (v["result"] == "Pass"):
		r = 1
	else:
		r = 0
		
	print t.strftime('%s'), indl, indt, r
		

print " %%% Locations "
for l in locations:
	print "% ", l, locations.index(l)

print " %%% Test cases "
for t in tests:
	print "% ", t, tests.index(t)
	
