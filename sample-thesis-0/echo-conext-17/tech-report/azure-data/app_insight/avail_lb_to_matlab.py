import os
import sys

import json
from pprint import pprint

import dateutil.parser


with open('output_lb.json') as data_file:    
    data = json.load(data_file)


for v in data:
	t = dateutil.parser.parse(v["time"])
	p = v["properties"]["port"]
	tdc = v["properties"]["totalDipCount"]
	dc = v["properties"]["dipDownCount"]
	he = v["properties"]["healthPercentage"]
	
	print t.strftime('%s'), p, tdc, dc, he
		


	



# {
# 	"time": "2016-10-06T06:14:24.7039150Z",
# 	"systemId": "e27408d2-b81c-476c-908e-e6fa172d943c",
# 	"category": "LoadBalancerProbeHealthStatus",
# 	"resourceId": "/SUBSCRIPTIONS/8BB381B1-DE12-4FB7-A019-AEEEB2071090/RESOURCEGROUPS/OPENEPC0/PROVIDERS/MICROSOFT.NETWORK/LOADBALANCERS/MYLB",
# 	"operationName": "LoadBalancerProbeHealthStatus",
# 	"properties": {"publicIpAddress":"40.69.42.246","port":80,"totalDipCount":2,"dipDownCount":0,"healthPercentage":100.000000}
# }
