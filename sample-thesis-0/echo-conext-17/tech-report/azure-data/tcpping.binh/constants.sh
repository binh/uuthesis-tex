#!/bin/bash

PORTS="80 2000 2001 2002"
HTTP_IP="127.0.0.1 52.178.207.8 13.79.168.159 40.118.165.178 104.42.234.146 13.80.14.121 13.95.147.203 bbc.co.uk"
HTTP_DNS="localhost bdeploy1mydnsname-epc0.northeurope.cloudapp.azure.com deploy2mydnsname-epc0.northeurope.cloudapp.azure.com deploy1wusmydnsname-epc0.westus.cloudapp.azure.com deploy2wusmydnsname-epc0.westus.cloudapp.azure.com deploy1weumydnsname-epc0.westeurope.cloudapp.azure.com deploy2weumydnsname-epc0.westeurope.cloudapp.azure.com"
#result="/proj/PhantomNet/binh/msr-scripts/experiments/availability/http/RESULTS" #phantomnet
result="$HOME/http/RESULTS"  #Azure VMs
#result="/Users/binh/Desktop/Papers/msr/msr-scripts/experiments/available/http/process_result/vm/RESULTS" #test
http_result="$result/http.result"


fetch_html="./fetch_html.sh"
traceroute="./traceroute.sh"
test="./http.sh"
DATE=$(date +%s)



GAP=10 #VM time stamp gap in second. Ignore gaps that are smaller.
extracted_result="$HOME/http/RESULTS_EXTRACTED/"
get_gaps="get_gaps.sh"
get_network_fails="get_network_fails.sh"
group_fails="group_consecutive_fails.py"
group_fails_seperate="group_consecutive_fails_seperate_event.py"
group_fails_seperate_tcpping="group_consecutive_fails_seperate_event_tcpping_file.py"
