import sys
import collections
from subprocess import call

if len(sys.argv) < 2:
 	print "Usage: <input file name>"
	sys.exit(1)

input_file=open(sys.argv[1],'r').readlines()


#Get all timestamps and its destination
dst_ts = collections.defaultdict(list)
dst_dst = collections.defaultdict(list)
for l in input_file:
	try:
		ts = int(l.split("|")[0])
		dst = str(l.split("|")[1]).strip()
		#ts[dst].append(t)
		#ts_bin.append(ts)
		#dst_bin.append(dst)
		dst_ts[dst].append(ts)
		dst_dst[dst].append(dst)
	except:
		continue

for dst in dst_dst:
			  	ts_bin = dst_ts[dst]
				dst_bin = dst_dst[dst]

				#print len(ts_bin)
				gap_bin = collections.defaultdict(list)
				gap_bin_dst = collections.defaultdict(list)


				index = 0
				current_index = 0
				ts = ts_bin[0]
				last_ts = ts_bin[0]

				while current_index < len(ts_bin)-1:
					current_ts = ts_bin[current_index]	
					current_dst = dst_bin[current_index]
					ts_dst = "%s+%s" % (current_ts, current_dst)
					#print ts_dst
					gap_bin[ts_dst].append(current_ts)
					#if current_dst not in gap_bin_dst[current_ts]:
					#	gap_bin_dst[current_ts].append(current_dst)

					last_ts = current_ts
					for i in range (current_index+1, len(ts_bin)):
						#print "i=%s"%i
						forward_ts = ts_bin[i]
						forward_dst = dst_bin[i]
						if forward_ts - last_ts <= 10: #if the 2 fail events are 10s away, group them. 10s seems to be the 'right' value of 2 consecutive tcpping fails
							#if forward_dst not in gap_bin_dst[current_ts]:
							#	gap_bin_dst[current_ts].append(forward_dst)
							if forward_ts - last_ts != 0 and forward_ts > last_ts: #If consecutive timestamp
								gap_bin[ts_dst].append(forward_ts)
							last_ts = forward_ts
						else:	#if not, move the last_index pointer forward to current index
							current_index = i
							break	
					current_index = i
					if current_index == len(ts_bin)-1 and forward_ts not in gap_bin[ts_dst]:
						gap_bin["%s+%s"%(forward_ts,forward_dst)].append(forward_ts)
						gap_bin["%s+%s"%(forward_ts,forward_dst)].append(forward_ts+1)
						


					#print current_index

				gap_len = []
				cnt = 0
				for ts_dst in gap_bin:
					cnt+=1
					min_ts = float("inf")
					max_ts = -float("inf")
					for t in gap_bin[ts_dst]:
						if int(t) <= min_ts:
							min_ts = int(t)
						if int(t) >= max_ts:
							max_ts = int(t)
					ts = int (ts_dst.split("+")[0])
					dst = ts_dst.split("+")[1]
					print "%s|%s|%s"%(ts, str(max_ts-min_ts+1), dst)
