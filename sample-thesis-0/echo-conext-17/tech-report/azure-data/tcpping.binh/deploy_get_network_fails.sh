#!/bin/bash

#install probes on all VMs on multiple deployments
#source ../../constants.sh
source constants.sh

for ip in $HTTP_IP
do
	if [ "$ip" == "127.0.0.1" -o "$ip" == "bbc.co.uk" ]; then
		continue
	fi
	scp -P 1000  $get_network_fails $group_fails $group_fails_seperate $group_fails_seperate_tcpping constants.sh get_tcpping_fail_summary.sh epc@$ip:/home/epc/http;
	ssh -p 1000  -t -t epc@$ip "cd /home/epc/http/; sudo ./$get_network_fails; sudo ./get_tcpping_fail_summary.sh ";
	
	scp -P 1001  $get_network_fails  $group_fails $group_fails_seperate $group_fails_seperate_tcpping constants.sh get_tcpping_fail_summary.sh epc@$ip:/home/epc/http;
	ssh -p 1001  -t -t epc@$ip "cd /home/epc/http/; sudo ./$get_network_fails; sudo ./get_tcpping_fail_summary.sh ";

	scp -P 1002  $get_network_fails  $group_fails $group_fails_seperate $group_fails_seperate_tcpping constants.sh get_tcpping_fail_summary.sh epc@$ip:/home/epc/http;
	ssh -p 1002  -t -t epc@$ip "cd /home/epc/http/; sudo ./$get_network_fails; sudo ./get_tcpping_fail_summary.sh";
done

#scp -P 1000  ports.conf epc@$ip:/home/epc/ports.conf;
