#!/bin/bash
#source ../../constants.sh

source  constants.sh	#deploy

mkdir RESULTS
echo "$(hostname)"
echo "-----------"

#wget
#start=$(head -1 $http_result | cut -d"|" -f1)
#end=$(tail -1 $http_result | cut -d"|" -f1)
#echo "#"$start"|"$end > /tmp/inter_dc.wget_fail
#grep FAIL $http_result | sort > /tmp/inter_dc.wget_fail
#python group_consecutive_fails.py /tmp/inter_dc.wget_fail > /tmp/inter_dc.wget_gap

#tcpping
#start=$(head -1 ./RESULTS/$(hostname).time | cut -d"|" -f1)
#end=$(tail -1 ./RESULTS/$(hostname).time | cut -d"|" -f1)
#echo "#"$start"|"$end > /tmp/inter_dc.tcpping_fail
#grep "no response" $http_result | sort > /tmp/inter_dc.tcpping_fail
#python group_consecutive_fails_seperate_event.py /tmp/inter_dc.tcpping_fail > /tmp/inter_dc.tcpping_gap


#echo "wget"
#cat /tmp/inter_dc.wget_gap | sort -n -t \| -k2 | tail -5
#echo "tcpping"
#cat /tmp/inter_dc.tcpping_gap | sort -n -t \| -k2 | tail -5

echo "Extracting gaps in tcpping probe"
rm /tmp/tcpping.gap.10.events
rm /tmp/all.tcpping.gap
for i in $HTTP_IP
do
	for p in $PORTS
	do
		if [ "$i" == "127.0.0.1" ]; then
			continue
		fi
		if [ "$i" == "bbc.co.uk" -a "$p" != "80" ]; then
			continue
		fi
		python group_consecutive_fails_seperate_event_tcpping_file.py $result/$i-$p.tcpping |  sort -n -t \| -k2 > /tmp/$i-$p.tcpping.gap 
	
		cat /tmp/$i-$p.tcpping.gap >> /tmp/all.tcpping.gap
	done
done

echo "10 most serious events:" >> /tmp/tcpping.gap.10.events
sort -n -t \| -k2 /tmp/all.tcpping.gap | tail -10 >> /tmp/tcpping.gap.10.events
echo "10 most serious events:" 
sort -n -t \| -k2 /tmp/all.tcpping.gap | tail -10

#echo "VM downtime results are in RESULTS/vm.gap"
