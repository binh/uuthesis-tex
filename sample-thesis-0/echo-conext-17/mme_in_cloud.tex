\subsection{What does public cloud provide?}
\label{azure_study}



Typical telco appliances like the ones described in the previous section provide
availability of 99.999\%~\cite{nokia.9471.wmm} (``five 9s'' availability).
The five 9s availability corresponds to an overall outage of 1 minute in 2 months.
Instead, cloud offerings today, such as AWS and Azure, advertise VM availabilities of ``four 9s'', or
outages of 1 minute every week -- an order of magnitude larger total outage compared to reliable hardware platforms.

Besides the overall availability in the number of 9s,
the mobile network reliability requirements outlined in Section~\ref{sec:control-plane-requirements} highlight that the duration of
an outage can be critical. For example,
the system may be able to recover from many short 1-second outages using
transport or other mechanisms,
but a few outages lasting minutes can be catastrophic (example~\ref{fig:mme-outage}). 
It is thus crucial to understand the availability properties (total outage instances and their duration) of public clouds in practice, beyond advertised SLAs.

To this end, we perform a 3-month-long measurement study in a major public cloud provider.
%by deploying VMs in several regions of Microsoft Azure and across multiple availability 
%zones. 
We expect our findings to be indicative of other providers as well. 
We monitor the VM uptimes as well as the reachability of VMs at multiple levels: 
data center (DC) cluster, single DC, and across DCs. 
A DC cluster consists of three VMs in different availability zones behind a load-balancer within the same regional data center.
%We deploy three VMs in different availability zones behind a load-balancer within the same regional data centre (DC). We
%define this as a DC \emph{cluster}, 
There are two clusters per DC. In total, we use 3 DCs, two in Europe and one in the U.S. and perform
TCP pings every 1 second from each VM to all other VMs.
Further, we use Azure's Application Insights service~\cite{appinsight} to monitor the reachability of our VMs from the public Internet.
The service initiates web request to all VMs every 10 minutes from 10 locations across 4 continents. The cloud is available if {\em at least} one VM in a cluster is available.

%\begin{itemize}
%\item Each VM performs TCP pings to all other VMs every second.
%\item Each VM performs wget requests all other VMs every 10 seconds.
%\item We use Azure's Application Insights service~\cite{appinsight} to monitor the reachability of our VMs from the public Internet. The service initiates web request to all VMs every 10 minutes from 10 locations across 4 continents.
%\end{itemize}
%\vskip 3pt
$\bullet$ \noindent{\bf Results.}
Our results are summarized in Table~\ref{tab:azure_study}.
Each row in the table shows the observed availability constrained on an outage duration
(e.g., in row $> 1$ min we only account for outages that are longer than 1 min; at least some of these cannot be
handled by MME retransmissions, as illustrated in example~\ref{fig:mme-outage}).
%We note that observations from wget probes are very similar to the TCP ping ones, so we omit them. 
We observe that the advertised SLAs of four 9s are generally met by the cloud. 
Most of the outages are very short, and can possibly be attributed to network congestion, or
other instantaneous problems. 
We observed intra-cloud outages of more than 1 second, 2,400 times during our study.
The Cumulative Distribution Function (CDF) of the durations of such outages 
is depicted in Figure~\ref{fig:tcpping-outage-cdf}.
In all, there are 7 outages that last more than 1 minute and they can all be attributed to VM failures. 
However, VMs in the same DC cluster do not tend to fail at the same time.
%Hence, while individual VMs do not meet the availability requirements of today's telco-grade hardware,
%\emph{a DC cluster appears to potentially offer availability of five 9s}, 
%and most of the internal DC failures can be mitigated by replication within a DC cluster.

\begin{table}[t]
  \small
  \caption{Inter-DC availability in a major cloud provider}
  \centering
  \begin{tabular}{l||c|c|c|c||c|c|c|c}
    & \multicolumn{4}{c|}{Cloud} & \multicolumn{4}{|c}{World-wide} \\
    \hline
    Outage & VM & DC Clus. & DC & DCs & VM & DC Clus. & DC & DCs \\
    \hline
    All & 99.9947\%  & 99.9998\% & 99.9999\% & 100\% & 99.988\%  & 99.991\% & 99.9921\% & 100\%\\
    $>$ 10 sec & 99.9947\%  & 99.9998\% & 99.9999\% & 100\% & 99.988\%  & 99.991\% & 99.9921\% & 100\%\\
    $>$ 1 min & 99.9948\%  & 99.9998\% & 99.9999\% & 100\% & 99.988\%  & 99.991\% & 99.9921\% & 100\%\\ 
    \hline
  \end{tabular}
  \label{tab:azure_study}
\end{table}


\begin{figure}[b]
  \centering
  \includegraphics[scale=0.4]{echo-conext-17/figs/results/tcpping_outage_cdf.pdf}
  \caption{Outage duration distribution across all pairs of VMs}
  \label{fig:tcpping-outage-cdf}
\end{figure}  

The picture is significantly different as observed from hosts in the Internet (``World-wide'' in Table~\ref{tab:azure_study}).
Availability is roughly an order of magnitude less compared to intra-DC measurements,
implying that most ``outages'' are due to public Internet connectivity problems reaching the cloud.
The CDF of the durations of the outages longer than 10 minutes (measurement interval) is depicted in Figure~\ref{fig:tcpping-outage-cdf},
and we can see that more than 20\% of them last 20 minutes or more. 

%\vskip 3pt
$\bullet$ \noindent{\bf Implications:}
%This implies that it is not sufficient to place a service within a single DC cluster to achieve five 9s reachability from the public Internet,
%unless a dedicated connectivity service to the cloud~\cite{awsdirect, expressroute} is deployed which can incur extra cost. 
%Yet, multiple points of presence across regional data centres appear to safeguard against such problems.
In summary, 
%taking into account the limited duration of our study,
we observe that our key requirements (high availability and state persistency) can be achieved with five 9s only if the service is replicated across
multiple VMs across availability zones in a single DC; additionally, coping with public Internet reachability problems requires
service presence across multiple regional data centers unless a dedicated connectivity service to the cloud~\cite{awsdirect,expressroute} is deployed which can incur extra cost.
%Section~\ref{sec:discussion} discusses the form such a presence can take in the context
%of a cellular core. 

We also note that other studies, such as~\cite{cloud.outage} that gives
account of public cloud reliability over a 7-year period,
point out that a median reliability across all public clouds
(32 public clouds studied) is below 99.9\%
and that the median duration of an outage varies between
1.5 and 24 hours, depending on the type of failure.
%Even if some of these failures have happened recently on Azure cloud, we
%might have missed them due to a comparatively shorter duration of our study.
%But 
This reinforces the need for software replication and proactively dealing with faults. 


%% \begin{itemize}
%% \item Advertized SLAs of four 9s are generally met by the cloud
%% \item Individual VM outages do occur and outages are general clustered. We observed outages for more than 1 sec XXX times during our study.
%% \item Intra-DC reliability is generally sufficient - we have not observed any events where all VMs across the three availability zones of one of our DC clusters
%% fail simultaneously.
%% \item Most ``outages'' are due to public Internet connectivity problems to the cloud (Table~\ref{tab:azure_study_appsinsight}). 
%% \end{itemize}
 




%% \begin{table}[t]
%%   \centering
%%   \caption{Availability and outages as seen from the public Internet}
%%   \label{tab:azure_study_appinsight}
%%   \begin{tabular}{l||c|c|c|c}
%%      & VM & DC Cluster & DC & Across DCs\\
%%     \hline
%%     Availability & 99.95\%  & 100\% & 100\% & 100\% \\
%%    Outages $>$ 1 sec &   & & & \\
%%    Outages $>$ 10 sec &   & & & \\
%%    Outages $>$ 1 min &   & & & \\
%%   \end{tabular}
%% \end{table}








%% {\bf BR: OLD}

%%   In contrast to telecome-grade reliable hardware 
%%   mentioned above, public cloud VMs are subjected to 
%%   events that affect reliability of 4G/LTE network running 
%%   on it. For example, a VM crashes could cause all 
%%   UE context to be lost and service outage (see section~\ref{sec:high-availble-mme}). 
%%   An VM upgrade or migration could also cause service outage during 
%%   the events.

%%   To achieve a reliable 4G/LTE on public clouds given 
%%   the unreliable infrastructure, we need mechanisms 
%%   to enable the following properties of the 4G/LTE control plane 
%%   in public clouds:

%% \begin{trivlist}
%%   \item $\bullet$ \textit{High availability:} This property ensures the 4G/LTE network on public cloud 
%%     is available for most of the time if the eNodeB is available.
%%     This means if the eNodeB is active and able to reach a MME, it should 
%%     have service from that MME.
%%     %This means MME should \emph{always} be 
%%     %responsive to UE requests under circumstances such as node crashes or network partitions. 
%%     %MME should provide \textit{at-least-once} semantic for reply to UE.
%%   \item $\bullet$ \textit{No lost of UE context (or session):} The UE context or session, that is generated 
%%     when the UE first attaches to the network, should never be lost.
%%     This requirement should be maintained even when MMEs crash. This prevents the costly service 
%%     outage due to mismatch between UE and MME's states ((section~\ref{sec:state-consistent-mme}.)
%%   \item $\bullet$ \textit{States consistency between MME and UE and other control plane components:} 
%%     During operation the UE's state maintained on the MME should 
%%     be consistent with the actual state the UE observed (via replies from MME) 
%%     and the state on other control plane components that is preserved for the UE. 
%%     This requires the MME to process and reply requests from a particular UE  
%%     in a first-in-first-out (FIFO) order. This is to also prevent the service 
%%     outage due to mismatch between UE and MME's states (section~\ref{sec:fifo-execution-mme}.)
%%   \item $\bullet$ \textit{Scalable and elastic MME:} Unlike huge specialized hardware boxes our MMEs run on 
%%     VMs with limited resources. This requires a way to horizontallly scale up or down the resources of MMEs 
%%     based on workload. 
%% \end{trivlist}  


%%     \xxx{
%%       - No longer having highly reliable hardware.\\
%%       - In contrast, VMs come and go.\\
%%       - Refer to outage papers.\\
%%       - Refer to VM migration, unplanned maintenant.
%%     }

