\section{Evaluation}
\label{sec:results}


%In this section 
We evaluate \echo in the Azure public cloud across several dimensions.  
We examine the correctness of our implementation, the latency introduced
across various components of the architecture, the observed throughput 
and simulate potential failure scenarios. 
Our main findings can be summarized as follows:

\begin{trivlist}
\item $\bullet$ We demonstrate that our cloud-based implementation correctly services 6,720
requests over one week without any failures in the \echo{} system.
\item $\bullet$ \echo introduces reasonable overheads as a trade-off for a public-cloud reliable deployment. When replication within a single data center is used, 
  the response latency is increased by less than 10\% and there is no visible drop in throughput. Even in more extreme deployments, we show that 
 total latency is well below standard 3GPP timeouts and would not be noticeable by users. The result shows user-perceived latency is similar in \echo{} and T-mobile.
\item $\bullet$ By emulating typical data center failures, we show that \echo gracefully handles all such cases without noticeable user experience impact.
\end{trivlist}

$\bullet$ \noindent {\bf Evaluation setup:}
Our base deployment is given in Figure~\ref{fig:evaluation}. 
It  consists of radio equipment (a UE - Nexus 5, LTE eNodeB - IP.Access small-cell) in PhantomNet~\cite{banerjee2015phantomnet}
    and an EPC core (MME pool with 2 MMEs, a ZooKeeper ensemble with 3 nodes, and other EPC components - SGW, PGW, HSS) in Azure. Each node is 
    a Standard\_DS3 VM with 4 cores.
We also use a local OpenEPC deployment in PhantomNet to compare \echo's performance.


$\bullet$ \noindent {\bf Reliability options.} 
We consider two availability options as depicted in Figure~\ref{fig:evaluation}: 
single data center -- 
all ZooKeeper (ZK) nodes are collocated in the same data center, and 
multiple data centers -- ZK nodes are located in 
multiple DCs. 
 The network latency between the eNodeB and Azure is around 22~ms round-trip. The 3 
    Azure DCs are 20~ms round-trip away from each other.
A single DC deployment provides less reliability but also lower latency than a multi-DC deployment.
We evaluate both of them as both can be relevant for different application scenarios. 

\begin{figure}[]
  \centering
  \includegraphics[width=0.9\columnwidth]{echo-conext-17/figs/evaluation-topology.pdf}
  \vspace{-3mm}
  \caption{\echo{} evaluation topology set up
    %: 
    %consists of radio equipment 
    %(an UE - Nexus 5, LTE eNodeB - IP.Access small-cell) in PhantomNet 
    %and an EPC core (MME pool with 2 MMEs, a Zookeeper ensemble with 3 nodes, and other EPC components - SGW, PGW, HSS) in Azure. 
    %The ZK nodes are deployed in two modes: single data center (1-DC) where the ZK ensemble machines 
    %are in a single DC, multiple data centers (3-DCs) where the ZK ensemble machines 
    %each resides in a separate DC. 
    %The network latency between the eNodeB and Azure is around 22ms round-trip. The 3 
    %Azure DCs are 20ms round-trip away of each other.
  }
  \label{fig:evaluation}
  %\vspace{-6mm}
\end{figure}


The reliability also depends on ZooKeeper operational parameters. 
We evaluated three ZK logging configurations: {\em synchronous disk} (Disk), {\em asynchronous disk logging} (Disk-nFC, no force sync) and {\em logging to ramdisk} (Ramdisk).
Synchronous disk logging is the most robust and quickest to recover, but introduces most latency.
Ramdisk and Disk-nFC (log to disc but don't wait before acknowledging) are two trade-offs that reduce latency
but also slightly reduce the ability and speed of recovery. 
Table~\ref{table:cloud-deployment-options} shows the deployment options and failure scenarios 
that they can tolerate.
%Figure~\ref{fig:Zookeeper write latency} shows the zookeeper write latencies for different deployment configurations, in which the baremetal one shows the optimal performance in a non-virtualization deployment. 
We compared \echo{} with OpenEPC which stores UE context in memory.
We also compared user perceived performance of \echo{} and T-mobile. 
We introduced node crashes to the prototype and illustrate \echo{} is robust against failure events.  



%% Candy's text:
%% The durability of the Zookeeper cluster depends on both the Zookeeper configuration and the cloud deployment option. 
%% For one datacenter deployment, in order to be able to recover from failures of all of the zookeeper servers in situations such as datacenter failures, disk logging is favorable because all logging data are saved on disk so zookeeper servers can fully recover states after the failure of all servers. The downside of disk logging is higher latency, but as in-datacenter network latency is low, the overall latency is still acceptable after adding disk logging latency. For use cases that prioritize performance and can tolerate a small bit of data loss in rare cases such as datacenter failures, Disk-nFC is a good choice. With Disk-nFC, latency can be reduced a lot, but as logging data is asyncronously written to disk, when all of the Zookeeper servers fail, some logging information that haven't been written to disk at the time of the failure are lost and cannot be recovered, although the amount of lost data is usually very small. Zookeeper configuration using Ramdisk is not recommended for one datacenter deployment, because with Ramdisk configuration, all data will be lost and cannot be recovered if all of the zookeeper servers fail at the same time, and the performance benefit is negligible as the performance of Ramdisk configuration is similar to that of the Disk-nFC configuration. Another downside of Ramdisk configuration is that when a Zookeeper server is recovering from a failure, as all previous data are lost, it has to fetch data from peer servers so that it takes long time to recover.
%% For 3 datacenter deployment, disk logging is not quite necessary because it is virtually impossible for all servers in 3 different datacenters to fail at the same time, so Disk-nFC configurtion suffices to provide strong durability. Ramdisk configuration is not favorable still for 3 datacenter configuration, because of the high server recovering overhead after failures.  



    %% We varied ZK configuration to see how that affects \echo{}. 
    %% The main intuition is that how ZK logs its operation could  
    %% trade-offs latency and reliability in \echo{}.
    %% In specific, we used three ZK configurations:
    %% \begin{trivlist}
    %%   \item \textbf{Disk logging:} ZK logs znodes into hard disk. 
    %%   Because of disk I/O, this configuration incurs higher 
    %%   latency. In term of reliability, because UE context is persistent in disk, node crashes 
    %%   won't cause UE context to be lost. Also, a crashed node will be able to rejoin the ZK ensemble quickly 
    %%   as it logs operations in hard disk.
    %%   \item \textbf{Ramdisk logging:} ZK logs zodes into Ramdisk (memory). This avoid the disk I/O 
    %%   thus incurs smaller latency. However, UE context is lost if the ensemble becomes unavailable.
    %%   A crashed node would also join the ensemble as a new node quick could take up more time to recover.
    %%   \item \textbf{Asynchronous Disk logging:} ZK logs znodes into disk asynchronously (without Force Sync or nFC.) This eliminates disk I/O latency and also persists most of UE context into disk.
    %% \end{trivlist}
    
\begin{table}[]
  \centering
  \caption {ZK configurations and cloud deployment options in \echo{} evaluation with their latency and reliability profiles: Disk-nFC and Ramdisk configurations have smaller latency while 3DCs cloud deployment could tolerate 1 DC failure.}
  \label{table:cloud-deployment-options}
        {%\small
          \footnotesize
          \begin{tabular} {|c||c|c|c|c|}
            \hline
            %\multirow{2}{*}{\textbf{Option}} & \textbf{Latency} & \multicolumn{2}{c||}{\textbf{Reliability}}  \\ \cline{2-4}
            \multirow{2}{*}{\textbf{Option}} & \textbf{Latency} & \multicolumn{3}{c|}{\textbf{Robust against failures}}  \\ \cline{3-5}
            &                   &   \textbf{Node} & \textbf{Avail. Zone}  & \textbf{DC}   \\ \hline \hline
            OpenEPC                         & Low               &    No        &            No          & No                        \\   \hline 
            1DC,Disk                       & Moderate          &    Yes       &            Yes         & No                       \\   \hline
            1DC,Disk-nFC                   & Low               &    Yes       &            Yes         & No                       \\   \hline
            1DC,Ramdisk                    & Low               &    Yes       &            Yes          & No                       \\   \hline
            3DCs,Disk-nFC                  & High              &    Yes       &            Yes         & Yes                        \\   \hline   
          \end{tabular}
        }
\end{table}


\begin{figure}[tbh]
    \includegraphics[height=4cm, width=4cm]{echo-conext-17/figs/results/attach-service-request-latency.eps}
    %\caption{Attach procedure latency observed on eNodeB. }
    \caption{Attach (top) and Service Request (bottom) procedures latency on 1DC deployment, observed on eNodeB}
    \label{fig:latency-msg}
\end{figure}


\begin{figure}[tbh]
    \includegraphics[height=4cm, width=6.5cm]{echo-conext-17/figs/results/latency-each-msg.eps}
    %\caption{Attach procedure latency observed on eNodeB. }
    \caption{Latency of each individual message in an Attach (left part) and Service Request (right part) procedures on 1DC deployment}
    \label{fig:latency-procedure-attach-sr}
\end{figure}

\begin{figure}[tbh]
    \includegraphics[height=3.5cm, width=4cm]{echo-conext-17/figs/results/inter_dc_zk.pdf}
    %\caption{Attach procedure latency observed on eNodeB. }
    \caption{Latency for attach procedure on 1DC and 3DC deployments}
    \label{fig:reliability-vs-latency}
\end{figure}


\begin{figure}[tbh]
    \includegraphics[scale=0.5]{echo-conext-17/figs/zk_write_latency_cdf.png}
    %\caption{Attach procedure latency observed on eNodeB. }
    \caption{Latency vs. reliability: Network latency CDF for ZooKeeper write. Baremetal shows optimal, non-virtualized performance.}
    \label{fig:Zookeeper write latency}
\end{figure}

\begin{figure}[tbh]
    \includegraphics[height=3cm, width=4.5cm]{echo-conext-17/figs/results/attach-mme-time-cdf-with-timer.eps}
    %\caption{Attach procedure latency observed on eNodeB. }
    \caption{Network latency CDF for attach procedure.}
    \label{fig:latency-msg-attach-cdf}
\end{figure}

\begin{figure}[tbh]
    \includegraphics[height=3cm, width=5.5cm]{echo-conext-17/figs/results/end-to-end.eps}
    %\caption{Attach procedure latency observed on eNodeB. }
    \caption{UE-perceived latency for attach procedure (top) and UE-perceived latency for service procedure (bottom).}
    \label{fig:end-to-end-latency}
\end{figure}




\vskip 4pt
\noindent\textbf{Correctness.}
We deployed \echo on one Azure data center and ran it for 7 days.
We generated 6,720 Service and Context Release requests (20,160 messages) 
from a Nexus 5 device attached to a eNodeB.
The system remained stable and all requests were correctly processed.
We next randomly introduced node reboot and process crash events
on 1\% of control messages;
\echo recovered from crashes and all messages were correctly processed.




%  \subsection{Results}
\vskip 4pt
\noindent\textbf{Latency.} 
Figures~\ref{fig:latency-procedure-attach-sr} shows latency of entire Attach (top) and Service Request (bottom) procedures with different ZK configurations running in a DC.
The latency is broken down into EPC core network - the latency between EPC components (including ZK);
Network time - network round-trip time between eNodeB and Azure;
and Radio - latency to set up radio bearers on UE and eNodeB hardware.
Overall, \echo{} introduces only about 7\% (70~ms) more latency for an Attach compared to OpenEPC,
which stores UE context in memory. 
%which is almost negligible. 
The overall latency is dominated by radio bearer configuration between UE and eNodeB. 


\begin{figure}[tbh]
    \includegraphics[height=4cm, width=4.5cm]{echo-conext-17/figs/results/throughput-procedure.eps}
    %\caption{Attach procedure latency observed on eNodeB. }
    \caption{Throughput of Attach and Service Request.}
    \label{fig:throughput}
\end{figure}

\begin{figure}[tbh]
    \includegraphics[height=3.7cm, width=5.3cm]{echo-conext-17/figs/results/mme-crashes-openepc.eps}
    %\caption{Attach procedure latency observed on eNodeB. }
    \caption{Unmodified OpenEPC MME crash results in an outage.}
    \label{fig:mme_crashes_openepc}
\end{figure}

\begin{figure}[tbh]
    \includegraphics[height=3.7cm, width=5.3cm]{echo-conext-17/figs/results/mme-crashes-zk.eps}
    %\caption{Attach procedure latency observed on eNodeB. }
    \caption{\echo{} MME crash avoids outages.}
    \label{fig:mme_crashes_zk}
\end{figure}





\vskip 4pt
\noindent\textbf{Individual message overheads.} 
Figure~\ref{fig:latency-msg} shows
the latency overhead \echo{} introduced to each message exchanged between UE and MME in an 
Attach (left part) and Service Request Procedure (right part).
The odd-numbered messages (1-Attach Request, 3-Authentication Response, 5-Security Mode Complete, 7-UE Information Response, 1-Service Request) are sent by the UE and are processed by \echo{}.
The even-numbered messages (2-Authentication Request, 4-Security Mode Command, 5-UE Information Request, 8-Attach Accept, 2-Context Setup Request) are sent by \echo{} and processed by the UE.
The results confirm that radio setup and authentication processing on UE (msgs. 2-left, 8, 2-right) dominate the total procedure latency. 
Looking at \echo{} latency (i.e., msgs. 1-left, 3, 5, 7, 1-right) we can see a clear latency
overhead trend among \echo{}-Disk, \echo{}-nFC and OpenEPC. Overall, using disk logging incurs
the most latency overhead while using disk without force sync (Disk-nFC) incurs less latency.
The per message overhead \echo{} introduced is small but noticeable, about 40\%.
%Because Disk-nFC has the same latency overhead with Ramdisk configuration while being more reliable,
%we will choose Disk-nFC as the configuration for \echo{} in the following evaluation.

 


\vskip 4pt
\noindent\textbf{Reliability vs. Latency trade-off.}
Figure~\ref{fig:reliability-vs-latency} shows latency of an Attach Procedure with ZK deployed in a single DC and 3 DCs.
\echo{} with multiple-DC deployments will survive DC failures (Table~\ref{table:cloud-deployment-options}) yet incur higher latency because of  network latency between ZK nodes (40\% or 400~ms more for attach procedure). 
Depending on the response time and reliability characteristics required, one may favor one option over the other. 
For example, public Internet outages can simply be relayed from reachable data centers if this is a viable option for a 
particular deployment. 
However, even with the most extreme deployment, \echo{} incurred overhead is still tolerable for UE operating 3GPP protocols.
We further probe into this by showing a CDF of the latency of each ZK write (Figure~\ref{fig:Zookeeper write latency}) and 
each message on \echo{} MME in an Attach procedure (Figure~\ref{fig:latency-msg-attach-cdf}). 
Replication to 3~DCs can incur 10$\times$ messaging latency as it may invoke several ZK writes. 
Yet, this is still only a fraction of the total latency and 
well below the smallest timeout value of an UE -- 5s for T3417
(see section 10.2 in 3GPP NAS timers~\cite{nas.protocols}, 3GPP S1AP timers~\cite{s1ap.3gpp}.)
In future, this could be improved by using closer data centers or
closer integration of a consensus protocol into Algorithm~\ref{alg:transaction} to reduce the number of writes. 
  




\vskip 4pt
\noindent\textbf{UE-perceived latency.}
Figure~\ref{fig:end-to-end-latency} shows the latencies of Attach and Service Request procedures perceived by a UE on \echo{} and T-mobile.
Since we can't capture T-mobile control messages inside their proprietary EPC deployment,
we measured the latency by triggering Attach and Service Request on the Nexus 5, 
using the same methodology on both platforms for a fair comparison.
To trigger an Attach we toggle the airplane mode in the Nexus 5.
To trigger an Server Request we let the device idle to make sure it releases radio connection, and trigger a Ping request from it to the Internet.
We then measure the time it takes for the Nexus 5 to be network-available (from the trigger-time to the first Ping packet gets through.)
As these latencies include phone-level overheads, they correspond to end-user perceived latencies, and they are much larger than network measured latencies (Figure~\ref{fig:echo-overhead1}). 
Overall, \echo{} control procedure latency on one DC is comparable to T-mobile, but worse on 3 DCs. However,
control events are infrequent, so we expect that this will not affect end-user experience. 
%This suggests users would have at least the same experience with \echo{} compared to other carriers.
   


\vskip 4pt
\noindent\textbf{Throughput.}  
Figure~\ref{fig:throughput} shows \echo{}'s peak throughput on 1~DC and 3~DCs for Attach and Service Requests. 
\echo{} throughput is comparable to OpenEPC.
Even though the throughput does not look very high, notice that each procedure consists of multiple messages exchanged
(e.g., 8 messages for an Attach),
and it is comparable with throughput reported in other papers~\cite{arijit.conext.2015}. 






\vskip 4pt
\noindent\textbf{Failure scenarios.} 
Figures~\ref{fig:mme_crashes_openepc} and~\ref{fig:mme_crashes_zk} 
show OpenEPC and \echo{} operation when an MME crashes. 
The UE attached to OpenEPC was not able to use the network for 54 minutes because of
the crash, whereas with \echo{} the UE continued to use the network without disruption.
In Figure~\ref{fig:mme_crashes_openepc}, the UE attached and successfully requested 
services (via Service Requests) between 0-20 mins. At minute 23rd, the MME restarted.  
The UE was not able to use the network for 22 mins after the restart (red crosses 
denotes failed Service Request) until a failed periodical Tracking Area Update 
(at 55th minute) which triggered a re-attach (similar to example~\ref{fig:mme-crashes}).
On the other hand, as in Figure~\ref{fig:mme_crashes_openepc}, there were 
2 MME instances in \echo{}. 
At minute~11 the MME1 instance restarted, the eNodeB reestablished 
the S1AP connection after the crash, the UE's requests were load balanced to 
the MME2 instance and were processed successfully (blue dots after 11th minute).
Note that the 1st attach request on MME2 experienced a slightly higher latency because MME2 had to contact ZK for the UE's Context.
This illustrates the advantage of \echo{} over OpenEPC in term of reliability against node crashes.

%\begin{figure}[]
%  \centering
%  \includegraphics[scale=0.5]{echo-conext-17/figs/results/mme-crashes-openepc.eps}
%  \caption{OpenEPC's MME restart causes 54-min service outage. At around 23s the MME restarts. 
%  It lost the UE Context therefore following Service Requests from the UE failed (red crosses.) 
%  After 54 minutes the UE performed a periodical Tracking Area Update (TAU) and was also failed.
%  The UE then performed Attach at around 56 minutes and service was recovered.}
%  \label{fig:mme_crashes_openepc}
%\end{figure}

%\begin{figure}[]
%  \centering
%  \includegraphics[scale=0.5]{echo-conext-17/figs/results/mme-crashes-zk.eps}
%  \caption{\echo{}'s MME crashes does not cause service outage on UE. 
%  At around 11th minute MME1 in \echo{} was crashed. The eNodeB agent 
%  detected a connection lost therefore reestablish a connection with MME2 
%  (S1 setup request at 11th minute.) Following Service Request was successfully 
%  served by MME2 without any disruptions.
%  Note that the 1st request on MME2 had higher latency because MME2 had to 
%  fetch ZK for the UE's Context.
%  }
%  \label{fig:mme_crashes_zk}
%\end{figure}

%\textbf{ZK crashes event:} figure ~\ref{fig:zk_crashes}
%  \begin{figure}[]
%    \centering
%    \includegraphics[scale=0.3]{echo-conext-17/figs/zk_crashes.png}
%    \caption{ZK crashes and UE continue to have services}
%    \label{fig:zk_crashes}
%  \end{figure}


%% BR: This is now in correctness section
%% \vskip 4pt
%% \noindent\textbf{Injected failures}
%%     \xxx{
%%       - Deploy in Azure.\\
%%       - Introduce failures: MME crashes, ZK node crashes.\\
%%       - UE sends periodically traffic: every 5 mins.\\
%%       - Measure: period that the UE does not have service.\\
%%     }


\vskip 4pt
\noindent\textbf{eNodeB client.}
We deployed our eNodeB's \echo client implementation on IP Access E40 eNodeB~\cite{ipaccess} as a user-mode daemon.
We configured four mobile nodes to perform data transfers and then sleep over periods of 1 minute,
generating 8 requests per minute.
A typical small cell can support up to 64 active users, so this represents a typical load.
The induced CPU load was not noticeable on the embedded Linux monitor. 

    
