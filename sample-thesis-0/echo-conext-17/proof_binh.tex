%\newtheorem{theorem}{Theorem}
\newtheorem{property}{Property}
\newtheorem{assumption}{Assumption}
%\newtheorem{corollary}{Corollary}[theorem]
%\newtheorem{lemma}[theorem]{Lemma}

Here we give a sketch of why ECHO is safe even though components are redundant
and non-blocking under failure. Showing that 
\echo{} {\em appears} to process operations atomically, in client FIFO order,
one-at-a-time demonstrates safety. 


The proof first needs to show that a leaf component (a component that 
does not trigger side effects to other components) operates linearizably 
(i.e., serializable and in FIFO order). 
It then uses induction, where the base case is the leaf component,  
to show that every component in \echo{} operates linearizably 
and so does \echo{} as a whole. Finally, because the requests arrive 
at \echo{} in the client FIFO order, \echo{} operates in client FIFO order. 



\begin{assumption}[Idempotent operation]
\label{idempotent}
Each leaf component instance processes requests from other components idempotently; that is, a retry causes 
the same effect on the component.
\end{assumption}


With the assumption of idempotent operation of a leaf component, we prove that a leaf component operates linearizably.

\begin{lemma}[A leaf component is linearizable]
\label{leaf-linearizability}
Each transition on a leaf component instance is linearizable; that is, it processes operations atomically in some total order consistent with the request ID.
\end{lemma}



\noindent
\textbf{Proof.}
As shown in Figure~\ref{fig:leaf-component-proof}, 
%an operation takes effect some time between its first invocation (Request $n$) and its acknowledgment (Reply $n$).
%Component instances only process messages transaction ID order, which is consistent with client invocation order.
given that the update to the shared State Storage is atomic, individually, each processed request with ID $n+1$ results in one of four outcomes: 1) {\em aborted}-the component is not in state $n$ or $n+1$, so the request is invalid and ignored; 
%(i.e., at step~\ref{lst:line:check_sequence_number})
2) {\em successful}-the operation completes successfully, the component instance updates the State Storage, moves 
the component from state $n$ to $n+1$ at step~\ref{lst:line:commit} and replies; 3) {\em crashed before update}-the operation fails in \circled{1} before updating the state leaving the component in state $n$; or 4) 
{\em crashed after update}-the operation fails in \circled{2} after updating the state 
leaving the component in state $n+1$ without a reply.

In case 3, because of the eventual completeness, there must be another 
instance that progresses to either case 4 (in-completed) or case 2 (completed).
In case 4, 
%since the component is already in state $n+1$ and the reply is recorded 
%in the State Storage (step~\ref{lst:line:check_reply_exists}), 
%, the effect is the same 
%because the component is idempotent by assumption~\ref{idempotent}. 
%That is, 
another component instance when receives a retry, simply replies 
with the recorded reply which eventually results 
in case 2. Therefore, a component only executes requests in the specified order, either completely successful or failed.
\qed


%In fact, in all components the final update to shared storage serves a linearization point for the processing of each message by a whole component.
%It is a single, atomic, FIFO ordered operation that all processed messages must reach before they are acknowledged to the client.
%The final update to the top-level component's shared state serves as the linearization point for each transaction.



Given that a leaf component operates linearizably, we can show that \echo system is linearizable.


\begin{lemma}[\echo's linearizability]
\label{linearizability}
\echo is linearizable; that is, it appears to process operations atomically in some total order consistent with the requests order of the client.
\end{lemma}

\noindent
\textbf{Proof.}
\echo's linearizability could be proved using induction with the base case is the 
{\em leaf component}. 

{\em Base case:} As in lemma~\ref{leaf-linearizability}, a leaf component operates 
linearizably. 

{\em Induction hypothesis:} Now assuming component $M$ operates linearizably, we need to prove component $M+1$ operates linearizably. 

\iffalse
  \begin{figure*}[!ht]
    \begin{subfigure}{.33\textwidth}
      \centering
      \includegraphics[scale=0.4]{echo-conext-17/figs/echo-leaf-proof.pdf}
      \caption{}
      \label{fig:leaf-component-proof}
    \end{subfigure}%
    \begin{subfigure}{.28\textwidth}
      \centering
      \includegraphics[scale=0.4]{echo-conext-17/figs/echo-linearizability-proof.pdf}
      \caption{}
      \label{fig:linearizability-proof}
    \end{subfigure}
    \begin{subfigure}{.36\textwidth}
      \centering
      \includegraphics[scale=0.5]{echo-conext-17/figs/mme-implementation.pdf}
      \caption{}
      \label{fig:agent-layer}
    \end{subfigure}
    \vspace{-5mm}
    \caption{
      (a) \echo's leaf component is linearizable;
      (b) \echo is linearizable;
      (c) Changes that \echo introduces to conventional EPC architecture (here showing MME as a sample components). 
            }
  \label{fig:proof-and-implementation}
  \vspace{-3mm}
  \end{figure*}
\fi

\iffalse
\begin{figure*}
  \begin{figure}[h]
    \centering
    \includegraphics[scale=0.5]{echo-conext-17/figs/echo-leaf-proof.png}
    \caption{\echo's leaf component is linearizable.}
    \label{fig:leaf-component-proof}
  \end{figure}

  \begin{figure}[h]
    \centering
    \includegraphics[scale=0.5]{echo-conext-17/figs/echo-linearizability-proof.png}
    \caption{\echo is linearizable.}
    \label{fig:linearizability-proof}
  \end{figure}

  \begin{figure}[]
    \centering
    \includegraphics[scale=0.5]{echo-conext-17/figs/mme-implementation.png}
    \caption{Changes that \echo introduces to conventional EPC architecture (here showing MME as a sample components).}
    \vspace{-0.15in}
    \label{fig:agent-layer}
  \end{figure}
\end{figure*}
\fi 
To prove that component $M+1$ operates linearizably, similarly, we examine an operation 
of Request $n$ on component $M+1$. Similar to the leaf component proof above, the request has 4 outcomes: {\em aborted, successful, crashed before update and 
crashed after update}. The {\em crashed after update} outcome   
eventually results in {\em successful} case as in the proof above. In the {\em crashed before update}  
case, if the component crashes in \circled{2}, 
eventually there is an instance triggers the side effect and progresses to case 2. 
If there are multiple instances trigger multiple side effects, 
the effect is linearizable as induction hypothesis. 
Therefore, component $M+1$ is linearizable.
\qed
%1) the component instance is not in state $n$ or $n+1$, so the request is ignored; 2) the operation completes successfully, the component instance updates the State Storage (moving from state $n$ to $n+1$) and replies, 3) the operation fails before updating the state (i.e., fails in (1) period and leaves the component in state $n$), or 4) the operation fails after updating the state (i.e., fails in (2) period and leaves the component in state $n+1$).
%In case 4, similarly the component is already in state $n+1$ therefore another 
%component instance simply replies with a recorded reply (idempotent operation) when 
%a retry arrives. This eventually results in case 2.
%In case 3, eventually there is an instance triggers the side effect and progresses to case 2. If there are multiple instances trigger multiple side effects, the effect is still linearizable as assumed. Therefore, component $M+1$ is linearizable.
%\qed

%shorten version
\iffalse
Lemma~\ref{linearizability} can be immediately strengthened, since the total
order above is precisely mobile device's FIFO order, therefore \echo{} is 
correct.


\begin{lemma}[FIFO Processing]
\echo appears to processes operations atomically, in client FIFO order, one-at-a-time.
\end{lemma}

%Finally, since the ordinary, unreplicated protocol precisely processes messages atomically, in client FIFO order, one-at-a-time, this gives the essential safety property:

\begin{property}[\echo's Safety Property]
\echo appears to processes operations atomically, in client FIFO order, one-at-a-time. 
Or the set of states observed by \echo clients is equivalent to the unreplicated (EPC) protocol.
\end{property}
\fi

Lemma~\ref{linearizability} can be immediately strengthened, since the total
order above is precisely mobile device's FIFO order.

\begin{lemma}[FIFO Processing]
ECHO appears to processes operations atomically, in client FIFO order, one-at-a-time.
\end{lemma}

Finally, since the ordinary, unreplicated protocol precisely processes messages atomically, in client FIFO order, one-at-a-time, this gives the essential safety property:

\begin{property}[ECHO Safety Property]
The set of states observed by ECHO clients is equivalent to the unreplicated protocol.
\end{property}
