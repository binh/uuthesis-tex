\chapter{Conclusion}
  \label{chapter:conclusion}
  \vspace{-10mm}
  \section{Summary of the dissertation}
The next generation (5G) mobile network is expected to host emerging use cases that have a wide range of requirements; from Internet of Things (IoT) devices 
that prefer low-overhead and scalable networks, to remote machine operation or remote 
health-care services that require reliable end-to-end communications. 
However, the (4G) mobile core network 
has several drawbacks in terms of enabling new technologies and 
supporting new applications. On the other hand, SDN and NFV are 
the key technologies that improve flexibility and agility in 
fixed networks. Applying SDN/NFV together with big-data analytics and 
distributed system techniques could enable a {\bf flexible, low-overhead, and reliable mobile core network.}


This Dissertation focused on improving scalability and reliability of the mobile 
network for future applications. 
The Dissertation presented \smore~\cite{smore} and \simeca~\cite{simeca} which use Software Defined Networking (SDN) and Network Function Virtualization (NFV) to enhance scalability of the mobile core network for a massive number of Internet-of-Things (IoT) devices. It then presented \absence~\cite{absence} which uses big-data analytics and a novel failure detection technique to detect failures in an operational mobile network. Lastly it presented \echo~\cite{echo} which uses distributed system techniques to enhance availability of software-based mobile networks in public clouds.


Chapter~\ref{chapter:chap1} focuses on a flexible, low-overhead and scalable mobile core network. 4G mobile core networks are largely based on 
proprietary and expensive hardware components that limit both 
new functionalities as well as low latency applications. 
\smore is an SDN-based offloading architecture for LTE/EPC network. 
Using an SDN infrastructure and an SDN controller, together with a proper monitoring mechanism, 
\smore offloads a portion of LTE traffic to a local edge cloud to support low latency applications. The challenges of \smore are to find a practical solution given the 
current deployment of the mobile network and to be transparent to existing LTE traffic. 
\smore intercepts data and control plane traffic at Mobile Telephone Switching Office 
(MTSO). It monitors the LTE control channel to extract data plane information used 
in data plane offloading. It offloads registered traffic to the mobile edge cloud and leaves normal LTE traffic untouched.
 

In terms of network architecture and protocols, the current EPC core network 
was designed to support a small number of devices streaming high-quality traffic (e.g., a smart-phone streaming a Youtube video) rather than a large amount of 
devices sending small and sporadic traffic (e.g., a massive number of IoT devices 
sending a few bytes every 15 minutes).


\simeca is an SDN-based mobile edge cloud architecture that provides 
a new network service abstraction that is more suitable for IoT devices. 
Unlike the control and data planes of LTE/EPC that are based on IP tunnels to realize end-to-end communications with QoS supports, \simeca's data plane is best-effort to reduce overheads. \simeca's control and data planes are more light-weight (up to 20\%/37\% lower data/control overhead respectively) compared to LTE/EPC's. Removing IP tunnels in the mobile core, however, raises a challenge of how to support mobility in \simeca. Moreover, designing data and control 
planes that are more light-weight while supporting mobility is not straight-forward. 
\simeca proposed an SDN-based mobile edge cloud infrastructure that is more distributed 
and SDN-controlled. To reduce control overhead, \simeca classifies packets at the edge (base stations) while core network switches simply forward packets based on the packet header (i.e., smart edge - dumb core principle). At the base stations, packet headers are translated to 
prevent adding any extra overhead to the data plane. A device in \simeca has 
a device ID used for end-to-end applications and a routing ID used for routing 
and mobility. This identity separation mechanism together with a routing ID tracking 
mechanism allow seamless mobility in \simeca. 




In order to improve service experience and availability of mobile networks, network operators have already deployed multiple monitoring systems to detect service disruptions and fix problems when they occur. However, detecting all service disruptions is challenging. Somewhat counter-intuitively, monitoring network components is insufficient to indicate user experience because of a complex relationship between the network status and user-perceived service experience. Moreover, service disruptions could happen because of reasons that are beyond the network itself, e.g., bugs on the customer's device.


Chapter~\ref{chapter:chap2} presented \absence, a passive service monitoring system that detects service disruptions by monitoring 
customer usage. The main idea in \absence is that customer usage, or lack thereof, 
is a reliable indicator of service disruptions or network problems. Using users' 
Call Data Records (CDR), \absence obtains a normal usage pattern of an aggregation 
of customers and infers an anomaly if the current usage is less than expected. 
\absence uses Map-Reduce and a large compute cluster to extract the real-time 
usage of different aggregations of users from Tera-Bytes of CDR generated 
constantly in the network. It then uses the time series decomposition technique to 
decompose the usage time series and detect anomalies. 
\absence was able to detect failures with up to 95\% accuracy. It also detected real 
failures that went under the radar in an operational network.


With technology advancements in SDN and NFV, next generation mobile networks are expected to be NFV-based. However, in stark contrast to telecom-grade hardware with built-in redundancy, commodity Off-the-Shell (COTS) hardware in NFV platforms often can't compare in terms of reliability. Availability of Telecom-grade mobile core network hardware is typically 99.999\% (i.e., less than 5 minutes of downtime per year) while most NFV platforms only guarantee 99.9\% availability (less than 5 minutes of downtime per 3 days). Therefore, an NFV-based mobile core network running on a less-reliable infrastructure requires extra mechanisms to guarantee its availability. 


Chapter~\ref{chapter:chap3} presented \echo, a distributed mobile core architecture 
that solves the availability problem for NFV-based mobile core networks. 
\echo preserves the mobile core protocols as they are complex and constantly evolving.
\echo adds extra abstractions on top of the mobile core 
network to compensate for the fact that nodes in an NFV-based infrastructure 
could be arbitrarily slow or crashed, and messages could be lost or delayed. 
\echo's reliability is based on a \emph{necessarily reliable} agent at each 
base station that serializes requests and keeps sending a request 
until it receives a reply. 
Mobile core components are replicated and their state is stored in a replicated and 
highly available key-value store. \echo then uses a nonblocking algorithm to 
ensure only one stateless instance can modify the control state, and stale requests 
are blocked to avoid state inconsistency. The algorithm is applied on all components 
in the mobile core in a recursive manner to achieve an atomic state change across 
distributed components as if they were on a single reliable component. \echo 
was deployed on Microsoft Azure cloud peering with an LTE small cell and a mobile 
smart-phone in PhantomNet~\cite{phantomnet}. \echo adds no significant overhead to the current 
mobile core network and is more reliable when multiple failure events were introduced.


\section{Future research directions}

This dissertation presented a study on the scalability and reliability aspects of the mobile network. Given the huge shift of the network to the next generation (5G) architecture, there are still a lot of topics to explore. This section gives a few research directions that take into account expected changes to future applications and the network infrastructure to support future requirements. The section first offers some insights about the interaction between applications and the mobile network, and then explores further possible improvements to the architecture and protocols of a software-based mobile networks in a distributed Mobile Edge Computing (MEC) infrastructure.

\subsection[Bridging the gap between applications and the mobile network]{Bridging the gap between applications\\ and the mobile network}
In the current mobile architecture, the end-point device 
(and applications running on it) treats the network as a black-box 
that can't be tailored to meet the applications' requirements. 
However, unlike wired networks, performance of mobile networks largely depends 
on the radio link performance and the protocol configuration at the base stations. 
A single wireless configuration is not likely to satisfy the wide range of 
requirements of all types of applications.   
For example, a previous work~\cite{tcp} pointed out that real-time interactive 
applications such as VoIP might prefer seamless handover while bulk-transfer 
applications such as web-browser or ftp might prefer lossless handover.


Bridging the gap between applications and the network allows applications   
to configure the network (or the network stack), make use of network 
information, or interact with the network to satisfy their specific requirements. 
For example, knowing the current load 
of the base station could help an application to adjust its sending rate to 
prevent harmful spurious TCP timeouts~\cite{tcp},  
or a police body camera application could request the network to reserve a prioritized  
connection that has enough bandwidth for a crime-site video-streaming when needed. 


To enable this, we need APIs to expose network's functionalities, network configurations and the end-point device's network stack. 
The network's functionality API exposes interfaces to manage resources in 
the network (e.g., set up or reserve bandwidth of a connection). 
The network's configuration API enables network control/data plane configuration such as 
handover type, Radio Link Control (RLC) queue size, RLC modes, etc. The network stack API allows 
the application to configure the device's network stack such as changing TCP's congestion window, increase TCP's Retransmission Timeout value, etc.

\subsection[Architectural and protocol-level improvements for NFV-based mobile edge computing]{Architectural and protocol-level improvements\\ for NFV-based mobile edge computing}
Future mobile networks are expected to be NFV-based. However, moving from a hardware-based mobile network to a software-based solution invalidates many existing assumptions and raises multiple challenges. One challenge is that unlike telco's specialized high-performance hardware, commodity OTS hardware 
in an NFV platform can't meet the reliability and scalability requirements.
For example, a VM clearly can't store the state and serve requests from 
millions of users as a more powerful hardware box could. Moreover, mobile edge computing makes 
the network infrastructure become more distributed. Exploring trade-offs in this new infrastructure is an interesting topic. For example, a distributed 
EPC instance at the edge would have less load per instance (and thus the need for an extreme scaling of the distributed EPC is reduced), but in the meantime the distributed 
EPC instances can only cover a smaller mobility area.


Firstly, one way to improve reliability of NFV-based mobile networks is to add extra abstractions to the existing LTE/EPC protocols~\cite{echo}. Another way to enhance 
reliability is to redesign the protocols and take into account the 
new NFV's assumptions. For example, given an unreliable NFV infrastructure, 
a ``good'' protocol on the end-device should \emph{proactively} check for state 
inconsistency and fix the state immediately instead of waiting for a periodic Tracking 
Area update and reattach~\cite{echo}.
Secondly, to scale the NFV-based mobile network given relatively small compute instances, a horizontal scaling approach would help. However, the approach should take into account the architecture of the mobile core and how the components interact. Interesting questions, for example, are: (1) When scaling the data plane, how do the network 
instances store their state in a scalable manner? (2) How to update the state in the data plane when there is mobility.
Lastly, to balance the performance and overhead of a distributed EPC architecture in MEC, 
a hybrid solution might help: a local EPC could be used to improve performance, while a centralized EPC could be used for a portion of users (or connections) that requires a high degree of mobility. This approach needs a proper separation of functionalities of  
the distributed EPC and the centralized EPC and how they interact.
