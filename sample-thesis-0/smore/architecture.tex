\subsection{\smore{} architecture}
  \label{sec:architecture}


The \smore architecture is depicted in Figure~\ref{fig:architecture}
in the context of an LTE/EPC mobile network. The service
goal of \smore is to reduce end-to-end delay for selected traffic
by offloading such traffic to \emph{offloading cloud} infrastructures
close to mobile devices. 
Regional aggregation points in the form of MTSOs represent an ideal target for deploying these offloading
infrastructures for two reasons. First, based on current centralized mobile
network deployments, all traffic passes through MTSOs en-route
to the Internet. Further, the MTSO locations
in a typical deployment are at a sweet spot, both in terms of 
geographical coverage (to reduce delay) and the number
of locations (filling the space between approximately 10 central
office locations and thousands of eNodeB locations). As
shown in Figure~\ref{fig:architecture}, \smore assumes 
that the offloading cloud platform and the \smore \emph{SDN substrate}
are deployed at MTSO locations.
The SDN substrate enables offloading to the offload cloud and
also allows the \smore \emph{monitor} to snoop on the LTE/EPC
control plane. The \smore monitor continuously monitors the
control plane, extracting relevant information. The monitor
enters information into a database and also provides 
triggers to the \smore \emph{controller} about UE
attach and mobility events. The \smore controller interacts
with the \smore SDN substrate to effect offloading
when needed and provides
a front-end that Internet based service providers can 
interact with to register for offloading services. 
Note that in all cases service providers using the
\smore service will only be able to offload traffic associated
with their own services.
\begin{figure}[t]
  \centering
  \includegraphics[scale=0.5]{./smore/figure/smore.pdf}
  \caption{\smore architecture} 
\label{fig:architecture}
\end{figure}

$\bullet$ {\bf On-demand use case.} The interactions between these components
are best explained with a typical use case, using the numbered
steps from Figure~\ref{fig:architecture}.
The \smore SDN and monitor allow
the control plane to be continuously monitored (\#1 in the figure).
To simplify exposition, we
assume an Internet-based service provider, e.g., a gaming
provider, who wants to make use of the \smore service.
Note that this generalizes to any Internet service provider
that wants to make use of the \smore offloading service.
End-users of the game service access the
Internet-based gaming frontend server via the normal
data path after attaching to the mobile network (\#2).
Realizing that it wants to offload the user in question
to an offload instance, logic in the gaming frontend
server requests offloading via the interface 
provided by the \smore controller (\#3). The \smore
controller consults the \smore database to determine
the current location of the UE in question, and proceeds
to configure the SDN substrate in the appropriate
MTSO location to set up offloading of the desired subset
of traffic (\#4). % The existence of the
%offloaded game server is communicated to
%the requesting UE.
%(E.g., via ``game server link``
%on the frontend server.)
When the UE connects to
the game server, this traffic will be transparently redirected to
the offload server in the MTSO cloud (\#5).

$\bullet$ {\bf Subscription use case.} Service providers (or end users)
can also opt to make use of a subscription model for offloading.
Once subscribed, information about the subscriber UE is maintained
in the \smore database. In this case, every time a subscriber
UE connects to the network, the offloading path is constructed
without the need for step \#3 in the previous scenario.
When the UE attaches, the
\smore monitor detects this event and signals the
\smore controller, which 
immediately installs the appropriate rules in the \smore SDN.

$\bullet$ {\bf Interaction with standard LTE/EPC.}
%\label{sec:triggers}
We now consider in more detail how \smore can realize its
functionality without requiring any modifications to
LTE/EPC network elements. We specifically consider how
the \smore monitor extracts information from the LTE/EPC
control plane during UE attach and handoff procedures.
The information obtained from these procedures
triggers the installation of rules in the \smore SDN
to realize offloading in a transparent manner.

\begin{figure}[t]
  \centering
  %\includegraphics[width=\columnwidth]{./figure/attach1.pdf}
  \includegraphics[width=0.9\columnwidth]{./smore/figure/attach.pdf}
%\vspace{-0.4cm} 
  \caption{Attach call flow}
%\vspace{-0.3cm} 
\label{fig:attach}
\end{figure}

$\bullet$ {\bf Attach.}
Figure \ref{fig:attach} shows how the \smore monitor 
snoops on the control plane interactions
between the eNodeB and the MME during UE attach
and subsequently helps trigger the pushing
of required offloading policies in the \smore SDN
via the \smore controller. Specifically,
during the UE attach procedure,
the \smore monitor extracts the UE's IMSI (International Mobile Subscriber Identity) 
and TAI (Tracking Area Identifier) from the `attach request message' 
sent to the MME via the eNodeB (\#2). 
When the MME responds with the 
`attach accept' message (\#5), 
the monitor extracts the UE's IP address, SGW's IP address, 
SGW's TEID, and UE's GUTI (Globally Unique Temporary ID). 
Finally, the monitor obtains the eNodeB's IP address and eNodeB's TEID 
when the eNodeB responds with the `context setup response' message 
(\#8).
At this point, the \smore monitor has all the 
information required to
trigger offloading and sends it 
to the \smore controller (\#9) which, in turn, updates 
its database. 
In the case of a subscription-based use case,
at this point the \smore controller 
pushes the required flows to the \smore SDN (\#10), which enables offloading
of game server specific traffic to the game server
hosted in the in-network cloud platform.
For the on-demand use case,
when the \smore controller receives
the offloading trigger from the 
gaming frontend server (\#13), it retrieves the UE's bearer information
from the database and pushes the required 
flows to the \smore SDN (\#14) to enable offloading.


$\bullet$ {\bf Handover.}
Figure \ref{fig:handover} shows the steps 
required to enable offloading in presence
of user mobility. Specifically we 
consider X2-based handover between eNodeBs without
MME and SGW relocation. (This is the common handover 
case in LTE/EPC.)
As seen in the figure, when the source eNodeB
decides to hand over the UE (based on measurement
reports) to the target eNodeB, it sends a handover request (\#2)
to the target which, in turn, does the necessary
resource allocation and sends back a handover acknowledgement
message (\#3) to the source.
The target also sends a path switch request 
message (\#4) to the MME that contains the target eNodeB
information (IP address and TEID), 
%specific to the target eNodeB,
which
will subsequently be used by the SGW to forward
downlink packets. The \smore monitor extracts
this bearer information,
and sends it to the \smore controller 
(\#5),
which proactively constructs a new rule 
to be pushed to the \smore SDN to enable 
processing of downlink packets
corresponding to the target eNodeB.
Note that all the other UE bearer
information remains unchanged (including
the SGW identifiers) and the \smore
controller obtains the other required information
from its database.
When the SGW updates the user bearer
to forward downlink packets to the 
target eNodeB instead of the source eNodeB,
the MME sends back a path switch 
acknowledgement message (\#10) to the 
target indicating the success
of the path switch request. When the 
\smore monitor sees this message, 
it sends a path switch trigger request (\#11) to the 
\smore controller, which, in turn, pushes
the previously constructed rule to the 
\smore SDN and deletes the previous rule (\#12).
Subsequently, the SDN substrate 
routes (after proper encapsulation)
all downlink packets from the cloud based game server
to the target eNodeB.  

\begin{figure}[t]
  \centering
 % \includegraphics[width=\columnwidth]{./figure/handover.pdf}
  \includegraphics[width=0.9\columnwidth]{./smore/figure/handover.pdf}
%\vspace{-0.4cm} 
  \caption{Handover call flow}
%\vspace{-0.3cm} 
\label{fig:handover}
\end{figure}








 







