\subsection{Introduction}

      Unprecedented growth in data traffic on mobile networks~\cite{moredata}
      is driven by
      the popularity of smartphones and tablets, advances in
      bandwidth available on mobile networks and the availability of
      a myriad of mobile applications and services.
      Despite these undeniable successes, 
      the delay experienced across core mobile networks
      is still problematic for many applications that need near-realtime
      communication.
      Online gaming applications, for example, have stringent quality
      of service (QoS) demands and latency can negatively
      interfere with game play~\cite{player.departure,game.delay.phd}. 


      A key architectural reason for delay remaining relatively high
      in the mobile core is the fact that the gateway
      nodes (i.e., Packet Data Network Gateways (PGW) in LTE/EPC
      nomenclature), are deployed in a highly centralized
      fashion~\cite{balakrishnan2009s}. This results in inefficient
      hierarchical routing and high delay---packets travel
      for a significant distance in the mobile core network
      before even reaching the Internet~\cite{hierarchical}.


      Approaches to reduce end-to-end delay involve
      various offloading strategies~\cite{bearerlipa,han2012mobile},
      including offloading to data centers inside the
      mobile provider core network footprint~\cite{banerjee2013moca}.
      Despite the potential benefits of these approaches,
      they are inherently difficult to deploy in real networks---because
      of the fundamental complexity of mobile architectures, approaches
      that require significant changes to those architectures are 
      very hard to realize in practice.

      In this work we propose an alternative approach to \emph{realizing}
      offloading in a mobile network:
      our Software defined network Mobile Offloading aRchitecturE (\smore).
      Like previous offloading approaches~\cite{banerjee2013moca}, we
      propose to deploy offloading data centers within the core of the 
      mobile network and to selectively redirect traffic to these
      data centers. The defining characteristic of \smore is that
      this offloading is accomplished \emph{without} requiring modifications to the  
      functionality of the core network proper, that is, without modifications
      to the functionality of network elements in the core mobile network.
      \smore is realized
      by deploying an SDN infrastructure at aggregation points in the
      mobile core network. This SDN infrastructure allows two functions
      critical to the functioning of \smore. First, it enables \emph{monitoring}
      of the mobile network control plane through a lightweight monitoring component.
      The \smore monitor continuously extracts information
      from the mobile network control plane and makes this information
      available to the \smore controller. Second, based on this information
      and the service logic executed by the \smore controller, the SDN
      infrastructure is used to transparently realize the offloading
      of selected traffic to offloading data centers.  
      We argue that the ability to deploy these changes without modifying
      functional components of the mobile core is critical to
      making the deployment of new functionality such as offloading
      practical.

      This work makes the following contributions:\\
      $\bullet$ We develop the \smore architecture to realize offloading in
      an LTE/EPC mobile network. Specifically, we show how offloading
      for selected traffic of subscribed users can be realized
      without modifying the standard LTE/EPC interactions, even
      when devices are mobile.\\
      $\bullet$ We present a prototype realization of our architecture
      using an LTE/EPC testbed and an Open vSwitch (OVS) based SDN.
      Specifically, our OVS enhancements allow transparent
      encapsulation and decapsulation of offloaded traffic.   

