\chapter{Background}
  \label{chapter:background}


  This chapter provides background knowledge of the LTE/EPC architecture: its network architecture overview, data service, connectivity abstractions, data and control planes and network stack. 
  This information is required to understand 
  the contributions in the following chapters.

    \section{Overview of network architecture}

      %In this section, we briefly describe the LTE/EPC mobile
      %network architecture, typical deployment and user plane (data
      %plane) protocol stack. 

      As shown in Figure~\ref{fig:lte_architecture}, a LTE/EPC network consists of a wireless radio access network (Radio Access Network or RAN) and a wired mobile core network (EPC core network).
      The RAN consists of multiple base stations (eNodeBs) that communicate with end 
      devices (User Equipment - UE) over a radio interface. The EPC core network 
      consists of a data plane and a control plane. The control plane interacts with the UE to authenticate and set up or modify connections in the data plane to 
      enable end-to-end communications between the UE and other IP networks. The 
      control plane also keeps track of the UE when it moves across eNodeBs. The data 
      plane consists of gateways that forward IP packets from the eNodeBs to another IP 
      network or Internet. 

      \begin{figure}[]
        \centerline{\includegraphics[scale=0.8]{figs/lte-epc-architecture.png}}
        \caption{LTE/EPC architecture}
        \label{fig:lte_architecture}
      \end{figure}

      A minimum EPC core network that provides data service will have Mobility Management Entity (MME), Home Subscriber Server (HSS), Policy and Charging Rules Function (PCRF) in the control plane, Serving Gateway (SGW) and Packet Data Network Gateway (PGW) in the data plane. Table~\ref{table:acronym} shows the abbreviations and their full names that are frequently used in this dissertation.

                \begin{table}[]
                    \centering
                    \caption {Acronyms and their full forms}
                    \label{table:acronym}
                    {\small
                    \begin{tabular} {|c||c|}
                        \hline
                        \textbf{Acronym}      & \textbf{In Full}   \\ \hline \hline
                         LTE     & Long Term Evolution \\
                         EPC     & Evolved Packet Core \\
                         EPS     & Evolve Packet System \\
                         PDN     & Packet Data Network \\
                         APN     & Access Point Name \\
                         GTP     & GPRS Tunneling Protocol \\
                         TEID    & Tunnel Endpoint Identification \\
                         UE      & User Equipment \\
                         eNodeB  & Evolved NodeB  \\
                         MME     & Mobility Management Entity \\
                         HSS     & Home Subscriber Server \\
                         PCRF    & Policy and Charging Function \\
                         SGW     & Serving Gateway \\
                         PGW     & Packet Gateway \\ 
                         QCI     & QoS Class Identifier \\ 
                         GBR     & Guaranteed Bit Rate \\ 
                         PDCP    & Packet Data Convergence Protocol \\
                         RLC     & Radio Link Control \\ \hline
                    \end{tabular}
                    }
                \end{table}


      An overview of interactions between components in a LTE/EPC network is as follows. After power up, a UE synchronizes with a nearby eNodeB and sets up a control 
      plane channel with the network's control plane (i.e., MME). The UE then authenticates 
      itself and the network by exchanging a shared secret it has on its preinstalled 
      SIM card with the MME. During authentication, the MME queries the HSS to obtain the  
      UE's subscriber information. If the authentication is successful, the UE then requests 
      to connect to an IP network, e.g., Internet. When it receives a connection request, 
      the MME messages the SGW which in turn messages the PGW to set up an Evolved Packet System (EPS) bearer in 
      the data plane. After setting up the EPS bearer, MME replies to the UE with the bearer information and the UE 
      is able to communicate with the IP network via the eNodeB. Similar interactions happen when the UE moves across eNodeBs. The UE requests a handover procedure with the MME which in turn updates the data plane to the target eNodeB. 

    \section{Data service in LTE/EPC}
      The LTE/EPC network is designed for IP services; the role of the RAN and the EPC 
      core network is to provide IP communications between two end-points, the UE and 
      an IP host that the UE is communicating with. All applications running on the UE can make use of the IP connection to exchange IP packets with the other end-point. 
      The applications could either be provided by the mobile operator, by a company in a corporate IP network or by a normal service provider in the Internet.



      Figure~\ref{fig:data_service} shows an application running on a data service in LTE/EPC. 
      After successfully attaching to the LTE/EPC network, the UE is assigned an 
      IP address. From the UE's perspective, the gateway node (i.e., PGW) is the first IP hop for the assigned IP of the UE. There is a point-to-point link between the UE and the PGW through an eNodeB and a SGW (not shown in the figure). All traffic generated by the applications on the UE must go through the PGW via the point-to-point link. 
      This way, the PGW is an IP anchor for the UE upon mobility. When the 
      UE moves across base stations, the point-to-point link changes. However, its associated PGW never changes and therefore 
      the IP connection survives the mobility. 

     \begin{figure}[h]
        \centerline{\includegraphics[scale=0.8]{figs/data-service.png}}
        \caption{Data service in LTE/EPC}
        \label{fig:data_service}
      \end{figure}



    \section{Connectivity abstractions in LTE/EPC}
      The LTE/EPC network provides different connectivity abstractions for the UE. 
      A UE could have one or multiple Packet Data Network (PDN) connections to one or multiple IP networks. A  
      PDN connection is the most coarse-grain connectivity abstraction a UE can have. Under each PDN connection, the UE can set up multiple EPS bearers each with a different QoS class that is needed by applications. More details about PDN connections and EPS bearers are as follows:


      \subsection{PDN connections} 
      When a UE powers up it will request at least one PDN 
      connection to an IP network. The PDN connection is identified by its Access Point Name (APN). For example, as shown in Figure~\ref{fig:pdn_connections}, a UE can have two PDN connections, one to the Internet (APN name is Internet) and another one 
      to a Corporate ``ABC'' at the same time. The UE could also request an additional 
      PDN connection to an IP Multimedia Subsystem (IMS) for services such as VoIP if needed. With each PDN connection, the UE is assigned an IP address associated 
      with that IP network.

     \begin{figure}[t]
        \centerline{\includegraphics[scale=0.8]{figs/pdn-connections.png}}
        \caption{PDN connections in LTE/EPC network}
        \label{fig:pdn_connections}
      \end{figure}



      \subsection{EPS bearers} 
      A UE can request multiple EPS bearers under a PDN 
      connection. All of the EPS bearers under a PDN connection will have the same end-point IP address (i.e., the IP address assigned to the UE by that IP network). Each bearer, however, has a different QoS specification that is used for 
      traffic differentiation. An 
      EPS bearer is a point-to-point connection from the UE to the PGW in Figure~\ref{fig:data_service}.


      The EPS bearer represents a fine-grained level of QoS control in the LTE/EPC. 
      By default, a PDN connection must have a \emph{default bearer} that has the same 
      life time as the PDN connection. The default bearer often does not provide 
      strict QoS guarantees. Additional \emph{dedicated bearers} could be 
      requested by specific applications on the UE. Dedicated bearers often provide 
      specific QoS guarantees for applications. For example, low-latency 
      applications might request a low-latency dedicated bearer to send data. The dedicated bearers are ephemeral and will be removed if the applications no longer  
      need them.

      A bearer associates with a QoS class identifier (QCI) that specifies 
      a delay budget and a loss rate budget of that bearer. A bearer could be Guaranteed-Bit Rate (GBR) if it guarantees a minimum bit rate or Non-BGR if it does not. Table~\ref{table:qci} shows the standardized QCIs, the delay and loss budgets and their example applications.



                  \begin{table}[b]
                    \centering
                    \caption {Standardized QCIs and example applications.}
                    \label{table:qci}
                    {\small
                    \begin{tabular} {|c||c|c|c|c|c|}
                        \hline
                        \textbf{QCI}      & \textbf{Type}    & \textbf{Priority} & \textbf{Delay budget} & \textbf{Loss rate} & \textbf{Applications}                           \\ \hline \hline
                        1 & GBR & 2 & 100ms & $10^{-2}$ & Conversational Voice \\
                        2 &     & 4 & 150ms & $10^{-3}$ & Conversational Video \\
                        3 &     & 3 & 50ms  & $10^{-3}$ & Real-time gaming \\   
                        4 &     & 5 & 300ms & $10^{-6}$ & Buffered Video \\   \hline
                        5 & None-GBR & 1 & 100ms  & $10^{-6}$ & IMS signaling \\   
                    6,8,9 &          & 6 & 300ms  & $10^{-6}$ & TCP-based Www, ftp, etc. \\   
                        7 &          & 7 & 100ms  & $10^{-3}$ & Live voice streaming \\ \hline
                    \end{tabular}
                    }
                \end{table}



    \section{Mobility}
     Mobility is the core feature of a mobile network. Since an eNodeB can only cover 
     a certain geographical area, a moving UE could break its association with an eNodeB if it moves out of the coverage of that eNodeB and initiates another 
     association with a nearby eNodeB. The primary goal of the 
     LTE/EPC network is to provide functionality of mobility which ensures the following:
     
      $\bullet$ The network or traffic from other IP networks can reach the UE given the association point (i.e., eNodeB) of the UE is changing due to mobility. This 
      UE-eNodeB association could change either when the UE is radio-active (i.e., sending/receiving data) or radio-idle (i.e., not sending/receiving data).
      
      $\bullet$ Ongoing sessions from/to the UE must be maintained as the UE moves. This means the end-point IP address on the UE does not change and the connections are {\em seamlessly} adjusted during mobility.
 
     The following describes two different modes of mobility in LTE/EPC. The mobility 
     management described here only applies for \emph{intra-LTE} mobility, i.e., mobility between eNodeBs in a LTE/EPC network.

     \subsection{Idle-mode mobility} 
     In idle-mode mobility, a UE is radio-idle and does not have any 
     ongoing IP sessions with PDN network(s). The network, however, needs 
     to keep track of the location of the UE in case it wants to reach the UE when 
     traffic to that UE is generated. 
     The UE reports its location to the network even when it is radio-idle.


     The eNodeBs are grouped into Tracking Areas (TAs). Each TA consists of a number 
     of eNodeBs depending on the deployment as shown in Figure~\ref{fig:ta}. A UE reports its location periodically 
     after a fixed interval of time regardless of whether the UE is radio active or not. UE also reports its location immediately if it moves 
     out of a TA. This way, the network knows ``roughly'' where the UE is at the 
     granularity of a TA. Note that the smaller the TA, the more accurately the 
     network can track the UE with higher signaling 
     overhead due to more frequent location update.

 			\begin{figure}[h]
        \centerline{\includegraphics[scale=0.8]{figs/ta.png}}
        \caption{Tracking Areas in LTE}
        \label{fig:ta}
      \end{figure}

     When the UE is idle and the network 
     wants to reach it (e.g., if there is traffic destined to that UE), the network \emph{pages} the UE for its location before sending the traffic (i.e., {\em paging procedure}). The MME sends a probe signal to all SGWs which in turn notify the eNodeBs in the TA that the UE was last seen. The eNodeBs broadcast the signal to all UEs and the 
     UE that is being looked at for replies. The UE then activates its radio interface and 
     sets up a connection with the network for traffic delivery.

         
     \subsection{Active-mode mobility} 
     In active-mode mobility, a UE has ongoing IP session(s) with PDN network(s). The network needs to maintain the IP sessions when 
     the UE moves from an eNodeB to another eNodeB. When connected to an eNodeB, the UE 
     periodically reports the signal quality of its neighbor eNodeBs. If a neighbor eNodeB 
     has ``better'' signal quality, the current eNodeB triggers a \emph{handover procedure} to handle the UE to the target eNodeB. After the handover, the point-to-
     point connection between the eNodeB and the PGW changes: the point-to-point 
     connection switches from the old eNodeB to the new eNodeB.



    \section{Components in LTE/EPC}
      This section provides more details about components of LTE/EPC and their roles in the architecture. The components are divided into Control Plane and Data Plane.
      
      \subsection{Control plane}

      $\bullet$ \textbf{MME.} The main component of the control plane is the
      Mobility Management Entity (MME) which controls the LTE access network. 
      It is responsible for: (1) selecting SGW during an initial attach and handover, (2) tracking the UE during idle-mobility and paging, (3) activating 
      or deactivating the EPS bearers on the data plane per request from UE, (4) authenticating the UE 
      if necessary based on the subscribing information from HSS.

      $\bullet$ \textbf{HSS.} HSS is a database of the network that stores user data such as 
      credentials for authentication, default APNs of users and users' access authorization information.

      $\bullet$ \textbf{PCRF.} PCRF encompasses policy control decision and flow-based charging 
      control functionalities. The PCRF receives service information from an Application Function (AF) server and decides how the data flow for a service is handled. The AF server typically lives in the Internet. The UE contacts the AF server via its default bearer.
      

      \subsection{Data plane}


      $\bullet$ \textbf{SGW.} The gateway serves as an aggregation point for multiple eNodeBs 
      in a geographical area and an anchor point for intra and 
      interarea mobility. When the UE moves within an area, traffic towards the UE always 
      go to the SGW of that area. When the UE handovers to an eNodeB served by a different SGW, 
      traffic towards the old SGW will be forwarded to the new SGW. SGW also serves 
      as a buffer of downlink data traffic when a paging procedure happens.

      $\bullet$ \textbf{PGW.} This gateway connects the mobile network with the Internet or other IP networks. The PGW allocates an IP address to the UE. It also performs deep packet inspection, packet filtering, or implements the QoS aspects of EPS bearers. PGW is also an IP anchor point for intertechnology handovers (e.g., LTE to WiMax handover).

   \section{Data plane protocol stack}
      This section provides detailed information on how data plane IP packets are processed at each component and how an EPS bearer delivers IP packets by GPRS Tunneling Protocol (GTP) tunnels.

      \subsection{Data plane protocol stack} Figure~\ref{fig:data_plane} shows the 
      protocol stack of the data plane of LTE/EPC. The UE maintains an IP session with 
      the PGW which in turn maintains another IP session (e.g., using Network Address 
      Translation (NAT)) with the server. The application on the UE runs on top of 
      these IP sessions. 

      \begin{figure}[h]
        \centerline{\includegraphics[scale=0.7]{figs/user-stack.png}}
        \caption{LTE/EPC data plane protocol stack}
        \label{fig:data_plane}
      \end{figure}


      At the RAN, the UE and eNodeB exchange IP packets using Packet 
      Data Convergence Protocol (PDCP) and Radio Link Control (RLC) Protocol and LTE MAC/PHY. 
      When they arrive at the eNodeB, IP packets from a UE will be delivered to the PGW 
      through GTP tunnels running on top of an IP network (UDP/IP) between eNodeB, SGW, and PGW. 

      The following shows an example of how an uplink IP packet is processed at each 
      component. When arriving at the eNodeB the IP packet popped out of the PDCP layer 
      is then encapsulated using GTP on top of UDP/IP transport. Note that the relay 
      at the eNodeB needs to look up which GTP tunnel to use because there are 
      multiple GTP tunnels per UE. The GTP-encapsulated packets will be sent to the 
      SGW which decapsulates the packet and encapsulates it again with a different 
      GTP/UDP header. The SGW then sends the GTP packets to PGW. The PGW then decapsulates 
      the packets to get the inner IP packets. It will send the IP packets to the server 
      or alternatively NATs the packets with its routable address and sends out to the 
      server. 

     
      \subsection{EPS bearer and GTP tunnels} 
      Figure~\ref{fig:eps_bearer} shows 
      an EPS bearer in LTE/EPC. An EPS bearer is a point-to-point link 
      between the UE and the PGW. The IP packets from/to the UE are delivered 
      to/from the PGW by the EPS bearer. A UE has at least one default EPS bearer 
      and optionally multiple dedicated EPS bearers per PDN connection. 
      The interfaces of eNodeB-SGW/SGW-PGW pairs are called S1-U and S5, respectively.


 			\begin{figure}[h]
        \centerline{\includegraphics[scale=0.8]{figs/eps-bearer.png}}
        \caption{An EPS bearer and GTP tunnels in LTE/EPC data plane}
        \label{fig:eps_bearer}
      \end{figure}

      An EPS bearer consists of 6 GTP tunnels: 3 GTP tunnels for each uplink and downlink direction. At the RAN interface between the UE and eNodeB, a radio 
      bearer is set up per EPS bearer. This radio bearer is identified by a Data Radio 
      Bearer (DRB) Identifier. Between the eNodeB and SGW are 2 S1-U GTP tunnels 
      that are identified at the eNodeB and SGW by UL-S1U Tunnel EndPoint Identifier (TEID) 
      and DL-S1U TEID. Similarly, there are 2 S5 GTP tunnels between SGW and PGW 
      that are identified by UL/DL-S5 TEIDs.
      

      The mobile network also uses GTP for supporting per user mobility and quality of service.
      Each tunnel can be distinguished by a unique Tunnel EndPoint Identifier (TEID). When
      a packet is encapsulated, the TEID field in the GTP-U packet header is set
      to the value expected by the tunnel destination.
      The eNodeB/SGW/PGW obtains the TEIDs from the control
      plane (MME) during the connection set-up or modification. The GTP tunnels 
      between SGW and PGW stay persistent unless the UE moves across SGWs. 
      The GTP tunnels between eNodeB and SGW are ephemeral; when the UE is 
      radio-inactive the tunnels are removed. When the UE has data to send it 
      signals the LTE/EPC control plane to set up these tunnels before sending 
      data packets.




      
   




  
  
